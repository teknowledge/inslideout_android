package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.inslideout.adapter.RemoveSportsTeamAdapter;
import com.inslideout.data.Sports;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class RemoveSportsTeams extends Activity {

	private String mCategory;
	private TextView title_remove_text;
	public ProgressDialog progressDialog;
	private ArrayList<Sports> teamsList = new ArrayList<Sports>();

	private EditText search_remove_teams;

	private ListView remove_teamsListView;

	private Button pro_btn, college_btn, backBtn;

	RemoveSportsTeamAdapter adapter;

	private String isPro;

	private boolean isCollege;

	private LinearLayout pro_and_collegeLayout;
	private Context mContext;
	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remove_sports_teams);

		mContext = RemoveSportsTeams.this;

		progressDialog = new ProgressDialog(RemoveSportsTeams.this);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, RemoveSportsTeams.this);

		Intent intent = getIntent();

		mCategory = intent.getStringExtra("category");

		pro_and_collegeLayout = (LinearLayout) findViewById(R.id.pro_and_collegeLayout);

		title_remove_text = (TextView) findViewById(R.id.title_remove_text);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		pro_btn = (Button) findViewById(R.id.pro_btn);

		college_btn = (Button) findViewById(R.id.college_btn);

		if (!isCollege) {
			isPro = "1";

			isCollege = true;

			if (utils.isConnectingToInternet(mContext)) {
				new GetSportsTeamstoRemoveTask().execute("");
			} else {
				utils.showDialog(mContext);
			}

		}

		pro_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!isCollege) {
					isCollege = true;
					isPro = "1";
					pro_btn.setBackgroundResource(R.drawable.pro_drawable);
					college_btn
							.setBackgroundResource(R.drawable.college_drawable);

					if (utils.isConnectingToInternet(mContext)) {
						new GetSportsTeamstoRemoveTask().execute("");
					} else {
						utils.showDialog(mContext);
					}
				}
			}
		});

		college_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isCollege) {
					isCollege = false;
					isPro = "0";

					pro_btn.setBackgroundResource(R.drawable.pro_checked);
					college_btn
							.setBackgroundResource(R.drawable.college_checked);

					if (utils.isConnectingToInternet(mContext)) {
						new GetSportsTeamstoRemoveTask().execute("");
					} else {
						utils.showDialog(mContext);
					}
				}

			}
		});

		remove_teamsListView = (ListView) findViewById(R.id.remove_teamsListView);

		search_remove_teams = (EditText) findViewById(R.id.search_remove_teams);

		search_remove_teams.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				String text = search_remove_teams.getText().toString()
						.toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		search_remove_teams
				.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub

						if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
								|| (actionId == EditorInfo.IME_ACTION_DONE)) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									search_remove_teams.getWindowToken(), 0);
						}
						return false;
					}
				});

		switch (Integer.parseInt(mCategory)) {
		case 1:
			title_remove_text.setText("Remove Baseball Teams");
			break;
		case 2:
			title_remove_text.setText("Remove Basketball Teams");
			pro_and_collegeLayout.setVisibility(View.VISIBLE);
			break;
		case 3:
			title_remove_text.setText("Remove Football Teams");
			pro_and_collegeLayout.setVisibility(View.VISIBLE);
			break;
		case 4:
			title_remove_text.setText("Remove Hockey Teams");
			break;
		case 5:
			title_remove_text.setText("Remove Soccer Teams");
			break;
		}

	}

	public void loadListTeams() {
		adapter = new RemoveSportsTeamAdapter(RemoveSportsTeams.this,
				R.layout.remove_teams_inflate, teamsList);

		remove_teamsListView.setAdapter(adapter);

	}

	private class GetSportsTeamstoRemoveTask extends
			AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								RemoveSportsTeams.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.CATEGORY, new StringBody(
						mCategory));
				entity.addPart(RequestParameters.IS_PRO, new StringBody(isPro));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.FETCH_USER_ADDED_TEAM);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				teamsList = new ArrayList<Sports>();

				JSONArray jsonArray = jsonResponse.getJSONArray("data");

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonData = (JSONObject) jsonArray.get(i);
					String teamId = jsonData.getString("teamidFk");
					String teamName = jsonData.getString("teamname");

					Sports teamDetails = new Sports();
					teamDetails.setmTeamId(teamId);
					teamDetails.setmTeamName(teamName);

					teamsList.add(teamDetails);

					System.out.println(teamId + teamName);
				}
				// Loading list view with team names
				loadListTeams();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent = new Intent(RemoveSportsTeams.this,
				SportsTeamsActivity.class);
		intent.putExtra("category", mCategory);
		startActivity(intent);
		finish();
	}
}
