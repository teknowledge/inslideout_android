package com.inslideout.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inslideout.R;
import com.inslideout.data.ConcertsEvents;

public class ConcertsEventsAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<ConcertsEvents> concertsEventsArrayList;
	private Context mContext;

	LayoutInflater inflater;

	public ConcertsEventsAdapter(Context context, int layoutResourceId,
			ArrayList<ConcertsEvents> mConcertsEventsArrayList) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.concertsEventsArrayList = mConcertsEventsArrayList;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return concertsEventsArrayList.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			holder.artistName = (TextView) convertView
					.findViewById(R.id.eventName);
			holder.outLength = (TextView) convertView
					.findViewById(R.id.outNumber);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.artistName.setText(concertsEventsArrayList.get(position)
				.getArtistname());
		holder.outLength.setText(concertsEventsArrayList.get(position)
				.getOutLength());
		return convertView;
	}

	class ViewHolder {

		TextView artistName;
		TextView outLength;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return concertsEventsArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
