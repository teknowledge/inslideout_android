package com.inslideout.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.inslideout.R;
import com.inslideout.data.SuggestGetSet;
import com.inslideout.service.JsonParse;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Filter;

public class SuggestionAdapter11 extends ArrayAdapter<String> {

	protected static final String TAG = "SuggestionAdapter";
	private List<SuggestGetSet> suggestions;
	int position;

	public SuggestionAdapter11(Activity context, String nameFilter) {
		super(context, R.layout.get_artists_layout);
		suggestions = new ArrayList<SuggestGetSet>();
	}

	@Override
	public int getCount() {
		return suggestions.size();
	}

	@Override
	public String getItem(int index) {
		position = index;
		return suggestions.get(index).getArtistName();

	}

	@Override
	public Filter getFilter() {
		Filter myFilter = new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				JsonParse jp = new JsonParse();
				if (constraint != null
						&& !constraint.toString().equalsIgnoreCase("")) {
					// A class that queries a web API, parses the data and
					// returns an ArrayList<GoEuroGetSet>
					List<SuggestGetSet> new_suggestions = jp
							.getParseJsonWCF(constraint.toString());
					suggestions.clear();
					for (int i = 0; i < new_suggestions.size(); i++) {
						suggestions.add(new_suggestions.get(i));
					}

					// Now assign the values and count to the FilterResults
					// object

					Collections.sort(suggestions);
					filterResults.values = suggestions;
					filterResults.count = suggestions.size();
				}
				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence contraint,
					FilterResults results) {
				if (results != null && results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
		};
		return myFilter;
	}

	public SuggestGetSet getArtistIdAndName() {
		return suggestions.get(position);

	}
}
