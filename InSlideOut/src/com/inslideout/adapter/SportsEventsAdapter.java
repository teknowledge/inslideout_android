package com.inslideout.adapter;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inslideout.R;
import com.inslideout.data.SportsEvents;

public class SportsEventsAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<SportsEvents> sportsEventsArrayList;
	private Context mContext;

	LayoutInflater inflater;

	public SportsEventsAdapter(Context context, int layoutResourceId,
			ArrayList<SportsEvents> mSportsEventsArrayList) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.sportsEventsArrayList = mSportsEventsArrayList;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return sportsEventsArrayList.size();
	}

	ViewHolder holder;
	View v;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			// holder.eventName = (TextView) convertView
			// .findViewById(R.id.eventName);
			// holder.eventHomeTeam = (TextView) convertView
			// .findViewById(R.id.eventHomeTeam);
			// holder.eventAwayTeam = (TextView) convertView
			// .findViewById(R.id.eventAwayTeam);
			holder.outLength = (TextView) convertView
					.findViewById(R.id.outNumber);
			holder.eventHomeTeam = (TextView) convertView
					.findViewById(R.id.eventHomeTeam);
			holder.eventAwayTeam = (TextView) convertView
					.findViewById(R.id.eventAwayTeam);
			
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		
		String event = sportsEventsArrayList.get(position).getmEventname();
		final String eventNameArray[] = event.split(" at ");
		
		
		
		holder.eventHomeTeam.setText(eventNameArray[1]);
		holder.eventAwayTeam.setText(eventNameArray[0]);
		
//		boolean isHomeSingleLine = sportsEventsArrayList.get(position).isHomeSingleLine();
//		
//		boolean isAwaySingleLine = sportsEventsArrayList.get(position).isAwaySingleLine();
//		
//		if(!isHomeSingleLine) {
//			
//		} else {
//			holder.eventHomeTeam
//		}
//
//		new Handler().post(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//				int homeLineCount = holder.eventHomeTeam.getLineCount();
//
//				int awayLineCount = holder.eventAwayTeam.getLineCount();
//				int pos = position;
//				
//				
//				
//				
//				System.out.println("current pos : "+ pos + " home line count : " + homeLineCount+" eventAway line count : " + awayLineCount);
//				if(isHomeSingleLine) {
//					if (homeLineCount > 1) {
//						holder.eventHomeTeam.setTextSize(13);
//						
//						SportsEvents sportsEvents = sportsEventsArrayList.get(position);
//						
//						sportsEvents.setHomeSingleLine(false);
//						
//						sportsEventsArrayList.set(position, sportsEvents);
//						
//						notifyDataSetChanged();
//					} else {
//						holder.eventHomeTeam.setTextSize(18);
//					}
//				} else {
//					if (homeLineCount > 1 && !isHomeSingleLine) {
//						holder.eventHomeTeam.setTextSize(13);
//					} else {
//						holder.eventHomeTeam.setTextSize(18);
//					}
//				}
//				
//				if(isAwaySingleLine) {
//					if (awayLineCount > 1) {
//						holder.eventAwayTeam.setTextSize(13);
//						
//						SportsEvents sportsEvents = sportsEventsArrayList.get(position);
//						
//						sportsEvents.setAwaySingleLine(false);
//						
//						sportsEventsArrayList.set(position, sportsEvents);
//						
//						notifyDataSetChanged();
//					} else {
//						holder.eventAwayTeam.setTextSize(18);					
//					}
//				} else {
//					if (awayLineCount > 1 && !isAwaySingleLine) {
//						holder.eventAwayTeam.setTextSize(13);
//					} else {
//						holder.eventAwayTeam.setTextSize(18);
//					}
//				}
//		
//			}
//
//		});

		// holder.eventName.setText(sportsEventsArrayList.get(position)
		// .getmEventname());
		holder.outLength.setText(sportsEventsArrayList.get(position)
				.getmOutLength());
		return convertView;
	}

	class ViewHolder {

		TextView eventHomeTeam;
		TextView eventAwayTeam;
		TextView outLength;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return sportsEventsArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
