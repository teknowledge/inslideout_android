package com.inslideout.adapter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inslideout.R;
import com.inslideout.data.Contacts;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class MyDeleteConnectionAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<Contacts> myConnectionsArray;
	private Context mContext;
	private ArrayList<Contacts> arraylist;

	LayoutInflater inflater;

	public ProgressDialog progressDialog;
	private Utils utils;

	public MyDeleteConnectionAdapter(Context context, int layoutResourceId,
			ArrayList<Contacts> connectionsListArray) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.myConnectionsArray = connectionsListArray;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.arraylist = new ArrayList<Contacts>();
		this.arraylist.addAll(myConnectionsArray);
		utils = new Utils();
		progressDialog = new ProgressDialog(mContext);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return myConnectionsArray.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.connectionName = (TextView) convertView
					.findViewById(R.id.coneection_name);
			holder.delete_icon = (ImageView) convertView
					.findViewById(R.id.delete_icon);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.connectionName.setText(myConnectionsArray.get(position)
				.getmContactName());

		holder.delete_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						mContext);

				// set title
				alertDialogBuilder.setTitle("Are you sure ?");

				// set dialog message
				alertDialogBuilder
						.setMessage("Delete the Connection")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, close
										// current activity

										if (utils
												.isConnectingToInternet(mContext)) {
											new RemoveConnectionUserList(
													position).execute("");
										} else {
											utils.showDialog(mContext);
										}

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing
										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView connectionName;
		ImageView delete_icon;

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return myConnectionsArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		myConnectionsArray.clear();
		if (charText.length() == 0) {
			myConnectionsArray.addAll(arraylist);
		} else {
			for (Contacts name : arraylist) {
				if (name.getmContactName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					myConnectionsArray.add(name);
				}
			}
		}
		notifyDataSetChanged();
	}

	private class RemoveConnectionUserList extends
			AsyncTask<String, Void, Void> {

		String mResponse = "";
		String connectionStr = "";

		JSONObject jsonResponse = new JSONObject();
		int mPosition;

		public RemoveConnectionUserList(int position) {
			mPosition = position;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								mContext, RequestParameters.USER_ID)));

				if (myConnectionsArray.get(mPosition).getmEmailId()
						.equalsIgnoreCase("")) {
					connectionStr = myConnectionsArray.get(mPosition)
							.getmContactNumber()
							+ myConnectionsArray.get(mPosition).getmEmailId();
				} else {
					connectionStr = myConnectionsArray.get(mPosition)
							.getmContactNumber()
							+ myConnectionsArray.get(mPosition).getmEmailId()
							+ ",";
				}

				entity.addPart(RequestParameters.USER_LIST, new StringBody(

				connectionStr));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.REMOVE_CONNECTIONS);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equals("success")) {
					Toast.makeText(mContext,
							"Connection is deleted successfully",
							Toast.LENGTH_LONG).show();

					myConnectionsArray.remove(mPosition);
					notifyDataSetChanged();
				} else {
					Toast.makeText(mContext, "error deleting Connections",
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
