package com.inslideout.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.inslideout.R;
import com.inslideout.data.Sports;

public class SportsTeamsAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<Sports> teamNamesArray;
	private Context ctx;
	boolean[] itemChecked;
	LayoutInflater inflater;
	ArrayList<String> checkedValue = new ArrayList<String>();

	public SportsTeamsAdapter(Context context, int layoutResourceId,
			ArrayList<Sports> teamsNamesListArray) {
		super();
		// TODO Auto-generated constructor stub
		this.ctx = context;
		this.teamNamesArray = teamsNamesListArray;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		itemChecked = new boolean[teamNamesArray.size()];
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return teamNamesArray.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.teamNames = (TextView) convertView
					.findViewById(R.id.teamName);

			holder.check_teams = (CheckBox) convertView
					.findViewById(R.id.check_teams);

			holder.check_teams
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							int getPosition = (Integer) buttonView.getTag();
							teamNamesArray.get(getPosition).setChecked(
									buttonView.isChecked());
							if (isChecked) {

								// itemChecked[position] = true;
								checkedValue.add(teamNamesArray
										.get(getPosition).getmTeamId());

							} else {

								// itemChecked[position] = false;
								checkedValue.remove(teamNamesArray.get(
										getPosition).getmTeamId());
							}
						}
					});

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.check_teams.setTag(position);
		holder.teamNames.setText(teamNamesArray.get(position).getmTeamName());

		holder.check_teams.setChecked(teamNamesArray.get(position).isChecked());

		return convertView;
	}

	class ViewHolder {
		TextView teamNames;
		CheckBox check_teams;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return teamNamesArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public ArrayList<String> getArrayListOfCheckedTeams() {
		return checkedValue;

	}
}
