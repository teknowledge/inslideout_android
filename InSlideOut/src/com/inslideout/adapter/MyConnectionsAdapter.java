package com.inslideout.adapter;

import java.util.ArrayList;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inslideout.R;

public class MyConnectionsAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<String> myConnectionsArray;
	private Context mContext;
	private ArrayList<String> arraylist;

	LayoutInflater inflater;

	public ProgressDialog progressDialog;

	public MyConnectionsAdapter(Context context, int layoutResourceId,
			ArrayList<String> connectionsListArray) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.myConnectionsArray = connectionsListArray;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.arraylist = new ArrayList<String>();
		this.arraylist.addAll(myConnectionsArray);

		progressDialog = new ProgressDialog(mContext);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return myConnectionsArray.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.connectionName = (TextView) convertView
					.findViewById(R.id.coneection_name);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.connectionName.setText(myConnectionsArray.get(position));

		return convertView;
	}

	class ViewHolder {
		TextView connectionName;

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return myConnectionsArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		myConnectionsArray.clear();
		if (charText.length() == 0) {
			myConnectionsArray.addAll(arraylist);
		} else {
			for (String name : arraylist) {
				if (name.toLowerCase(Locale.getDefault()).contains(charText)) {
					myConnectionsArray.add(name);
				}
			}
		}
		notifyDataSetChanged();
	}

}
