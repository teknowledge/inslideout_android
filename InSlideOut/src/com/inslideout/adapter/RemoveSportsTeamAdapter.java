package com.inslideout.adapter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inslideout.R;
import com.inslideout.data.Sports;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class RemoveSportsTeamAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<Sports> teamNamesArray;
	private Context ctx;

	LayoutInflater inflater;
	ArrayList<String> checkedValue = new ArrayList<String>();
	public ProgressDialog progressDialog;
	private ArrayList<Sports> arraylist;
	private Utils utils;

	public RemoveSportsTeamAdapter(Context context, int layoutResourceId,
			ArrayList<Sports> teamsNamesListArray) {
		super();
		// TODO Auto-generated constructor stub
		this.ctx = context;
		utils = new Utils();
		this.teamNamesArray = teamsNamesListArray;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		progressDialog = new ProgressDialog(ctx);
		this.arraylist = new ArrayList<Sports>();
		this.arraylist.addAll(teamNamesArray);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return teamNamesArray.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.teamNames = (TextView) convertView
					.findViewById(R.id.teamName_remove);

			holder.delete_icon = (ImageView) convertView
					.findViewById(R.id.delete_icon);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.teamNames.setText(teamNamesArray.get(position).getmTeamName());

		holder.delete_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						ctx);

				// set title
				alertDialogBuilder.setTitle("Are you sure ?");

				// set dialog message
				alertDialogBuilder
						.setMessage("Delete the team")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, close
										// current activity

										if (utils.isConnectingToInternet(ctx)) {
											new RemoveTeamFromUserList(position)
													.execute("");
										} else {
											utils.showDialog(ctx);
										}

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing
										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView teamNames;
		ImageView delete_icon;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return teamNamesArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		teamNamesArray.clear();
		if (charText.length() == 0) {
			teamNamesArray.addAll(arraylist);
		} else {
			for (Sports sport : arraylist) {
				if (sport.getmTeamName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					teamNamesArray.add(sport);
				}
			}
		}
		notifyDataSetChanged();
	}

	private class RemoveTeamFromUserList extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();
		int mPosition;

		public RemoveTeamFromUserList(int position) {
			mPosition = position;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ctx, RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.TEAM_ID, new StringBody(
						teamNamesArray.get(mPosition).getmTeamId()));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.REMOVE_USER_TEAM);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equals("success")) {
					Toast.makeText(ctx, "Team is deleted successfully",
							Toast.LENGTH_LONG).show();

					teamNamesArray.remove(mPosition);
					notifyDataSetChanged();
				} else {
					Toast.makeText(ctx, "error deleting teams",
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
