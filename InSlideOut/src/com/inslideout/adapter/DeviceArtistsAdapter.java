package com.inslideout.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.inslideout.R;
import com.inslideout.data.SuggestGetSet;

public class DeviceArtistsAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private ArrayList<SuggestGetSet> artistsArray;
	private Context mContext;

	LayoutInflater inflater;
	public ArrayList<SuggestGetSet> mCheckedArtistsArray;

	public DeviceArtistsAdapter(Context context, int layoutResourceId,
			ArrayList<SuggestGetSet> artistsListArray) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.artistsArray = artistsListArray;
		this.layoutResourceId = layoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return artistsArray.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.artistName = (TextView) convertView
					.findViewById(R.id.artistNames);
			holder.check_artists = (CheckBox) convertView
					.findViewById(R.id.check_artists);

			convertView.setTag(holder);

			holder.check_artists.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					SuggestGetSet data = (SuggestGetSet) cb.getTag();

					data.setChecked(cb.isChecked());
				}
			});

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.artistName.setText(artistsArray.get(position).getArtistName());
		holder.check_artists.setChecked(artistsArray.get(position).isChecked());
		holder.check_artists.setTag(artistsArray.get(position));

		return convertView;
	}

	class ViewHolder {
		TextView artistName;
		CheckBox check_artists;

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return artistsArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	public List<SuggestGetSet> getArtistArray() {
		return artistsArray;

	}

	public ArrayList<SuggestGetSet> getArrayListArtists() {

		mCheckedArtistsArray = new ArrayList<SuggestGetSet>();

		for (int i = 0; i < artistsArray.size(); i++) {
			SuggestGetSet artist = artistsArray.get(i);

			if (artist.isChecked()) {
				mCheckedArtistsArray.add(artist);
			}
		}
		System.out.println(mCheckedArtistsArray.size());
		return mCheckedArtistsArray;
	}

}
