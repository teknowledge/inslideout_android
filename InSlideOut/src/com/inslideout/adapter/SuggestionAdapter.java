package com.inslideout.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inslideout.R;
import com.inslideout.data.SuggestGetSet;
import com.inslideout.service.JsonParse;

public class SuggestionAdapter extends ArrayAdapter<SuggestGetSet> {

	protected static final String TAG = "SuggestionAdapter";
	private List<SuggestGetSet> suggestions;
	int position;
	LayoutInflater inflater;
	public ArrayList<SuggestGetSet> mCheckedArtistsArray;

	public SuggestionAdapter(Activity context, String nameFilter) {
		super(context, R.layout.get_artists_layout);
		suggestions = new ArrayList<SuggestGetSet>();
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		return suggestions.size();
	}

	@Override
	public SuggestGetSet getItem(int index) {
		position = index;
		return suggestions.get(index);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(R.layout.get_artists_layout, parent,
					false);
			holder = new ViewHolder();
			holder.artistName = (TextView) convertView
					.findViewById(R.id.artist_name);
			holder.check_artist = (CheckBox) convertView
					.findViewById(R.id.check_artist);

			holder.check_artist.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					SuggestGetSet data = (SuggestGetSet) cb.getTag();

					data.setChecked(cb.isChecked());
				}
			});

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder = new ViewHolder();
		holder.artistName = (TextView) convertView
				.findViewById(R.id.artist_name);
		holder.check_artist = (CheckBox) convertView
				.findViewById(R.id.check_artist);

		holder.artistName.setText(suggestions.get(position).getArtistName());
		holder.check_artist.setChecked(suggestions.get(position).isChecked());
		holder.check_artist.setTag(suggestions.get(position));

		return convertView;
	}

	class ViewHolder {
		TextView artistName;
		CheckBox check_artist;
	}

	@Override
	public Filter getFilter() {
		Filter myFilter = new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				JsonParse jp = new JsonParse();
				if (constraint != null
						&& !constraint.toString().equalsIgnoreCase("")) {
					// A class that queries a web API, parses the data and
					// returns an ArrayList<GoEuroGetSet>
					List<SuggestGetSet> new_suggestions = jp
							.getParseJsonWCF(constraint.toString());
					suggestions.clear();
					for (int i = 0; i < new_suggestions.size(); i++) {
						suggestions.add(new_suggestions.get(i));
					}

					// Now assign the values and count to the FilterResults
					// object

					Collections.sort(suggestions);
					filterResults.values = suggestions;
					filterResults.count = suggestions.size();
				}
				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence contraint,
					FilterResults results) {
				if (results != null && results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
		};
		return myFilter;
	}

	public SuggestGetSet getArtistIdAndName() {
		return suggestions.get(position);

	}

	public List<SuggestGetSet> getArtistArray() {
		return suggestions;

	}

	public ArrayList<SuggestGetSet> getArrayListArtists() {

		mCheckedArtistsArray = new ArrayList<SuggestGetSet>();

		for (int i = 0; i < suggestions.size(); i++) {
			SuggestGetSet artist = suggestions.get(i);

			if (artist.isChecked()) {
				mCheckedArtistsArray.add(artist);
			}
		}
		System.out.println(mCheckedArtistsArray.size());
		return mCheckedArtistsArray;
	}
}
