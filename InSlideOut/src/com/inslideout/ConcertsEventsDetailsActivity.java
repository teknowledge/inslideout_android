package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inslideout.data.SportsAndConcertsEventsAddress;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class ConcertsEventsDetailsActivity extends Activity {

	private String mArtistName, mLevel, mConnectionsCount;
	public ProgressDialog progressDialog;

	private TextView event_name, artist_name;
	private Button artists_connections, backBtn;

	private TextView title_text;
	private Button planeBtn;

	private ArrayList<String> conectionsList = new ArrayList<String>();

	LinearLayout childLayout;

	private ArrayList<SportsAndConcertsEventsAddress> concertsDetailsAddressArray = new ArrayList<SportsAndConcertsEventsAddress>();
	public Utils utils = new Utils();
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.concerts_events_details);

		mContext = ConcertsEventsDetailsActivity.this;

		progressDialog = new ProgressDialog(ConcertsEventsDetailsActivity.this);

		planeBtn = (Button) findViewById(R.id.planeBtn);

		planeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
				// .parse("http://m.cheapoair.com/?CAID=111673&FPAffiliate=CAN_111673"));
				// startActivity(browserIntent);

				String ticketLink = "http://m.cheapoair.com/?CAID=111673&FPAffiliate=CAN_111673";

				Intent intent = new Intent(ConcertsEventsDetailsActivity.this,
						SeatGeekActivity.class);
				intent.putExtra("ticket", ticketLink);
				startActivity(intent);

			}
		});

		Intent intent = getIntent();

		mArtistName = intent.getStringExtra("artistName");

		conectionsList = intent.getStringArrayListExtra("connectiosArray");

		mLevel = intent.getStringExtra("level");

		title_text = (TextView) findViewById(R.id.title_text);

		if (mLevel.equalsIgnoreCase("0")) {
			title_text.setText("Local Concert");
			planeBtn.setVisibility(View.GONE);
		} else {
			title_text.setText("National Concert");

		}

		mConnectionsCount = intent.getStringExtra("connectionsCount");

		event_name = (TextView) findViewById(R.id.event_name);
		artist_name = (TextView) findViewById(R.id.artist_name);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		artists_connections = (Button) findViewById(R.id.artists_connections);

		artists_connections.setText(mConnectionsCount + " Connections");

		childLayout = (LinearLayout) findViewById(R.id.layout_add);

		if (utils.isConnectingToInternet(mContext)) {
			new GetArtistDetails().execute("");
		} else {
			utils.showDialog(mContext);
		}

		artists_connections.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConcertsEventsDetailsActivity.this,
						MyConnectionsActivity.class);
				intent.putStringArrayListExtra("myConnections", conectionsList);
				startActivity(intent);
			}
		});
	}

	private class GetArtistDetails extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		String mEventName = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConcertsEventsDetailsActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.ARTIST_NAME, new StringBody(
						mArtistName));
				entity.addPart(RequestParameters.LEVEL, new StringBody(mLevel));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.FETCH_ARTIST_EVENTS);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				JSONArray eventDetailsArray = jsonResponse.getJSONArray("data");

				for (int i = 0; i < eventDetailsArray.length(); i++) {

					JSONObject eventDetails = eventDetailsArray
							.getJSONObject(i);

					String event_Time = eventDetails.getString("eventtime");
					String event_address = eventDetails.getString("address");
					String event_sportId = eventDetails.getString("zipcode");
					String event_ticketLink = eventDetails
							.getString("ticketlink");
					String event_Date = eventDetails.getString("eventdate");

					mEventName = eventDetails.getString("eventname");

					String time = utils.convertTime(event_Time);

					SportsAndConcertsEventsAddress addressData = new SportsAndConcertsEventsAddress();
					addressData.setEventAdress(event_address);
					addressData.setEventBuyTicket(event_ticketLink);
					addressData.setEventDate(event_Date);
					// addressData.setEventTime(event_Time);
					addressData.setEventTime(time);

					concertsDetailsAddressArray.add(addressData);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			event_name.setText(mEventName + "'s Events");

			artist_name.setText(mEventName);

			for (int i = 0; i < concertsDetailsAddressArray.size(); i++) {

				LayoutInflater inflater = getLayoutInflater();
				View view = inflater.inflate(
						R.layout.sports_events_address_inflate, null);
				LinearLayout rowLayout = (LinearLayout) view
						.findViewById(R.id.row_layout);
				final TextView eventAddress = (TextView) view
						.findViewById(R.id.event_address);
				final TextView event_Date = (TextView) view
						.findViewById(R.id.event_date);
				final TextView event_Time = (TextView) view
						.findViewById(R.id.event_time);
				Button buyTickets = (Button) view.findViewById(R.id.buy_ticket);
				buyTickets.setTag(i);
				final String ticketLink = concertsDetailsAddressArray.get(i)
						.getEventBuyTicket();
				buyTickets.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Intent browserIntent = new Intent(Intent.ACTION_VIEW,
						// Uri.parse(ticketLink));
						// startActivity(browserIntent);

						Intent intent = new Intent(
								ConcertsEventsDetailsActivity.this,
								SeatGeekActivity.class);
						intent.putExtra("ticket", ticketLink);
						startActivity(intent);
					}
				});

				// String numberString = phoneArray[i];
				// String typeString = phoneTypeArray[i];
				//
				// number.setText(numberString);
				// type.setText(typeString);

				eventAddress.setText(concertsDetailsAddressArray.get(i)
						.getEventAdress());
				event_Date.setText(concertsDetailsAddressArray.get(i)
						.getEventDate());
				event_Time.setText(concertsDetailsAddressArray.get(i)
						.getEventTime());

				childLayout.addView(view);
			}

		}
	}
}
