package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class LoginActivity extends Activity {

	private Button register, login;
	private ImageView forgot_password;
	private EditText user_name, pass_word, email_forgot;
	public ProgressDialog progressDialog;

	String session = "LOGIN";
	private Context mContext;
	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		mContext = LoginActivity.this;

		progressDialog = new ProgressDialog(LoginActivity.this);

		/********** hide keyboard on start *********/

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, LoginActivity.this);

		register = (Button) findViewById(R.id.register);

		login = (Button) findViewById(R.id.login);

		user_name = (EditText) findViewById(R.id.user_name);

		pass_word = (EditText) findViewById(R.id.pass_word);

		forgot_password = (ImageView) findViewById(R.id.forgot_password);

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!user_name.getText().toString().equalsIgnoreCase("")) {
					if (!pass_word.getText().toString().equalsIgnoreCase("")) {
						// Login with credentials

						if (utils.isConnectingToInternet(mContext)) {
							new LoginUserTask().execute("");
						} else {
							utils.showDialog(mContext);
						}

					} else {
						Toast.makeText(getApplicationContext(),
								"Please enter password", Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Please enter username", Toast.LENGTH_SHORT).show();
				}
			}
		});

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this,
						RegisterActivity.class);
				startActivity(intent);
				finish();
			}
		});

		forgot_password.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// custom dialog
				final Dialog dialog = new Dialog(LoginActivity.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				dialog.setContentView(R.layout.forgot_password);
				dialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(android.graphics.Color.TRANSPARENT));

				email_forgot = (EditText) dialog
						.findViewById(R.id.email_forgot);
				Button submit = (Button) dialog.findViewById(R.id.submit);

				Button cancel = (Button) dialog.findViewById(R.id.cancel);
				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (!email_forgot.getText().toString()
								.equalsIgnoreCase("")) {
							if (isValidEmail(email_forgot.getText().toString())) {
								if (utils.isConnectingToInternet(mContext)) {
									new ForgotPasswordTask(dialog).execute("");
								} else {
									utils.showDialog(mContext);
								}
							} else {
								Toast.makeText(getApplicationContext(),
										"Invalid Email", Toast.LENGTH_SHORT)
										.show();
							}

						} else {
							Toast.makeText(getApplicationContext(),
									"Invalid email id", Toast.LENGTH_SHORT)
									.show();
						}
					}
				});

				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

				dialog.show();
			}
		});
	}

	private class LoginUserTask extends AsyncTask<String, Void, Void> {

		String response = "";

		String userId = "";
		String zipCode = "";
		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		// name, email,
		// password,gender,age,nationality,language,longitude,latitude,interestedin,userpic
		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.EMAIL, new StringBody(
						user_name.getText().toString()));

				entity.addPart(RequestParameters.PASSWORD, new StringBody(
						pass_word.getText().toString()));

			} catch (UnsupportedEncodingException e) {

				response = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				response = "exception";
				e.printStackTrace();
			}

			// *** Simply Copy Paste ********
			try {

				// response = PostingDataToServer.postJson(entity,
				// ApplicationAPIs.REGISTER_USER);
				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.AUTHENTICATE);

				response = jsonResponse.getString("errorMsg");
				System.out.println(jsonResponse);

				// System.out.println(response + "value text");

			} catch (Exception e) {
				response = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// {"errorMsg":"emailExists"}
			try {

				if (response.equalsIgnoreCase("success")) {
					// Toast.makeText(LoginActivity.this, response,
					// Toast.LENGTH_LONG).show();

					userId = jsonResponse.getString("userid");
					zipCode = jsonResponse.getString("zipcode");

					Global.getInstance().storeIntoPreference(
							LoginActivity.this, RequestParameters.USER_ID,
							userId);
					Global.getInstance().storeIntoPreference(
							LoginActivity.this, RequestParameters.ZIP_CODE,
							zipCode);
					Global.getInstance().storeIntoPreference(
							LoginActivity.this, RequestParameters.SESSION,
							"LOGIN");

					Intent intent = new Intent(LoginActivity.this,
							MainActivity.class);
					startActivity(intent);
					finish();

				} else if (response.equalsIgnoreCase("error")) {
					Toast.makeText(LoginActivity.this,
							"Invalid username or password", Toast.LENGTH_LONG)
							.show();
				}

				else {
					System.out.println(response);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class ForgotPasswordTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		// String userId = "";
		// String zipCode = "";
		JSONObject jsonResponse = new JSONObject();

		Dialog dialog;

		public ForgotPasswordTask(Dialog mDialog) {
			this.dialog = mDialog;
		}

		// String alreadyExisted = "{" + '"' + "errorMsg" + '"' + ":" + '"'
		// + "email already exists" + '"' + "}";

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		// name, email,
		// password,gender,age,nationality,language,longitude,latitude,interestedin,userpic
		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.EMAIL, new StringBody(
						email_forgot.getText().toString()));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** Simply Copy Paste ********
			try {

				// response = PostingDataToServer.postJson(entity,
				// ApplicationAPIs.REGISTER_USER);
				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.FORGOT_PASSWORD);

				// System.out.println(response + "value text");

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			System.out.println(jsonResponse);
			if (jsonResponse.has("success")) {
				try {
					mResponse = jsonResponse.getString("success");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					mResponse = jsonResponse.getString("error");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (mResponse.equalsIgnoreCase("Yes")) {
				Toast.makeText(getApplicationContext(),
						"Password has been successfully sent",
						Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			} else if (mResponse.equalsIgnoreCase("Not found")) {
				Toast.makeText(
						getApplicationContext(),
						"This user is not registered with us. Please register.",
						Toast.LENGTH_SHORT).show();
			}

		}
	}

	// validating email id
	private boolean isValidEmail(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
