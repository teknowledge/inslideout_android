package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.inslideout.adapter.ArtistsListAdapter;
import com.inslideout.adapter.DeviceArtistsAdapter;
import com.inslideout.adapter.SuggestionAdapter;
import com.inslideout.data.SuggestGetSet;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class ConcertsArtistsActivity11 extends Activity {

	private Button show_artists;
	private EditText search_artists;
	public ListView artistsListView;

	SuggestionAdapter adapter;
	private Context mContext;

	public ArrayList<SuggestGetSet> artistsList = new ArrayList<SuggestGetSet>();
	public ArrayList<SuggestGetSet> CheckedArtists;
	public ArtistsListAdapter mArtistsAdapter;

	public boolean isAdding = false;
	public boolean isDeviceAdding = false;

	private Button addBtn, backBtn;
	public ProgressDialog progressDialog;

	String totalArtistsIds = "";

	private ImageButton add_device_artists;
	private ArrayList<SuggestGetSet> deviceArtistsList = new ArrayList<SuggestGetSet>();

	private ListView deviceArtistsListView;

	private DeviceArtistsAdapter deviceArtistsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.concerts_artists);

		progressDialog = new ProgressDialog(ConcertsArtistsActivity11.this);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		RelativeLayout layout_parent = (RelativeLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, ConcertsArtistsActivity11.this);

		show_artists = (Button) findViewById(R.id.show_artists);

		addBtn = (Button) findViewById(R.id.addBtn);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		show_artists.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConcertsArtistsActivity11.this,
						MyArtistsActivity.class);
				startActivity(intent);
			}
		});

		add_device_artists = (ImageButton) findViewById(R.id.add_device_artists);

		add_device_artists.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						ConcertsArtistsActivity11.this);

				// set title
				alertDialogBuilder.setTitle("Add device artists");

				// set dialog message
				alertDialogBuilder
						.setMessage("Do you want to add device artists?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, close
										// current activity
										deviceArtistsListView
												.setVisibility(View.VISIBLE);
										artistsListView
												.setVisibility(View.GONE);

										deviceArtistsAdapter = new DeviceArtistsAdapter(
												ConcertsArtistsActivity11.this,
												R.layout.device_artist_inflate,
												getArtistFromDevice());

										deviceArtistsListView
												.setAdapter(deviceArtistsAdapter);
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing
										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});

		search_artists = (EditText) findViewById(R.id.search_artists);

		artistsListView = (ListView) findViewById(R.id.artistsListView);
		// search_artists.setThreshold(1);

		// search_artists.setAdapter(new SuggestionAdapter(this, search_artists
		// .getText().toString()));

		deviceArtistsListView = (ListView) findViewById(R.id.deviceArtistsListView);

		deviceArtistsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				// TODO Auto-generated method stub

				artistsListView.setVisibility(View.VISIBLE);

				SuggestGetSet artistsData = (SuggestGetSet) deviceArtistsAdapter
						.getItem(position);

				artistsData.setChecked(true);

				artistsList.add(artistsData);

				loadArtistsList(artistsList);
				isAdding = true;

				deviceArtistsListView.setVisibility(View.GONE);

			}
		});

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				CheckedArtists = null;

				CheckedArtists = new ArrayList<SuggestGetSet>();

				if (mArtistsAdapter != null) {
					CheckedArtists = mArtistsAdapter.getArrayListArtists();

					totalArtistsIds = "";
					String artistsIds = null;

					// for (int i = 0; i < CheckedArtists.size(); i++) {
					// artistsIds = CheckedArtists.get(i).getArtistId();
					// totalArtistsIds = artistsIds + "," + totalArtistsIds;
					// }

					for (int i = 0; i < CheckedArtists.size(); i++) {

						String artistsIds1 = CheckedArtists.get(i)
								.getArtistName();

						if (artistsIds1.contains("&")) {
							artistsIds = artistsIds1.replace('&', ',');
							System.out.println(artistsIds);
						}

						totalArtistsIds = artistsIds1 + "," + totalArtistsIds;
					}

					System.out.println(totalArtistsIds);

					if (!totalArtistsIds.equalsIgnoreCase("")) {
						new InsertUserArtists().execute("");
					} else {
						Toast.makeText(ConcertsArtistsActivity11.this,
								"Check the artists to add", Toast.LENGTH_SHORT)
								.show();
					}
				}

			}
		});

		search_artists.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text

				deviceArtistsListView.setVisibility(View.GONE);
				artistsListView.setVisibility(View.VISIBLE);

				isAdding = false;
//				adapter = new SuggestionAdapter(ConcertsArtistsActivity11.this,
//						cs.toString());
				adapter.getFilter().filter(cs);

				artistsListView.setAdapter(adapter);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}

		});

		search_artists.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub

				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_DONE)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(
							search_artists.getWindowToken(), 0);
				}
				return false;
			}
		});

		// artistsListView.setAdapter(adapter);

		artistsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

//				if (!isAdding) {
//					String artist = adapter.getItem(position);
//
//					SuggestGetSet artistsData = adapter.getArtistIdAndName();
//
//					System.out.println(artist + artistsData.getArtistId()
//							+ artistsData.getSeatgeekID());
//
//					artistsData.setChecked(true);
//
//					artistsList.add(artistsData);
//
//					loadArtistsList(artistsList);
//
//					isAdding = true;
//
//				} else {
//
//				}

			}
		});

	}

	public void loadArtistsList(ArrayList<SuggestGetSet> list) {

		mArtistsAdapter = new ArtistsListAdapter(
				ConcertsArtistsActivity11.this, R.layout.artists_list_layout,
				artistsList);
		artistsListView.setAdapter(mArtistsAdapter);
	}

	private class InsertUserArtists extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConcertsArtistsActivity11.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.ARTIST_LIST, new StringBody(
						totalArtistsIds));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.INSERT_USER_ARTIST);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equals("success")) {
					Toast.makeText(
							ConcertsArtistsActivity11.this,
							"Artists have been added successfully. Artist(s) which do not have SeatGeek IDs associated may not reflect in your profile",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(ConcertsArtistsActivity11.this,
							"error adding artists", Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public ArrayList<SuggestGetSet> getArtistFromDevice() {

		deviceArtistsList = new ArrayList<SuggestGetSet>();
		ArrayList<String> artistsArrayListData = new ArrayList<String>();

		String selection = MediaStore.Audio.Media.ARTIST + " != 0";

		String[] projection = { MediaStore.Audio.Media.ALBUM,
				MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.TITLE,
				MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.DURATION };
		Cursor cursor = this.getContentResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection,
				selection, null, null);

		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {

					String artistName = cursor.getString(1);

					// SuggestGetSet artist = new SuggestGetSet();
					// artist.setArtistName(artistName);
					//
					// deviceArtistsList.add(artist);

					artistsArrayListData.add(artistName);

				} while (cursor.moveToNext());
			}
			cursor.close();

		}

		Utils.removeDuplicates(artistsArrayListData);

		for (int i = 0; i < artistsArrayListData.size(); i++) {
			SuggestGetSet artist = new SuggestGetSet();
			artist.setArtistName(artistsArrayListData.get(i));
			deviceArtistsList.add(artist);
		}
		Collections.sort(deviceArtistsList);
		//
		// for (int i = 0; i < deviceArtistsList.size(); i++) {
		// System.out.println(deviceArtistsList);
		// }
		return deviceArtistsList;
	}

}
