package com.inslideout;

import java.util.Timer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class SeatGeekActivity extends Activity {

	private WebView webView;
	ProgressBar progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.seatgeek);
		String ticketLink = getIntent().getStringExtra("ticket");

		webView = (WebView) findViewById(R.id.webView);
		webView.setWebViewClient(new MyWebViewClient());

		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl(ticketLink);

		startWebView(ticketLink);
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void startWebView(String url) {

		webView.setWebViewClient(new WebViewClient() {

			// If you will not use this method url links are opeen in new brower
			// not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

				view.loadUrl(url);
				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {
				if (progressDialog == null) {

					progressDialog = (ProgressBar) findViewById(R.id.marker_progress);

					progressDialog.setVisibility(View.VISIBLE);

				}

			}

			public void onPageFinished(WebView view, String url) {

				try {

					if (progressDialog.isShown()) {
						progressDialog.setVisibility(View.GONE);

					}

				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}

		});

		// Javascript inabled on webview
		webView.getSettings().setJavaScriptEnabled(true);

		// Other webview options
		/*
		 * webView.getSettings().setLoadWithOverviewMode(true);
		 * webView.getSettings().setUseWideViewPort(true);
		 * webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		 * webView.setScrollbarFadingEnabled(false);
		 * webView.getSettings().setBuiltInZoomControls(true);
		 */

		// Load url in webview
		webView.loadUrl(url);

	}
}
