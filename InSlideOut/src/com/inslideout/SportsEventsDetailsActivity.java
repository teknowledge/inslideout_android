package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inslideout.data.SportsAndConcertsEventsAddress;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class SportsEventsDetailsActivity extends Activity {

	public ProgressDialog progressDialog;

	private String mEventId;

	private TextView team1, team2;
	private TextView event_address, event_date, event_time;
	private TextView event_Name;

	private Button planeBtn;

	private Button connections, backBtn;

	LinearLayout childLayout;
	private String mConnectionsCount;
	public String mLevel;

	private TextView title_text;

	private Utils utils = new Utils();
	private Context mContext;

	private ArrayList<SportsAndConcertsEventsAddress> addressListArray = new ArrayList<SportsAndConcertsEventsAddress>();

	private ArrayList<String> connectionsListArray = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.sports_events_details);

		mContext = SportsEventsDetailsActivity.this;

		progressDialog = new ProgressDialog(SportsEventsDetailsActivity.this);

		title_text = (TextView) findViewById(R.id.title_text);

		planeBtn = (Button) findViewById(R.id.planeBtn);

		planeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent browserIntent = new Intent(
				// Intent.ACTION_VIEW,
				// Uri.parse("http://m.cheapoair.com/?CAID=111673&FPAffiliate=CAN_111673"));
				// startActivity(browserIntent);

				String ticketLink = "http://m.cheapoair.com/?CAID=111673&FPAffiliate=CAN_111673";

				Intent intent = new Intent(SportsEventsDetailsActivity.this,
						SeatGeekActivity.class);
				intent.putExtra("ticket", ticketLink);
				startActivity(intent);

			}
		});

		childLayout = (LinearLayout) findViewById(R.id.childlayout);

		Intent intent = getIntent();

		mEventId = intent.getStringExtra("eventId");

		mLevel = intent.getStringExtra("level");

		mConnectionsCount = intent.getStringExtra("connectionsCount");

		connectionsListArray = intent
				.getStringArrayListExtra("connectiosArray");

		if (mLevel.equalsIgnoreCase("0")) {
			title_text.setText("Local Sports");
			planeBtn.setVisibility(View.GONE);
		} else {
			title_text.setText("National Sports");

		}

		team1 = (TextView) findViewById(R.id.team1);
		team2 = (TextView) findViewById(R.id.team2);

		event_Name = (TextView) findViewById(R.id.event_Name);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		connections = (Button) findViewById(R.id.connections);

		connections.setText(mConnectionsCount + " Connections");

		connections.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(SportsEventsDetailsActivity.this,
						MyConnectionsActivity.class);
				intent.putStringArrayListExtra("myConnections",
						connectionsListArray);
				startActivity(intent);
			}
		});

		// event_address = (TextView) findViewById(R.id.event_address);
		//
		// event_date = (TextView) findViewById(R.id.event_date);
		// event_time = (TextView) findViewById(R.id.event_time);

		if (utils.isConnectingToInternet(mContext)) {
			new GetEventDetails().execute("");
		} else {
			utils.showDialog(mContext);
		}

	}

	private class GetEventDetails extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		// String mEventTime = "";
		// String mAddress = "";
		// String mSportId = "";
		// String mTicketLink = "";
		// String mEventDate = "";
		String mTeam1 = "";
		String mTeam2 = "";
		String mEventName = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				// entity.addPart(
				// RequestParameters.USER_ID,
				// new StringBody(Global.getInstance().getPreferenceVal(
				// SportsEventsDetailsActivity.this,
				// RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.EVENT_ID, new StringBody(
						mEventId));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.GET_EVENT_DETAILS);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equalsIgnoreCase(
						"success")) {

					JSONArray eventDetailsArray = jsonResponse
							.getJSONArray("data");

					for (int i = 0; i < eventDetailsArray.length(); i++) {

						JSONObject eventDetails = eventDetailsArray
								.getJSONObject(i);

						mTeam1 = eventDetails.getString("team1");
						mTeam2 = eventDetails.getString("team2");

						String event_Time = eventDetails.getString("eventtime");
						String event_address = eventDetails
								.getString("address");
						String event_sportId = eventDetails
								.getString("sportid");
						String event_ticketLink = eventDetails
								.getString("ticketlink");
						String event_Date = eventDetails.getString("eventdate");

						mEventName = eventDetails.getString("eventname");

						String time = utils.convertTime(event_Time);

						SportsAndConcertsEventsAddress addressData = new SportsAndConcertsEventsAddress();
						addressData.setEventAdress(event_address);
						addressData.setEventBuyTicket(event_ticketLink);
						addressData.setEventDate(event_Date);
						// addressData.setEventTime(event_Time);
						addressData.setEventTime(time);

						addressListArray.add(addressData);

					}

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			team1.setText(mTeam1);
			team2.setText(mTeam2);
			event_Name.setText(mEventName);

			for (int i = 0; i < addressListArray.size(); i++) {

				LayoutInflater inflater = getLayoutInflater();
				View view = inflater.inflate(
						R.layout.sports_events_address_inflate, null);
				LinearLayout rowLayout = (LinearLayout) view
						.findViewById(R.id.row_layout);
				final TextView eventAddress = (TextView) view
						.findViewById(R.id.event_address);
				final TextView event_Date = (TextView) view
						.findViewById(R.id.event_date);
				final TextView event_Time = (TextView) view
						.findViewById(R.id.event_time);
				Button buyTickets = (Button) view.findViewById(R.id.buy_ticket);
				buyTickets.setTag(i);

				final String ticketLink = addressListArray.get(i)
						.getEventBuyTicket();

				buyTickets.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Intent browserIntent = new Intent(Intent.ACTION_VIEW,
						// Uri.parse(ticketLink));
						// startActivity(browserIntent);

						Intent intent = new Intent(
								SportsEventsDetailsActivity.this,
								SeatGeekActivity.class);
						intent.putExtra("ticket", ticketLink);
						startActivity(intent);

					}
				});

				eventAddress.setText(addressListArray.get(i).getEventAdress());
				event_Date.setText(addressListArray.get(i).getEventDate());
				event_Time.setText(addressListArray.get(i).getEventTime());

				childLayout.addView(view);
			}
			// event_address.setText(mAddress);
			//
			// event_date.setText(mEventDate);
			// event_time.setText(mEventTime);

		}
	}

}
