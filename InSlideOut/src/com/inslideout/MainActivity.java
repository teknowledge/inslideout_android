package com.inslideout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;

public class MainActivity extends FragmentActivity {

	MainLayout mLayout;
	private ListView lvMenu;
	private String[] lvMenuItems;
	Button btMenu;
	TextView tvTitle;
	MenuListAdapter adapter;
	ImageButton editBtn;

	public ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLayout = (MainLayout) this.getLayoutInflater().inflate(
				R.layout.activity_main, null);
		setContentView(mLayout);
		
		registerGCM();

		progressDialog = new ProgressDialog(MainActivity.this);

		editBtn = (ImageButton) findViewById(R.id.editBtn);
		editBtn.setVisibility(View.GONE);

		lvMenuItems = getResources().getStringArray(R.array.menu_items);
		lvMenu = (ListView) findViewById(R.id.menu_listview);
		// lvMenu.setAdapter(new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, lvMenuItems));

		adapter = new MenuListAdapter(MainActivity.this,
				R.layout.menu_list_inflate, lvMenuItems);

		lvMenu.setAdapter(adapter);

		lvMenu.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				onMenuItemClick(parent, view, position, id);
			}

		});

		btMenu = (Button) findViewById(R.id.button_menu);
		btMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Show/hide the menu
				toggleMenu(v);
			}
		});

		tvTitle = (TextView) findViewById(R.id.activity_main_content_title);

		FragmentManager fm = MainActivity.this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		HomePageActivity fragment = new HomePageActivity();
		ft.add(R.id.activity_main_content_fragment, fragment);
		ft.commit();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void toggleMenu(View v) {
		mLayout.toggleMenu();
	}

	private void onMenuItemClick(AdapterView<?> parent, View view,
			int position, long id) {
		String selectedItem = lvMenuItems[position];
		String currentItem = tvTitle.getText().toString();
		if (selectedItem.compareTo(currentItem) == 0) {
			mLayout.toggleMenu();
			return;
		}

		FragmentManager fm = MainActivity.this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment fragment = null;

		if (selectedItem.compareTo("Home") == 0) {
			fragment = new HomePageActivity();
			editBtn.setVisibility(View.GONE);
		} else if (selectedItem.compareTo("My Profile") == 0) {

			fragment = new MyProfileFragment(editBtn);
			editBtn.setVisibility(View.VISIBLE);
		}

		else if (selectedItem.compareTo("FAQ") == 0) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://inslideout.com/faq/"));
			startActivity(browserIntent);

			// fragment = new Layout3();
		} else if (selectedItem.compareTo("About") == 0) {

			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://inslideout.com/about-us/"));
			startActivity(browserIntent);
			// fragment = new Layout4();
		} else if (selectedItem.compareTo("Logout") == 0) {
			// fragment = new Layout5();

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					MainActivity.this);

			// set title
			alertDialogBuilder.setTitle("Please Confirm!");

			// set dialog message
			alertDialogBuilder
					.setMessage("Do you want to logout?")
					.setCancelable(false)
					.setPositiveButton("Logout",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// if this button is clicked, close
									// current activity
									new Logout().execute();
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// if this button is clicked, just close
									// the dialog box and do nothing
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		} else if (selectedItem.compareTo("Contact Us") == 0) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://inslideout.com/contact-us/"));
			startActivity(browserIntent);

			// fragment = new Layout6();
		} else if (selectedItem.compareTo("Rate this App") == 0) {
			
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("https://play.google.com/store/apps/details?id=com.inslideout"));
			startActivity(browserIntent);
			
			
			// fragment = new Layout6();
		}
	

		if (fragment != null) {
			ft.replace(R.id.activity_main_content_fragment, fragment);
			ft.commit();
			// tvTitle.setText(selectedItem);
		}
		mLayout.toggleMenu();
	}

	@Override
	public void onBackPressed() {
		if (mLayout.isMenuShown()) {
			mLayout.toggleMenu();
		} else {
			super.onBackPressed();
		}
	}

	class Logout extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Global.getInstance().storeIntoPreference(MainActivity.this,
					RequestParameters.SESSION, "LOGOUT");

			try {
				Thread.sleep(1000);
			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Intent signinActivity = new Intent(MainActivity.this,
					LoginActivity.class);
			startActivity(signinActivity);
			finish();
		}

	}
	
	private void registerGCM() {
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		// GCMRegistrar.checkManifest(this);

		registerReceiver(mHandleMessageReceiver,
				new IntentFilter(CommonUtilities.DISPLAY_MESSAGE_ACTION));

		// Get GCM registration id
		final String regId = GCMRegistrar.getRegistrationId(this);
		System.out.println("Registreation id : " + regId);
		// Check if regid already presents
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
		} else {
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				// Toast.makeText(getApplicationContext(),
				// "Already registered with GCM", Toast.LENGTH_LONG).show();
			} else {
				// Try to register again, but not in the UI thread.
				// It's also necessary to cancel the thread onDestroy(),
				// hence the use of AsyncTask instead of a raw thread.
				final Context context = this;
				// Asyntask
				AsyncTask<Void, Void, Void> mRegisterTask = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						// Register on our server
						// On server creates a new user
						String deviceid = Secure
								.getString(context.getContentResolver(),
										Secure.ANDROID_ID);
						// UserData userData =
						// LocalPreferences.getUserDataPreferences(getApplicationContext(),
						// PrefernceKeys.USER_DATA);
						// String userid = userData.getUserId();
						String userid = Global.getInstance().getPreferenceVal(
								context, RequestParameters.USER_ID);

						ServerUtilities.register(context, deviceid, regId,
								userid);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
					}

				};
				mRegisterTask.execute(null, null, null);
			}
		}
	}

	/**
	 * Receiving push messages
	 * */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(
					CommonUtilities.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(context.getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 * */

			// Showing received message
			// lblMessage.append(newMessage + "\n");
			// Toast.makeText(getApplicationContext(), "New Message: " +
			// newMessage, Toast.LENGTH_LONG).show();

			// Releasing wake lock
			WakeLocker.release();
		}
	};

	@Override
	protected void onDestroy() {

		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {

		}
		super.onDestroy();

	}
	
}
