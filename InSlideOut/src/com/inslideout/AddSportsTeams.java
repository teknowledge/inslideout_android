package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.inslideout.adapter.SportsTeamsAdapter;
import com.inslideout.data.Sports;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class AddSportsTeams extends Activity {

	private EditText search_teams;
	private TextView add_teams_sports;
	public ProgressDialog progressDialog;

	private ArrayList<Sports> teamsList = new ArrayList<Sports>();
	private SportsTeamsAdapter adapter;

	private ListView teamsListView;

	ArrayList<String> checkedValue = new ArrayList<String>();

	private Button addBtn, pro_btn, college_btn, backBtn;
	private String totalTeamIds = "";

	public String categoryLevel;

	private LinearLayout pro_and_collegeLayout;

	public String isProStr;

	private boolean isCollege;
	private Context mContext;
	private Utils utils = new Utils();

	// private boolean isInProCollege;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.add_sports_teams);

		mContext = AddSportsTeams.this;

		progressDialog = new ProgressDialog(AddSportsTeams.this);

		isProStr = "1";

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, AddSportsTeams.this);

		Intent intent = getIntent();
		categoryLevel = intent.getStringExtra("category");
		System.out.println(categoryLevel);

		addBtn = (Button) findViewById(R.id.addBtn);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		pro_btn = (Button) findViewById(R.id.pro_btn);

		pro_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isCollege) {
					isCollege = false;
					isProStr = "1";
					pro_btn.setBackgroundResource(R.drawable.pro_drawable);
					college_btn
							.setBackgroundResource(R.drawable.college_drawable);

					teamsList = null;
					teamsList = new ArrayList<Sports>();

					search_teams.setText("");

					loadListTeams();

					// new GetSportsTeamsTask().execute("");
					// new SearchSportsTeamsTask().execute("");
				}
			}
		});

		college_btn = (Button) findViewById(R.id.college_btn);

		college_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!isCollege) {
					isCollege = true;
					isProStr = "0";

					pro_btn.setBackgroundResource(R.drawable.pro_checked);
					college_btn
							.setBackgroundResource(R.drawable.college_checked);

					teamsList = null;
					teamsList = new ArrayList<Sports>();

					search_teams.setText("");

					loadListTeams();

					// new GetSportsTeamsTask().execute("");
					// new SearchSportsTeamsTask().execute("");
				}

			}
		});

		pro_and_collegeLayout = (LinearLayout) findViewById(R.id.pro_and_collegeLayout);

		add_teams_sports = (TextView) findViewById(R.id.add_teams_sports);
		switch (Integer.parseInt(categoryLevel)) {
		case 1:
			add_teams_sports.setText("Add Baseball Teams");
			break;
		case 2:
			add_teams_sports.setText("Add Basketball Teams");
			pro_and_collegeLayout.setVisibility(View.VISIBLE);
			// isInProCollege = true;
			break;
		case 3:
			add_teams_sports.setText("Add Football Teams");
			pro_and_collegeLayout.setVisibility(View.VISIBLE);
			// isInProCollege = true;
			break;
		case 4:
			add_teams_sports.setText("Add Hockey Teams");
			break;
		case 5:
			add_teams_sports.setText("Add Soccer Teams");
			break;
		}

		// if (isInProCollege) {
		// if (!isCollege) {
		// isCollege = true;
		// isProStr = "1";
		// //new GetSportsTeamsTask().execute("");
		// new SearchSportsTeamsTask().execute("");
		// }
		// }

		search_teams = (EditText) findViewById(R.id.search_teams);

		search_teams.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				final int DRAWABLE_RIGHT = 2;

				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (event.getRawX() >= (search_teams.getRight() - search_teams
							.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds()
							.width())) {

						// your action here
						if (!search_teams.getText().toString().trim()
								.equalsIgnoreCase("")) {

							String enteredListName = search_teams.getText()
									.toString().trim();

							if (utils.isConnectingToInternet(mContext)) {
								new SearchSportsTeamsTask().execute("");
							} else {
								utils.showDialog(mContext);
							}

						} else {
							Toast.makeText(AddSportsTeams.this,
									"enter any keyword", Toast.LENGTH_SHORT)
									.show();
						}

					}
				}
				return false;
			}
		});

		search_teams.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub

				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_DONE)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(search_teams.getWindowToken(),
							0);

					if (!search_teams.getText().toString().trim()
							.equalsIgnoreCase("")) {

						String enteredListName = search_teams.getText()
								.toString().trim();
						if (utils.isConnectingToInternet(mContext)) {
							new SearchSportsTeamsTask().execute("");
						} else {
							utils.showDialog(mContext);
						}

					} else {
						Toast.makeText(AddSportsTeams.this,
								"enter any keyword", Toast.LENGTH_SHORT).show();
					}
				}
				return false;
			}
		});

		teamsListView = (ListView) findViewById(R.id.teamsListView);

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (adapter != null) {
					checkedValue = adapter.getArrayListOfCheckedTeams();

					removeDuplicates(checkedValue);

					String teamIds = null;

					for (int i = 0; i < checkedValue.size(); i++) {
						teamIds = checkedValue.get(i);
						totalTeamIds = teamIds + "," + totalTeamIds;
					}

					System.out.println(totalTeamIds);
					if (!totalTeamIds.equalsIgnoreCase("")) {

						if (utils.isConnectingToInternet(mContext)) {
							new InsertUserTeamstask().execute("");
						} else {
							utils.showDialog(mContext);
						}

					} else {
						Toast.makeText(AddSportsTeams.this,
								"No Teams Selected", Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(AddSportsTeams.this, "No Teams Selected",
							Toast.LENGTH_LONG).show();
				}

			}
		});

	}

	public void loadListTeams() {
		adapter = new SportsTeamsAdapter(AddSportsTeams.this,
				R.layout.team_names_inflate_layout, teamsList);

		teamsListView.setAdapter(adapter);

	}

	private class SearchSportsTeamsTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.CATEGORY, new StringBody(
						categoryLevel));
				// entity.addPart(RequestParameters.IS_PRO, new
				// StringBody("1"));

				entity.addPart(RequestParameters.IS_PRO, new StringBody(
						isProStr));
				entity.addPart(RequestParameters.SEARCH_TEXT, new StringBody(
						search_teams.getText().toString().trim()));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.SEARCH_SPORT_TEAM);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				teamsList = new ArrayList<Sports>();

				JSONArray jsonArray = jsonResponse.getJSONArray("data");

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonData = (JSONObject) jsonArray.get(i);
					String teamId = jsonData.getString("teamid");
					String teamName = jsonData.getString("teamname");

					Sports teamDetails = new Sports();
					teamDetails.setmTeamId(teamId);
					teamDetails.setmTeamName(teamName);

					teamsList.add(teamDetails);

					System.out.println(teamId + teamName);
				}
				// Loading list view with team names

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(search_teams.getWindowToken(), 0);

				loadListTeams();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class InsertUserTeamstask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			// progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								AddSportsTeams.this, RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.TEAM_LIST, new StringBody(
						totalTeamIds));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.INSERT_USER_TEAM);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equals("success")) {
					Toast.makeText(AddSportsTeams.this,
							"Teams have been added successfully",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(AddSportsTeams.this, "error adding teams",
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class GetSportsTeamsTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			// progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.CATEGORY, new StringBody(
						categoryLevel));
				entity.addPart(RequestParameters.IS_PRO, new StringBody(
						isProStr));
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								AddSportsTeams.this, RequestParameters.USER_ID)));

				System.out.println(isProStr);

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.GET_SPORT_TEAMS);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				teamsList = new ArrayList<Sports>();

				JSONArray jsonArray = jsonResponse.getJSONArray("data");

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonData = (JSONObject) jsonArray.get(i);
					String teamId = jsonData.getString("teamid");
					String teamName = jsonData.getString("teamname");

					Sports teamDetails = new Sports();
					teamDetails.setmTeamId(teamId);
					teamDetails.setmTeamName(teamName);

					teamsList.add(teamDetails);

					System.out.println(teamId + teamName);
				}
				// Loading list view with team names
				loadListTeams();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(AddSportsTeams.this,
				SportsTeamsActivity.class);
		intent.putExtra("category", categoryLevel);
		startActivity(intent);
		finish();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void removeDuplicates(List list) {
		Set uniqueEntries = new HashSet();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (!uniqueEntries.add(element)) // if current element is a
												// duplicate,
				iter.remove(); // remove it
		}
	}
}
