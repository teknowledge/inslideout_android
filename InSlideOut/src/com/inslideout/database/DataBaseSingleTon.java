package com.inslideout.database;

import android.content.Context;

public class DataBaseSingleTon {

	
	private static DataBaseHandler db;
	
	public static Context sContext;
	
	public static DataBaseHandler getDB() {
		if(db == null)
			db = new DataBaseHandler(sContext);
		
		return db;
	}
	
}
