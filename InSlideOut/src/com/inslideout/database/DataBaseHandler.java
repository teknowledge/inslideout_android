package com.inslideout.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHandler {

	// table for clients

	public static final String _ID = "_id";
	public static final String CONTACT_ID = "contact_id";

	public static final String CONTACT_NAME = "contact_name";
	public static final String CONTACT_NUMBER = "contact_number";
	public static final String CONTACT_MAIL = "contact_mail";
	public static final String CONTACT_UPDATED_TIME = "contact_updated_time";
	public static final String CONTACT_ISO_STATUS = "contact_iso_status";
	public static final String CONTACT_ICON = "contact_icon";

	public static final String CONTACTS = "contacts";

	// ******* database name *********//
	private static final String DATABASE_NAME = "inslideout";

	// ******* database version *********//
	private static final int DATABASE_VERSION = 1;

	private static final String CREATE_CONTACTS = "create table contacts(_id integer primary key autoincrement , "
			+ "contact_id text ,"
			+ "contact_name text ,"
			+ "contact_number text ,"
			+ "contact_mail text ,"
			+ "contact_updated_time ,"
			+ "contact_iso_status text ," + "contact_icon text );";

	private final Context context;

	private DBhelper DBHelper;
	private SQLiteDatabase db;

	public DataBaseHandler(Context ctx) {
		this.context = ctx;
		DBHelper = new DBhelper(context);
//		db = DBHelper.getWritableDatabase();
	}

	private static class DBhelper extends SQLiteOpenHelper {
		DBhelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_CONTACTS);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log.w(TAG, "Upgrading database from version " + oldVersion+
			// " to "+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS contacts");

			onCreate(db);
		}
	}

	// ---opens the database---
	public DataBaseHandler open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	public boolean isOpen() {
		return db.isOpen();
	}

	/********** DATASETS table database methods ****************/

	/*****
	 * 
	 * @param contact_id
	 * @param contact_name
	 * @param contact_mail
	 * @param contact_number
	 * @param updated_time
	 * @param status
	 * @return
	 * 
	 *         Insert the contactsin conttacts table
	 */
	public long insertContacts(String contact_id, String contact_name,
			String contact_mail, String contact_number, String updated_time,
			String contact_icon) {
		
	//	db = DBHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();

		initialValues.put(CONTACT_ID, contact_id);
		initialValues.put(CONTACT_NAME, contact_name);
		initialValues.put(CONTACT_MAIL, contact_mail);
		initialValues.put(CONTACT_NUMBER, contact_number);
		initialValues.put(CONTACT_UPDATED_TIME, updated_time);

		initialValues.put(CONTACT_ICON, contact_icon);

		return db.insert(CONTACTS, null, initialValues);
	}

	/*****
	 * 
	 * @return
	 * @throws SQLException
	 *             get all the contacts from contacts table
	 */

	public Cursor getContacts() throws SQLException {

		//db = DBHelper.getWritableDatabase();
		
		return db.query(CONTACTS, new String[] { CONTACT_ID, CONTACT_NAME,
				CONTACT_MAIL, CONTACT_NUMBER, CONTACT_UPDATED_TIME,
				CONTACT_ISO_STATUS, CONTACT_ICON }, null, null, null, null,
				null);

	}

	/*****
	 * 
	 * @return
	 * @throws SQLException
	 *             get all iso contacts from contacts table
	 */

	public Cursor getConnectedContacts() throws SQLException {
		
		//db = DBHelper.getWritableDatabase();

		return db.query(CONTACTS, new String[] { CONTACT_ID, CONTACT_NAME,
				CONTACT_MAIL, CONTACT_NUMBER, CONTACT_UPDATED_TIME,
				CONTACT_ISO_STATUS, CONTACT_ICON }, CONTACT_ISO_STATUS + " = '"
				+ "1" + "'", null, null, null, null);

	}

	/*****
	 * 
	 * @return
	 * @throws SQLException
	 *             get all connected contacts from contacts table
	 */

	public Cursor getISOContacts() throws SQLException {
		
		//db = DBHelper.getWritableDatabase();

		return db.query(CONTACTS, new String[] { CONTACT_ID, CONTACT_NAME,
				CONTACT_MAIL, CONTACT_NUMBER, CONTACT_UPDATED_TIME,
				CONTACT_ISO_STATUS, CONTACT_ICON }, CONTACT_ISO_STATUS + " = '"
				+ "2" + "'" + " OR " + CONTACT_ISO_STATUS + " = '" + "1" + "'",
				null, null, null, null);

	}

	/***
	 * 
	 * @param contact_id
	 * @return Check the contact id whether it exists in contacts table
	 */

	public boolean isContactExists(String contact_id) throws SQLiteException {
		
		//db = DBHelper.getWritableDatabase();

		String Query = "Select * from " + CONTACTS + " where " + CONTACT_ID
				+ " = '" + contact_id + "'";

		if (db.isOpen()) {
			try {
				Cursor cursor = db.rawQuery(Query, null);
				if (cursor.getCount() <= 0) {
					return false;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		} else {
			return false;
		}

		return true;
	}

	/***
	 * 
	 * @param contact_id
	 * @return Check the contact id whether it exists in contacts table
	 */

	public boolean isContactsTableExists() {
		
		//db = DBHelper.getWritableDatabase();

		String Query = "Select * from " + CONTACTS;
		Cursor cursor = db.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			return false;
		}
		return true;
	}

	/****
	 * 
	 * @param contact_id
	 *            get contact updated time
	 * @return
	 * @throws SQLException
	 */

	public Cursor getContactUpdatedTime(String contact_id) throws SQLException {
		
		//db = DBHelper.getWritableDatabase();

		return db.query(CONTACTS, new String[] { CONTACT_UPDATED_TIME },
				CONTACT_ID + " = '" + contact_id + "'", null, null, null, null);

	}

	/****
	 * 
	 * @param contact_id
	 * @param contact_name
	 * @param contact_mail
	 * @param contact_number
	 * @param updated_time
	 * @param status
	 *            update contact modified contact using contact id
	 */

	public void updateContacts(String contact_id, String contact_name,
			String contact_mail, String contact_number, String updated_time,
			String icon) {
		
		//db = DBHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();

		initialValues.put(CONTACT_ID, contact_id);
		initialValues.put(CONTACT_NAME, contact_name);
		initialValues.put(CONTACT_MAIL, contact_mail);
		initialValues.put(CONTACT_NUMBER, contact_number);
		initialValues.put(CONTACT_UPDATED_TIME, updated_time);

		initialValues.put(CONTACT_ICON, icon);

		db.update(CONTACTS, initialValues, CONTACT_ID + " = '" + contact_id
				+ "'", null);
	}

	/*****
	 * update status iso
	 * 
	 * @param contact_id
	 * @param status
	 */

	public void updateISOStatus(String contact_id, String status) {
		
		//db = DBHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();

		initialValues.put(CONTACT_ID, contact_id);
		initialValues.put(CONTACT_ISO_STATUS, status);

		db.update(CONTACTS, initialValues, CONTACT_ID + " = '" + contact_id
				+ "'", null);
	}
}
