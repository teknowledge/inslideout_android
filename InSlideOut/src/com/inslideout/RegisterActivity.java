package com.inslideout;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class RegisterActivity extends Activity {

	private ImageButton backBtn;
	private Button go;
	private EditText first_name, last_name, mobile_number, email_address,
			zip_code, password, confirm_password;

	public ProgressDialog progressDialog;
	private Context mContext;
	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.register_activity);

		mContext = RegisterActivity.this;

		progressDialog = new ProgressDialog(RegisterActivity.this);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, RegisterActivity.this);

		backBtn = (ImageButton) findViewById(R.id.backBtn);
		go = (Button) findViewById(R.id.go);

		first_name = (EditText) findViewById(R.id.first_name);
		last_name = (EditText) findViewById(R.id.last_name);
		mobile_number = (EditText) findViewById(R.id.mobile_number);
		email_address = (EditText) findViewById(R.id.email_address);
		zip_code = (EditText) findViewById(R.id.zip_code);
		password = (EditText) findViewById(R.id.password);
		confirm_password = (EditText) findViewById(R.id.confirm_password);

		go.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (!first_name.getText().toString().equalsIgnoreCase("")) {
					if (!last_name.getText().toString().equalsIgnoreCase("")) {
						if (!mobile_number.getText().toString()
								.equalsIgnoreCase("")) {
							if (!email_address.getText().toString()
									.equalsIgnoreCase("")) {
								if (!zip_code.getText().toString()
										.equalsIgnoreCase("")) {
									if (!password.getText().toString()
											.equalsIgnoreCase("")) {
										if (!confirm_password.getText()
												.toString()
												.equalsIgnoreCase("")) {
											// submit the credentials to
											// register account

											if (utils
													.isConnectingToInternet(mContext)) {
												new RegistrationUserAsyncTask()
														.execute("");
											} else {
												utils.showDialog(mContext);
											}

										} else {
											Toast.makeText(
													getApplicationContext(),
													"Please enter confirm password",
													Toast.LENGTH_SHORT).show();
										}
									} else {
										Toast.makeText(getApplicationContext(),
												"Please enter password",
												Toast.LENGTH_SHORT).show();
									}
								} else {
									Toast.makeText(getApplicationContext(),
											"Please enter zip code",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(getApplicationContext(),
										"Please enter email address",
										Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(getApplicationContext(),
									"Please enter mobile number",
									Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Please enter last name", Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Please enter first name", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
		startActivity(intent);
		finish();
	}

	private class RegistrationUserAsyncTask extends
			AsyncTask<String, Void, Void> {

		String response = "";

		String userId = "";

		JSONObject jsonResponse = new JSONObject();

		// String alreadyExisted = "{" + '"' + "errorMsg" + '"' + ":" + '"'
		// + "email already exists" + '"' + "}";

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		// name, email,
		// password,gender,age,nationality,language,longitude,latitude,interestedin,userpic
		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.FIRST_NAME, new StringBody(
						first_name.getText().toString()));
				entity.addPart(RequestParameters.LAST_NAME, new StringBody(
						last_name.getText().toString()));
				entity.addPart(RequestParameters.CONTACT_NUMBER,
						new StringBody(mobile_number.getText().toString()));
				entity.addPart(RequestParameters.EMAIL, new StringBody(
						email_address.getText().toString()));
				entity.addPart(RequestParameters.ZIP_CODE, new StringBody(
						zip_code.getText().toString()));
				entity.addPart(RequestParameters.PASSWORD, new StringBody(
						password.getText().toString()));

			} catch (UnsupportedEncodingException e) {

				response = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				response = "exception";
				e.printStackTrace();
			}

			// *** Simply Copy Paste ********
			try {

				// response = PostingDataToServer.postJson(entity,
				// ApplicationAPIs.REGISTER_USER);
				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.REGISTER_USER);
				System.out.println(jsonResponse);
				response = jsonResponse.getString("errorMsg");

				// System.out.println(response + "value text");

			} catch (Exception e) {
				response = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// {"errorMsg":"emailExists"}
			try {

				if (response.equalsIgnoreCase("success")) {
					Toast.makeText(RegisterActivity.this, response,
							Toast.LENGTH_LONG).show();
					userId = jsonResponse.getString("userid");
					Global.getInstance().storeIntoPreference(
							RegisterActivity.this, RequestParameters.USER_ID,
							userId);

					Global.getInstance().storeIntoPreference(
							RegisterActivity.this, RequestParameters.ZIP_CODE,
							zip_code.getText().toString());
					Global.getInstance().storeIntoPreference(
							RegisterActivity.this, RequestParameters.SESSION,
							"LOGIN");

					Intent intent = new Intent(RegisterActivity.this,
							MainActivity.class);
					startActivity(intent);
					finish();
				} else if (response.equalsIgnoreCase("emailExists")) {
					Toast.makeText(
							RegisterActivity.this,
							"EmaiID already existed please try another emailID",
							Toast.LENGTH_LONG).show();
				} else if (response.equalsIgnoreCase("mobileExists")) {
					Toast.makeText(
							RegisterActivity.this,
							"mobile number already exists please try another mobile number",
							Toast.LENGTH_LONG).show();
				} else {
					System.out.println(response);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
