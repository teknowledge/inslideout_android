package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.inslideout.adapter.ArtistsListAdapter;
import com.inslideout.adapter.DeviceArtistsAdapter;
import com.inslideout.adapter.SuggestionAdapter;
import com.inslideout.data.SuggestGetSet;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class ConcertsArtistsActivity extends Activity {

	private Button show_artists;
	private EditText search_artists;
	public ListView artistsListView;

	SuggestionAdapter adapter;
	private Context mContext;

	public ArrayList<SuggestGetSet> artistsList = new ArrayList<SuggestGetSet>();
	public ArrayList<SuggestGetSet> CheckedArtists;
	public ArtistsListAdapter mArtistsAdapter;

	public boolean isAdding = false;
	public boolean isDeviceAdding = false;

	private Button addBtn, backBtn;
	public ProgressDialog progressDialog;

	String totalArtistsIds = "";

	private ImageButton add_device_artists;
	private ArrayList<SuggestGetSet> deviceArtistsList = new ArrayList<SuggestGetSet>();

	private ListView deviceArtistsListView;

	private DeviceArtistsAdapter deviceArtistsAdapter;
	String artistsIds = null;

	private TextView selectAll;
	private CheckBox check_all;
	private RelativeLayout relativeLayout;

	private boolean isDeviceArtitsAdded = false;
	ArrayList<SuggestGetSet> allArtists_select;
	private Utils utils = new Utils();

	private boolean isDeviceartists;
	private Button background_layout;
	private boolean help_screen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.concerts_artists);

		mContext = ConcertsArtistsActivity.this;

		progressDialog = new ProgressDialog(ConcertsArtistsActivity.this);
		
		background_layout = (Button) findViewById(R.id.background_layout);
		
		help_screen =	Global.getInstance().getBooleanType(ConcertsArtistsActivity.this,
				"help_add_artists");
		
		if(help_screen){
			background_layout.setVisibility(View.GONE);
		}

		
		background_layout.setBackgroundResource(R.drawable.help_add_artists);
		background_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				background_layout.setVisibility(View.GONE);

				Global.getInstance().storeBooleanType(
						ConcertsArtistsActivity.this, "help_add_artists", true);
			}
		});

		relativeLayout = (RelativeLayout) findViewById(R.id.select_all);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		RelativeLayout layout_parent = (RelativeLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, ConcertsArtistsActivity.this);

		show_artists = (Button) findViewById(R.id.show_artists);

		addBtn = (Button) findViewById(R.id.addBtn);

		backBtn = (Button) findViewById(R.id.backBtn);

		selectAll = (TextView) findViewById(R.id.selectAll);
		check_all = (CheckBox) findViewById(R.id.check_all);

		check_all.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				// if (isChecked) {
				allArtists_select = (ArrayList<SuggestGetSet>) deviceArtistsAdapter
						.getArtistArray();
				for (int i = 0; i < allArtists_select.size(); i++) {
					allArtists_select.get(i).setChecked(isChecked);
				}

				deviceArtistsAdapter.notifyDataSetChanged();

				// }
			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		show_artists.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConcertsArtistsActivity.this,
						MyArtistsActivity.class);
				startActivity(intent);
			}
		});

		// ******* to add the device artists for the first time to user's
		// profile ******//
		ArrayList<SuggestGetSet> artistsListArray = new ArrayList<SuggestGetSet>();
		artistsListArray = getArtistFromDevice();

		for (int i = 0; i < artistsListArray.size(); i++) {

			String artistsIds1 = artistsListArray.get(i).getArtistName();

			if (artistsIds1.contains("&")) {
				artistsIds = artistsIds1.replace('&', ',');
				System.out.println(artistsIds);
			}

			totalArtistsIds = artistsIds1 + "," + totalArtistsIds;
		}

		if (!totalArtistsIds.equalsIgnoreCase("")) {
			isDeviceArtitsAdded = true;

			boolean firstRun = Global.getInstance().getBooleanType(
					ConcertsArtistsActivity.this, "firstRun");

			if (!firstRun) {

				if (utils.isConnectingToInternet(mContext)) {

					new InsertUserArtists().execute("");
				} else {
					utils.showDialog(mContext);
				}

				Global.getInstance().storeBooleanType(
						ConcertsArtistsActivity.this, "firstRun", true);
			}

		} else {
			Toast.makeText(ConcertsArtistsActivity.this, "No device artists",
					Toast.LENGTH_SHORT).show();
		}

		add_device_artists = (ImageButton) findViewById(R.id.add_device_artists);

		add_device_artists.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!isDeviceartists) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							ConcertsArtistsActivity.this);

					// set title
					alertDialogBuilder.setTitle("Add device artists");

					// set dialog message
					alertDialogBuilder
							.setMessage("Do you want to add device artists?")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, close
											// current activity

											search_artists.setText("");

											deviceArtistsListView
													.setVisibility(View.VISIBLE);
											artistsListView
													.setVisibility(View.GONE);

											relativeLayout
													.setVisibility(View.VISIBLE);

											deviceArtistsAdapter = new DeviceArtistsAdapter(
													ConcertsArtistsActivity.this,
													R.layout.device_artist_inflate,
													getArtistFromDevice());

											deviceArtistsListView
													.setAdapter(deviceArtistsAdapter);

											isDeviceartists = true;

										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, just
											// close
											// the dialog box and do nothing
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}

			}
		});

		search_artists = (EditText) findViewById(R.id.search_artists);

		artistsListView = (ListView) findViewById(R.id.artistsListView);
		// search_artists.setThreshold(1);

		// search_artists.setAdapter(new SuggestionAdapter(this, search_artists
		// .getText().toString()));

		deviceArtistsListView = (ListView) findViewById(R.id.deviceArtistsListView);

		// deviceArtistsListView.setOnItemClickListener(new
		// OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// final int position, long id) {
		// // TODO Auto-generated method stub
		//
		// artistsListView.setVisibility(View.VISIBLE);
		//
		// SuggestGetSet artistsData = (SuggestGetSet) deviceArtistsAdapter
		// .getItem(position);
		//
		// artistsData.setChecked(true);
		//
		// artistsList.add(artistsData);
		//
		// loadArtistsList(artistsList);
		// isAdding = true;
		//
		// deviceArtistsListView.setVisibility(View.GONE);
		//
		// }
		// });

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				CheckedArtists = null;

				CheckedArtists = new ArrayList<SuggestGetSet>();

				if (!isDeviceartists) {
					if (adapter != null) {
						CheckedArtists = adapter.getArrayListArtists();

						totalArtistsIds = "";
						artistsIds = "";

						// for (int i = 0; i < CheckedArtists.size(); i++) {
						// artistsIds = CheckedArtists.get(i).getArtistId();
						// totalArtistsIds = artistsIds + "," + totalArtistsIds;
						// }

						for (int i = 0; i < CheckedArtists.size(); i++) {

							String artistsIds1 = CheckedArtists.get(i)
									.getArtistName();

							if (artistsIds1.contains("&")) {
								artistsIds = artistsIds1.replace('&', ',');
								System.out.println(artistsIds);
							}

							totalArtistsIds = artistsIds1 + ","
									+ totalArtistsIds;
						}

						System.out.println(totalArtistsIds);

						if (!totalArtistsIds.equalsIgnoreCase("")) {
							isDeviceArtitsAdded = false;
							if (utils.isConnectingToInternet(mContext)) {
								new InsertUserArtists().execute("");
							} else {
								utils.showDialog(mContext);
							}
						} else {
							Toast.makeText(ConcertsArtistsActivity.this,
									"Check the artists to add",
									Toast.LENGTH_SHORT).show();
						}
					}
				} else {
					// Toast.makeText(ConcertsArtistsActivity.this,
					// "Add device artists", Toast.LENGTH_SHORT).show();

					if (deviceArtistsAdapter != null) {

						CheckedArtists = deviceArtistsAdapter
								.getArrayListArtists();

						totalArtistsIds = "";
						artistsIds = "";

						// for (int i = 0; i < CheckedArtists.size(); i++) {
						// artistsIds = CheckedArtists.get(i).getArtistId();
						// totalArtistsIds = artistsIds + "," + totalArtistsIds;
						// }

						for (int i = 0; i < CheckedArtists.size(); i++) {

							String artistsIds1 = CheckedArtists.get(i)
									.getArtistName();

							if (artistsIds1.contains("&")) {
								artistsIds = artistsIds1.replace('&', ',');
								System.out.println(artistsIds);
							}

							totalArtistsIds = artistsIds1 + ","
									+ totalArtistsIds;
						}

						System.out.println(totalArtistsIds);

						if (!totalArtistsIds.equalsIgnoreCase("")) {
							isDeviceArtitsAdded = false;
							if (utils.isConnectingToInternet(mContext)) {
								new InsertUserArtists().execute("");
							} else {
								utils.showDialog(mContext);
							}
						} else {
							Toast.makeText(ConcertsArtistsActivity.this,
									"Check the artists to add",
									Toast.LENGTH_SHORT).show();
						}

					}

				}

			}
		});

		search_artists.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text

				deviceArtistsListView.setVisibility(View.GONE);
				relativeLayout.setVisibility(View.GONE);
				artistsListView.setVisibility(View.VISIBLE);

				isAdding = false;
				adapter = new SuggestionAdapter(ConcertsArtistsActivity.this,
						cs.toString());
				adapter.getFilter().filter(cs);

				artistsListView.setAdapter(adapter);

				isDeviceartists = false;

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}

		});

		search_artists.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub

				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_DONE)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(
							search_artists.getWindowToken(), 0);
				}
				return false;
			}
		});

		// artistsListView.setAdapter(adapter);

		artistsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				

			}
		});

	}

	public void loadArtistsList(ArrayList<SuggestGetSet> list) {

		mArtistsAdapter = new ArtistsListAdapter(ConcertsArtistsActivity.this,
				R.layout.artists_list_layout, artistsList);
		artistsListView.setAdapter(mArtistsAdapter);
	}

	private class InsertUserArtists extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConcertsArtistsActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.ARTIST_LIST, new StringBody(
						totalArtistsIds));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.INSERT_USER_ARTIST);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equals("success")) {

					if (isDeviceArtitsAdded) {
						// Toast.makeText(ConcertsArtistsActivity.this,
						// "Device artists are added", Toast.LENGTH_LONG)
						// .show();
					} else {
						Toast.makeText(
								ConcertsArtistsActivity.this,
								"Selected artists have been added. Any artists not listed in our database may not reflect in your profile.",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(ConcertsArtistsActivity.this,
							"error adding artists", Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public ArrayList<SuggestGetSet> getArtistFromDevice() {

		deviceArtistsList = new ArrayList<SuggestGetSet>();
		ArrayList<String> artistsArrayListData = new ArrayList<String>();

		String selection = MediaStore.Audio.Media.ARTIST + " != 0";

		String[] projection = { MediaStore.Audio.Media.ALBUM,
				MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.TITLE,
				MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.DURATION };
		Cursor cursor = this.getContentResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection,
				selection, null, null);

		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {

					String artistName = cursor.getString(1);

					// SuggestGetSet artist = new SuggestGetSet();
					// artist.setArtistName(artistName);
					//
					// deviceArtistsList.add(artist);

					artistsArrayListData.add(artistName);

				} while (cursor.moveToNext());
			}
			cursor.close();

		}

		Utils.removeDuplicates(artistsArrayListData);

		for (int i = 0; i < artistsArrayListData.size(); i++) {
			SuggestGetSet artist = new SuggestGetSet();
			artist.setArtistName(artistsArrayListData.get(i));
			deviceArtistsList.add(artist);
		}
		Collections.sort(deviceArtistsList);
		//
		// for (int i = 0; i < deviceArtistsList.size(); i++) {
		// System.out.println(deviceArtistsList);
		// }
		return deviceArtistsList;
	}

}
