package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.inslideout.adapter.MyArtistsAdapter;
import com.inslideout.data.SuggestGetSet;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class MyArtistsActivity extends Activity {

	private ListView myArtistsListView;
	public ProgressDialog progressDialog;

	private ArrayList<SuggestGetSet> addedArrayArtists = new ArrayList<SuggestGetSet>();

	private MyArtistsAdapter adapter;

	private EditText search_my_artists;

	private Button backBtn;
	private Context mContext;
	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.my_artists_layout);
		
		mContext = MyArtistsActivity.this;

		progressDialog = new ProgressDialog(MyArtistsActivity.this);
		
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, MyArtistsActivity.this);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		myArtistsListView = (ListView) findViewById(R.id.myArtistsListView);

		search_my_artists = (EditText) findViewById(R.id.search_my_artists);
		
		
		if (utils.isConnectingToInternet(mContext)) {
			new GetArtistsToRemove().execute("");
		} else {
			utils.showDialog(mContext);
		}

		

		search_my_artists.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				String text = search_my_artists.getText().toString()
						.toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}

	public void loadAddedArtistList() {
		adapter = new MyArtistsAdapter(MyArtistsActivity.this,
				R.layout.added_artist_list, addedArrayArtists);

		myArtistsListView.setAdapter(adapter);
	}

	private class GetArtistsToRemove extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								MyArtistsActivity.this,
								RequestParameters.USER_ID)));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.GET_ADDED_ARTIST);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				addedArrayArtists = new ArrayList<SuggestGetSet>();

				JSONArray jsonArray = jsonResponse.getJSONArray("data");

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonData = (JSONObject) jsonArray.get(i);
					String mArtistName = jsonData.getString("artistname");
					String mArtistIdFk = jsonData.getString("artistidFk");

					SuggestGetSet artist = new SuggestGetSet();

					artist.setArtistId(mArtistIdFk);
					artist.setArtistName(mArtistName);

					addedArrayArtists.add(artist);
				}
				Collections.sort(addedArrayArtists);
				// adapter.notifyDataSetChanged();
				loadAddedArtistList();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
