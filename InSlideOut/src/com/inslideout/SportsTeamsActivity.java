package com.inslideout;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class SportsTeamsActivity extends Activity {

	public ProgressDialog progressDialog;

	private Button backBtn;

	private TextView baseball_teamsCount, basketball_teamsCount,
			football_teamsCount, hockey_teamsCount, soccer_teamsCount;

	private Button remove_baseball_teams, remove_basketball_teams,
			remove_football_teams, remove_hockey_teams, remove_soccer_teams;

	private Button add_baseball_teams, add_basketball_teams,
			add_football_teams, add_hockey_teams, add_soccer_teams;

	public String baseBallStr = "1";
	public String basketBallStr = "2";
	public String footBallStr = "3";
	public String hockeyStr = "4";
	public String soccerStr = "5";

	private Context mContext;
	private Utils utils = new Utils();
	private Button background_layout;
	private boolean help_screen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.sports_teams_layout);

		mContext = SportsTeamsActivity.this;

		background_layout = (Button) findViewById(R.id.background_layout);
		help_screen = Global.getInstance().getBooleanType(
				SportsTeamsActivity.this, "help_add_teams");

		if (help_screen) {
			background_layout.setVisibility(View.GONE);
		}
		background_layout.setBackgroundResource(R.drawable.help_add_sports);
		background_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				background_layout.setVisibility(View.GONE);
				Global.getInstance().storeBooleanType(
						SportsTeamsActivity.this, "help_add_teams", true);
			}
		});

		progressDialog = new ProgressDialog(SportsTeamsActivity.this);

		if (utils.isConnectingToInternet(mContext)) {
			new FetchTeamCountTask().execute("");
		} else {
			utils.showDialog(mContext);
		}

		baseball_teamsCount = (TextView) findViewById(R.id.baseball_teamsCount);
		basketball_teamsCount = (TextView) findViewById(R.id.basketball_teamsCount);

		football_teamsCount = (TextView) findViewById(R.id.football_teamsCount);
		hockey_teamsCount = (TextView) findViewById(R.id.hockey_teamsCount);
		soccer_teamsCount = (TextView) findViewById(R.id.soccer_teamsCount);

		remove_baseball_teams = (Button) findViewById(R.id.remove_baseball_teams);
		remove_basketball_teams = (Button) findViewById(R.id.remove_basketball_teams);

		remove_football_teams = (Button) findViewById(R.id.remove_football_teams);
		remove_hockey_teams = (Button) findViewById(R.id.remove_hockey_teams);
		remove_soccer_teams = (Button) findViewById(R.id.remove_soccer_teams);

		add_baseball_teams = (Button) findViewById(R.id.add_baseball_teams);

		add_basketball_teams = (Button) findViewById(R.id.add_basketball_teams);

		add_football_teams = (Button) findViewById(R.id.add_football_teams);
		add_hockey_teams = (Button) findViewById(R.id.add_hockey_teams);
		add_soccer_teams = (Button) findViewById(R.id.add_soccer_teams);

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		add_baseball_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						AddSportsTeams.class);
				intent.putExtra("category", baseBallStr);
				startActivity(intent);
				finish();
			}
		});
		add_basketball_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						AddSportsTeams.class);

				intent.putExtra("category", basketBallStr);
				startActivity(intent);
				finish();
			}
		});
		add_football_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						AddSportsTeams.class);
				intent.putExtra("category", footBallStr);
				startActivity(intent);
				finish();
			}
		});
		add_hockey_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						AddSportsTeams.class);
				intent.putExtra("category", hockeyStr);
				startActivity(intent);
				finish();
			}
		});
		add_soccer_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						AddSportsTeams.class);
				intent.putExtra("category", soccerStr);
				startActivity(intent);
				finish();
			}
		});

		remove_baseball_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						RemoveSportsTeams.class);
				intent.putExtra("category", baseBallStr);
				startActivity(intent);
				finish();
			}
		});
		remove_basketball_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						RemoveSportsTeams.class);

				intent.putExtra("category", basketBallStr);
				startActivity(intent);
				finish();
			}
		});
		remove_football_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						RemoveSportsTeams.class);
				intent.putExtra("category", footBallStr);
				startActivity(intent);
				finish();
			}
		});
		remove_hockey_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						RemoveSportsTeams.class);
				intent.putExtra("category", hockeyStr);
				startActivity(intent);
				finish();
			}
		});
		remove_soccer_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsTeamsActivity.this,
						RemoveSportsTeams.class);
				intent.putExtra("category", soccerStr);
				startActivity(intent);
				finish();
			}
		});

	}

	private class FetchTeamCountTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		String baseBallTeamsCount = null;
		String basketballTeamsCount = null;
		String hockeyTeamsCount = null;
		String footBallTeamsCount = null;
		String soccerTeamsCount = null;

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								SportsTeamsActivity.this,
								RequestParameters.USER_ID)));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.FETCH_TEAM_COUNT);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonValues = jsonResponse.getJSONObject("data");
				baseBallTeamsCount = jsonValues.getString("baseball");
				basketballTeamsCount = jsonValues.getString("basketball");
				footBallTeamsCount = jsonValues.getString("football");
				hockeyTeamsCount = jsonValues.getString("hockey");
				soccerTeamsCount = jsonValues.getString("tennis");

				baseball_teamsCount.setText(baseBallTeamsCount);
				basketball_teamsCount.setText(basketballTeamsCount);

				football_teamsCount.setText(footBallTeamsCount);
				hockey_teamsCount.setText(hockeyTeamsCount);
				soccer_teamsCount.setText(soccerTeamsCount);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
