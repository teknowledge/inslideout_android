package com.inslideout;

import com.google.android.gcm.GCMRegistrar;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class HomePageActivity extends Fragment {

	ImageView sports_events, concerts_events, connections;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.home_fragment, null);



		sports_events = (ImageView) view.findViewById(R.id.sports_events);

		concerts_events = (ImageView) view.findViewById(R.id.concerts_events);

		connections = (ImageView) view.findViewById(R.id.connections);
		sports_events.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(getActivity(), "sports clicked",
				// Toast.LENGTH_LONG).show();

				Intent intent = new Intent(getActivity(),
						SportsEventsActivity.class);
				startActivity(intent);
			}
		});

		concerts_events.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(getActivity(), "concerts clicked",
				// Toast.LENGTH_LONG).show();
				Intent intent = new Intent(getActivity(),
						ConcertsEventsActivity.class);
				startActivity(intent);
			}
		});

		connections.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(getActivity(), "connections clicked",
				// Toast.LENGTH_LONG).show();

				Intent intent = new Intent(getActivity(),
						ConnectionsActivity.class);
				startActivity(intent);
			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}


	
}
