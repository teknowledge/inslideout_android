package com.inslideout.data;

public class Sports {

	public String mTeamId;
	public boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getmTeamId() {
		return mTeamId;
	}

	public void setmTeamId(String mTeamId) {
		this.mTeamId = mTeamId;
	}

	public String mTeamName;

	public String getmTeamName() {
		return mTeamName;
	}

	public void setmTeamName(String mTeamName) {
		this.mTeamName = mTeamName;
	}
}
