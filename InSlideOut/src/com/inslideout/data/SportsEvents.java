package com.inslideout.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class SportsEvents implements Serializable, Comparator<Contacts>,
		Comparable<SportsEvents> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String mPostalcode;
	public String mAddress;
	public String mTicketlink;
	public String mEventdate;
	public String mEventid;
	public String mEventtype;
	public String mEventname;
	public String mOutLength;
	public ArrayList<String> connections;
	public String mHomeTeam;
	
	private boolean isHomeSingleLine = true;
	
	private boolean isAwaySingleLine = true;
	
	

	public boolean isAwaySingleLine() {
		return isAwaySingleLine;
	}

	public void setAwaySingleLine(boolean isAwaySingleLine) {
		this.isAwaySingleLine = isAwaySingleLine;
	}

	public boolean isHomeSingleLine() {
		return isHomeSingleLine;
	}

	public void setHomeSingleLine(boolean isHomeSingleLine) {
		this.isHomeSingleLine = isHomeSingleLine;
	}

	public String getmHomeTeam() {
		return mHomeTeam;
	}

	public void setmHomeTeam(String mHomeTeam) {
		this.mHomeTeam = mHomeTeam;
	}

	public ArrayList<String> getConnections() {
		return connections;
	}

	public void setConnections(ArrayList<String> connections) {
		this.connections = connections;
	}

	public String getmOutLength() {
		return mOutLength;
	}

	public void setmOutLength(String mOutLength) {
		this.mOutLength = mOutLength;
	}

	public String getmPostalcode() {
		return mPostalcode;
	}

	public void setmPostalcode(String mPostalcode) {
		this.mPostalcode = mPostalcode;
	}

	public String getmAddress() {
		return mAddress;
	}

	public void setmAddress(String mAddress) {
		this.mAddress = mAddress;
	}

	public String getmTicketlink() {
		return mTicketlink;
	}

	public void setmTicketlink(String mTicketlink) {
		this.mTicketlink = mTicketlink;
	}

	public String getmEventdate() {
		return mEventdate;
	}

	public void setmEventdate(String mEventdate) {
		this.mEventdate = mEventdate;
	}

	public String getmEventid() {
		return mEventid;
	}

	public void setmEventid(String mEventid) {
		this.mEventid = mEventid;
	}

	public String getmEventtype() {
		return mEventtype;
	}

	public void setmEventtype(String mEventtype) {
		this.mEventtype = mEventtype;
	}

	public String getmEventname() {
		return mEventname;
	}

	public void setmEventname(String mEventname) {
		this.mEventname = mEventname;
	}

	// @Override
	// public int compareTo(SportsEvents another) {
	// // TODO Auto-generated method stub
	// return (this.mEventname.toLowerCase()).compareTo(another.mEventname
	// .toLowerCase());
	// }

	@Override
	public int compareTo(SportsEvents another) {
		// TODO Auto-generated method stub
		return (this.mHomeTeam.toLowerCase()).compareTo(another.mHomeTeam
				.toLowerCase());
	}

	@Override
	public int compare(Contacts lhs, Contacts rhs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
