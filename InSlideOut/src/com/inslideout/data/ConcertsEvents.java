package com.inslideout.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class ConcertsEvents implements Serializable, Comparator<Contacts>,
		Comparable<ConcertsEvents> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String artistname;
	public String outLength;

	public ArrayList<String> connectionsList;

	public ArrayList<String> getConnectionsList() {
		return connectionsList;
	}

	public void setConnectionsList(ArrayList<String> connectionsList) {
		this.connectionsList = connectionsList;
	}

	public String getArtistname() {
		return artistname;
	}

	public void setArtistname(String artistname) {
		this.artistname = artistname;
	}

	public String getOutLength() {
		return outLength;
	}

	public void setOutLength(String outLength) {
		this.outLength = outLength;
	}

	@Override
	public int compareTo(ConcertsEvents another) {
		// TODO Auto-generated method stub
		return (this.artistname.toLowerCase()).compareTo(another.artistname
				.toLowerCase());
	}

	@Override
	public int compare(Contacts lhs, Contacts rhs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
