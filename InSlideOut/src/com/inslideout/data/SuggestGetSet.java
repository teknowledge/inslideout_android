package com.inslideout.data;

import java.io.Serializable;
import java.util.Comparator;

public class SuggestGetSet implements Serializable, Comparator<SuggestGetSet>,
		Comparable<SuggestGetSet> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String artistId, artistName, seatgeekID;

	boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getArtistId() {
		return artistId;
	}

	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getSeatgeekID() {
		return seatgeekID;
	}

	public void setSeatgeekID(String seatgeekID) {
		this.seatgeekID = seatgeekID;
	}

	public SuggestGetSet(String artistId, String artistName, String seatgeekID) {
		this.setArtistId(artistId);
		this.setArtistName(artistName);
		this.setSeatgeekID(seatgeekID);
	}

	public SuggestGetSet() {

	}

	@Override
	public int compareTo(SuggestGetSet another) {
		// TODO Auto-generated method stub
		return (this.artistName.toLowerCase()).compareTo(another.artistName
				.toLowerCase());
	}

	@Override
	public int compare(SuggestGetSet lhs, SuggestGetSet rhs) {
		// TODO Auto-generated method stub
		return 0;
	}
}
