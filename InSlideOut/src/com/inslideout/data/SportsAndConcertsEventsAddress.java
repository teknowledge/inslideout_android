package com.inslideout.data;

public class SportsAndConcertsEventsAddress {

	public String eventAdress;
	public String eventDate;
	public String eventTime;
	public String eventBuyTicket;

	public String getEventAdress() {
		return eventAdress;
	}

	public void setEventAdress(String eventAdress) {
		this.eventAdress = eventAdress;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventBuyTicket() {
		return eventBuyTicket;
	}

	public void setEventBuyTicket(String eventBuyTicket) {
		this.eventBuyTicket = eventBuyTicket;
	}

}
