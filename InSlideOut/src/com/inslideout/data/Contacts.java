package com.inslideout.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

import android.net.Uri;

public class Contacts implements Serializable, Comparator<Contacts>,
		Comparable<Contacts> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String mContactName;
	public String mContactNumber;
	public String mEmailId = "";
	public String mTextColor;
	public String contactImage;
	public String phoneAndEmailIds;
	public String contactId;
	public String updatedTime;
	public String isoStatus;

	public String getIsoStatus() {
		return isoStatus;
	}

	public void setIsoStatus(String isoStatus) {
		this.isoStatus = isoStatus;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Uri photo;
	public boolean checkedUser;

	public String getPhoneAndEmailIds() {
		return phoneAndEmailIds;
	}

	public void setPhoneAndEmailIds(String phoneAndEmailIds) {
		this.phoneAndEmailIds = phoneAndEmailIds;
	}

	public Uri getPhoto() {
		return photo;
	}

	public void setPhoto(Uri photo) {
		this.photo = photo;
	}

	public String getContactImage() {
		return contactImage;
	}

	public void setContactImage(String contactImage) {
		this.contactImage = contactImage;
	}

	public String getmTextColor() {
		return mTextColor;
	}

	public void setmTextColor(String mTextColor) {
		this.mTextColor = mTextColor;
	}

	public boolean isCheckedUser() {
		return checkedUser;
	}

	public void setCheckedUser(boolean checkedUser) {
		this.checkedUser = checkedUser;
	}

	public ArrayList<String> mNumbersList = new ArrayList<String>();

	public ArrayList<String> getmNumbersList() {
		return mNumbersList;
	}

	public void setmNumbersList(ArrayList<String> mNumbersList) {
		this.mNumbersList = mNumbersList;
	}

	public String getmContactName() {
		return mContactName;
	}

	public void setmContactName(String mContactName) {
		this.mContactName = mContactName;
	}

	public String getmContactNumber() {
		return mContactNumber;
	}

	public void setmContactNumber(String mContactNumber) {
		this.mContactNumber = mContactNumber;
	}

	public String getmEmailId() {
		return mEmailId;
	}

	public void setmEmailId(String mEmailId) {
		this.mEmailId = mEmailId;
	}

	@Override
	public int compareTo(Contacts another) {
		// TODO Auto-generated method stub
		return (this.mContactName.toLowerCase()).compareTo(another.mContactName
				.toLowerCase());
	}

	@Override
	public int compare(Contacts lhs, Contacts rhs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
