package com.inslideout;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MenuListAdapter extends BaseAdapter {

	// mContext;
	int layoutResourceId;
	private String[] mMenuItems;
	private Context mContext;

	private int[] mMenuIcons = { R.drawable.home, R.drawable.my_profile,
			R.drawable.faq, R.drawable.about, R.drawable.logout,
			R.drawable.contact_us, R.drawable.rate_this_app };

	public MenuListAdapter(Context mContext, int layoutResourceId,
			String[] mMenuItems) {
		super();
		// TODO Auto-generated constructor stub
		this.mMenuItems = mMenuItems;
		this.mContext = mContext;
		this.layoutResourceId = layoutResourceId;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mMenuItems.length;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub

		// ViewHolder holder = null;
		View convertView = null;
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			holder.mMenuItem = (TextView) convertView
					.findViewById(R.id.menu_item);
			// holder.mIcon = (ImageView)convertView.findViewById(R.id.);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.mMenuItem.setText(mMenuItems[position]);
		holder.mMenuItem.setCompoundDrawablesWithIntrinsicBounds(
				mMenuIcons[position], 0, 0, 0);
		return convertView;
	}

	class ViewHolder {
		TextView mMenuItem;
		// ImageView mIcon;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mMenuItems[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
