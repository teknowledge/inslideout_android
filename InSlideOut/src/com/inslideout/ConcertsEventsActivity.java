package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.inslideout.adapter.ConcertsEventsAdapter;
import com.inslideout.data.ConcertsEvents;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class ConcertsEventsActivity extends Activity {

	private Button manage_artists, home_btn;
	private ListView concertsListView;
	public ProgressDialog progressDialog;
	private String mLevel;
	private ImageButton concerts_events_type;

	private TextView concerts_text;

	private boolean isNational;

	private ArrayList<ConcertsEvents> concertsEventsArray = new ArrayList<ConcertsEvents>();

	private ArrayList<String> connectionsListArray = new ArrayList<String>();

	private ConcertsEventsAdapter adapter;

	private Context mContext;
	private Utils utils = new Utils();
	private Button background_layout;
	private boolean help_screen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.concerts_events);

		mContext = ConcertsEventsActivity.this;

		progressDialog = new ProgressDialog(ConcertsEventsActivity.this);

		manage_artists = (Button) findViewById(R.id.manage_artists);
		concertsListView = (ListView) findViewById(R.id.concertsListView);
		background_layout = (Button) findViewById(R.id.background_layout);
		
		help_screen =	Global.getInstance().getBooleanType(ConcertsEventsActivity.this,
				"help_concerts");
		
		if(help_screen){
			background_layout.setVisibility(View.GONE);
			concertsListView.setVisibility(View.VISIBLE);
		}else{
			concertsListView.setVisibility(View.INVISIBLE);
		}
		background_layout.setBackgroundResource(R.drawable.help_concerts);
		background_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				background_layout.setVisibility(View.GONE);
				concertsListView.setVisibility(View.VISIBLE);
				Global.getInstance().storeBooleanType(
						ConcertsEventsActivity.this, "help_concerts", true);

			}
		});

		home_btn = (Button) findViewById(R.id.home_btn);

		home_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		concerts_text = (TextView) findViewById(R.id.concerts_text);

		
		

		manage_artists.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConcertsEventsActivity.this,
						ConcertsArtistsActivity.class);

				startActivity(intent);
			}
		});

		if (!isNational) {
			mLevel = "0";
			concerts_text.setText("Local Concerts");

			if (utils.isConnectingToInternet(mContext)) {
				new FetchConcertsEvents().execute("");
			} else {
				utils.showDialog(mContext);
			}

		}

		concerts_events_type = (ImageButton) findViewById(R.id.concerts_events_type);
		concerts_events_type.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!isNational) {
					isNational = true;
					mLevel = "1";
					concerts_text.setText("National Concerts");
					concerts_events_type
							.setImageResource(R.drawable.local_icon);
					if (utils.isConnectingToInternet(mContext)) {
						new FetchConcertsEvents().execute("");
					} else {
						utils.showDialog(mContext);
					}
				} else {
					isNational = false;
					mLevel = "0";
					concerts_text.setText("Local Concerts");
					concerts_events_type
							.setImageResource(R.drawable.national_icon);
					if (utils.isConnectingToInternet(mContext)) {
						new FetchConcertsEvents().execute("");
					} else {
						utils.showDialog(mContext);
					}
				}
			}
		});

		concertsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ConcertsEvents eventData = (ConcertsEvents) adapter
						.getItem(position);

				Intent intent = new Intent(ConcertsEventsActivity.this,
						ConcertsEventsDetailsActivity.class);

				intent.putExtra("artistName", eventData.getArtistname());
				intent.putExtra("level", mLevel);
				intent.putStringArrayListExtra("connectiosArray",
						eventData.getConnectionsList());
				intent.putExtra("connectionsCount", eventData.getOutLength());
				startActivity(intent);
			}
		});

	}

	private void loadConcertsEventsList() {
		// TODO Auto-generated method stub

		Collections.sort(concertsEventsArray);

		if (concertsEventsArray.size() == 0) {

			if (mLevel.equalsIgnoreCase("0")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"No event suggestions. Add artists and/or local connections.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// do things
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			} else if (mLevel.equalsIgnoreCase("1")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"No event suggestions. Add artists and/or distant connections.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// do things
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}

		}

		adapter = new ConcertsEventsAdapter(ConcertsEventsActivity.this,
				R.layout.sports_events_inflate, concertsEventsArray);

		concertsListView.setAdapter(adapter);
	}

	private class FetchConcertsEvents extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setTitle("Please wait....");
			progressDialog.setMessage("Event loading may take a few moments");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConcertsEventsActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.LEVEL, new StringBody(mLevel));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.FETCH_EVENTS_ARTISTS);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				concertsEventsArray = null;

				concertsEventsArray = new ArrayList<ConcertsEvents>();

				JSONObject jsonObject = jsonResponse.getJSONObject("data");

				JSONArray jsonConcertsArray = jsonObject
						.getJSONArray("concert");

				for (int i = 0; i < jsonConcertsArray.length(); i++) {

					connectionsListArray = null;
					connectionsListArray = new ArrayList<String>();

					JSONObject JsonData = jsonConcertsArray.getJSONObject(i);

					JSONArray eventsOutArray = JsonData.getJSONArray("out");

					ConcertsEvents sEventsData = new ConcertsEvents();

					for (int j = 0; j < eventsOutArray.length(); j++) {

						JSONObject objectConnect = eventsOutArray
								.getJSONObject(j);

						connectionsListArray.add(objectConnect
								.getString("name"));

					}

					sEventsData.setArtistname(JsonData.getString("artistname"));

					sEventsData.setOutLength(eventsOutArray.length() + "");

					sEventsData.setConnectionsList(connectionsListArray);
					concertsEventsArray.add(sEventsData);
				}

				loadConcertsEventsList();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
