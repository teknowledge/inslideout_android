package com.inslideout;

import android.content.Context;
import android.content.Intent;

import com.inslideout.service.ApplicationAPIs;

public final class CommonUtilities {

	// give your server registration url here
	public static final String SERVER_URL = ApplicationAPIs.REGISTER_DEVICE_FOR_PUSH;

	// Google project id
	public static final String SENDER_ID = "894512567397";

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "CommonUtilities";

	public static final String DISPLAY_MESSAGE_ACTION = "DISPLAY_MESSAGE";

	public static final String EXTRA_MESSAGE = "message";
	public static final String EXTRA_ID = "id";
	public static final String EXTRA_TYPE = "type";

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void displayMessage(Context context, String message,
			String type, String id) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		intent.putExtra(EXTRA_ID, id);
		intent.putExtra(EXTRA_TYPE, type);
		context.sendBroadcast(intent);
	}
}
