package com.inslideout.webservices;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class PostingDataToServer {

	static InputStream inputStream = null;
	static JSONObject jsonObj = null;
	static String jsonResponseString = "";

	public static String postJson(MultipartEntity entity, String url) {

		String serverResponse = "";
		URL uri = null;
		try {
			uri = new URL(url);
		} catch (MalformedURLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		HttpClient httpClient = new DefaultHttpClient();
		HttpURLConnection conn;
		try {
			conn = (HttpURLConnection) uri.openConnection();
			conn.setRequestProperty("Content-Length", "1000"); // <------------
			conn.connect();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		HttpPost httppost = new HttpPost(url);

		HttpContext localContext = new BasicHttpContext();

		try {

			httppost.setEntity(entity);

			HttpResponse response = httpClient.execute(httppost, localContext);

			BufferedReader reader = new BufferedReader(new InputStreamReader(

			response.getEntity().getContent(), "UTF-8"));

			serverResponse = reader.readLine();

			System.out.println("--- Server Response ---" + serverResponse);
			// Utils.sResponse = serverResponse;
			// Utils.sError = "";

		} catch (ClientProtocolException e) {
			// Utils.sError = "error";
			System.out.println();
			e.printStackTrace();

		} catch (IOException e) {
			// Utils.sError = "error";
			e.printStackTrace();

		}

		return serverResponse;

	}

	public static String sendJsonData(StringEntity entity, String url) {

		String serverResponse = "";
		URL uri = null;
		try {
			uri = new URL(url);
		} catch (MalformedURLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		HttpClient httpClient = new DefaultHttpClient();
		HttpURLConnection conn;
		try {
			conn = (HttpURLConnection) uri.openConnection();
			conn.setRequestProperty("Content-Length", "1000"); // <------------
			conn.connect();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		HttpPost httppost = new HttpPost(url);

		HttpContext localContext = new BasicHttpContext();

		try {

			httppost.setEntity(entity);

			HttpResponse response = httpClient.execute(httppost, localContext);

			BufferedReader reader = new BufferedReader(new InputStreamReader(

			response.getEntity().getContent(), "UTF-8"));

			serverResponse = reader.readLine();

			System.out.println("--- Server Response ---" + serverResponse);
			// Utils.sResponse = serverResponse;
			// / Utils.sError = "";

		} catch (ClientProtocolException e) {
			// Utils.sError = "error";
			System.out.println();
			e.printStackTrace();

		} catch (IOException e) {
			// /Utils.sError = "error";
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return serverResponse;

	}

	public static JSONObject getJSONFromServer(MultipartEntity entity,
			String url) {

		// Making HTTP request
		try {
			// defaultHttpClient
			System.out.println("ENTITY :" + entity + "URL :" + url);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(entity);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputStream = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			inputStream.close();
			jsonResponseString = sb.toString();
			System.out.println("RESPONSE FROM SERVER :: " + "  "
					+ jsonResponseString);
			// /Utils.sResponse = jsonResponseString;
			// ElectricalCoCNZPreference.savePreferences(ResponseFromServer.this,
			// "", jsonResponseString);
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jsonObj = new JSONObject(jsonResponseString);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return JSON String
		return jsonObj;

	}
	
	public static JSONObject getJSONFromUrl(String url) {

		System.out.println("URL IN SERVER : " + url);

		String jsonString = null;

		// Making HTTP request
		try {

			// // Load CAs from an InputStream
			// // (could be from a resource or ByteArrayInputStream or ...)
			// CertificateFactory cf = CertificateFactory.getInstance("X.509");
			// // From
			// https://www.washington.edu/itconnect/security/ca/load-der.crt
			// InputStream caInput = new BufferedInputStream(new
			// FileInputStream("load-der.crt"));
			// Certificate ca;
			// try {
			// ca = cf.generateCertificate(caInput);
			// System.out.println("ca=" + ((X509Certificate)
			// ca).getSubjectDN());
			// } finally {
			// caInput.close();
			// }
			//
			// // Create a KeyStore containing our trusted CAs
			// String keyStoreType = KeyStore.getDefaultType();
			// KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			// keyStore.load(null, null);
			// keyStore.setCertificateEntry("ca", ca);
			//
			// // Create a TrustManager that trusts the CAs in our KeyStore
			// String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			// TrustManagerFactory tmf =
			// TrustManagerFactory.getInstance(tmfAlgorithm);
			// tmf.init(keyStore);
			//
			// // Create an SSLContext that uses our TrustManager
			// SSLContext context = SSLContext.getInstance("TLS");
			// context.init(null, tmf.getTrustManagers(), null);

			HttpURLConnection linkConnection = null;
			try {
				URL linkurl = new URL(url);
				linkConnection = (HttpURLConnection) linkurl.openConnection();

				int responseCode = linkConnection.getResponseCode();
				if (responseCode == HttpURLConnection.HTTP_OK) {
					InputStream linkinStream = linkConnection.getInputStream();
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					int j = 0;
					while ((j = linkinStream.read()) != -1) {
						baos.write(j);
					}
					byte[] data = baos.toByteArray();
					jsonString = new String(data);

					jsonObj = new JSONObject(jsonString);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (linkConnection != null) {
					linkConnection.disconnect();
				}
			}

			// // defaultHttpClient
			// HttpClient httpClient = new DefaultHttpClient();
			// HttpGet httpPost = new HttpGet(url);
			//
			// HttpResponse httpResponse = httpClient.execute(httpPost);
			//
			// HttpEntity httpEntity = httpResponse.getEntity();
			// inputStream = httpEntity.getContent();

			// } catch (UnsupportedEncodingException e) {
			// e.printStackTrace();
			// } catch (ClientProtocolException e) {
			// e.printStackTrace();
			// } catch (IOException e) {
			// e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try {
		// BufferedReader reader = new BufferedReader(new InputStreamReader(
		// inputStream, "iso-8859-1"), 8);
		// StringBuilder sb = new StringBuilder();
		// String line = null;
		// while ((line = reader.readLine()) != null) {
		// sb.append(line + "\n");
		// }
		// inputStream.close();
		//
		// jsonResponseString = sb.toString();
		// System.out.println("RESPONSE FROM SERVER :: " + "  "
		// + jsonResponseString);
		//
		// } catch (Exception e) {
		// Log.e("Buffer Error", "Error converting result " + e.toString());
		// }
		//
		// // try parse the string to a JSON object
		// try {
		// jsonObj = new JSONObject(jsonResponseString);
		// } catch (JSONException e) {
		// Log.e("JSON Parser", "Error parsing data " + e.toString());
		// }

		// return JSON String
		return jsonObj;

	}

	

}
