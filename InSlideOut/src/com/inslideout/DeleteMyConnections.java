package com.inslideout;

import java.util.ArrayList;
import java.util.Locale;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.inslideout.adapter.MyDeleteConnectionAdapter;
import com.inslideout.data.Contacts;
import com.inslideout.service.Utils;

public class DeleteMyConnections extends Activity {

	private ArrayList<Contacts> myConnections = new ArrayList<Contacts>();

	private ListView myConnectionsListView;

	private MyDeleteConnectionAdapter adapter;
	private EditText search_connections;
	private Button backBtn;

	LinearLayout search_layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.my_connections);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, DeleteMyConnections.this);

		// Intent intent = getIntent();
		//
		// myConnections = intent.getStringArrayListExtra("myConnections");

		myConnections = ConnectionsActivity.connectionsArrayList;

		myConnectionsListView = (ListView) findViewById(R.id.myConnectionsListView);

		backBtn = (Button) findViewById(R.id.backBtn);

		search_layout = (LinearLayout) findViewById(R.id.search_layout);

		adapter = new MyDeleteConnectionAdapter(DeleteMyConnections.this,
				R.layout.my_delete_connection_inflate, myConnections);

		myConnectionsListView.setAdapter(adapter);

		search_connections = (EditText) findViewById(R.id.search_connections);

		if (myConnections.size() < 2) {
			search_layout.setVisibility(View.GONE);
		}

		search_connections.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				String text = search_connections.getText().toString()
						.toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 onBackPressed();

			}
		});

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		

		Intent intent = new Intent(DeleteMyConnections.this,
				ConnectionsActivity.class);
		startActivity(intent);
		finish();
	}

}
