package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.inslideout.AlphabetListAdapter.Item;
import com.inslideout.AlphabetListAdapter.Row;
import com.inslideout.AlphabetListAdapter.Section;
import com.inslideout.data.Contacts;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class ConnectionsScreen extends ListActivity {

	private AlphabetListAdapter adapter = new AlphabetListAdapter();
	private GestureDetector mGestureDetector;
	private List<Object[]> alphabet = new ArrayList<Object[]>();
	private HashMap<String, Integer> sections = new HashMap<String, Integer>();
	private int sideIndexHeight;
	private static float sideIndexX;
	private static float sideIndexY;
	private int indexListSize;

	public ProgressDialog progressDialog;
	public ArrayList<String> checkUsersList = new ArrayList<String>();

	private String checkUsers = "";
	List<Contacts> contactsList;
	String phoneNumbersAndEmailIds;
	private Button addBtn, backBtn, show_connections;
	String emailId = "";

	private Context mContext;
	private Utils utils = new Utils();

	private EditText search_contacts;

	public static ArrayList<Contacts> connectionsArrayList;
	private Button background_layout;
	private boolean help_screen;

	class SideIndexGestureListener extends
			GestureDetector.SimpleOnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			sideIndexX = sideIndexX - distanceX;
			sideIndexY = sideIndexY - distanceY;

			if (sideIndexX >= 0 && sideIndexY >= 0) {
				displayListItem();
			}

			return super.onScroll(e1, e2, distanceX, distanceY);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_alphabet);

		mContext = ConnectionsScreen.this;

		mGestureDetector = new GestureDetector(this,
				new SideIndexGestureListener());

		progressDialog = new ProgressDialog(ConnectionsScreen.this);
		background_layout = (Button) findViewById(R.id.background_layout);

		help_screen = Global.getInstance().getBooleanType(
				ConnectionsScreen.this, "help_connections");

		if (help_screen) {
			background_layout.setVisibility(View.GONE);

		}
		background_layout.setBackgroundResource(R.drawable.help_connections);
		background_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				background_layout.setVisibility(View.GONE);
				Global.getInstance().storeBooleanType(ConnectionsScreen.this,
						"help_connections", true);
			}
		});

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		RelativeLayout layout_parent = (RelativeLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, ConnectionsScreen.this);

		new LoadingContacts().execute("");

		addBtn = (Button) findViewById(R.id.addBtn);

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				phoneNumbersAndEmailIds = "";

				ArrayList<Contacts> checkedContactsToAdd = adapter
						.getContactsArrayListChecked();

				for (int i = 0; i < checkedContactsToAdd.size(); i++) {
					ArrayList<String> contactNumbers = checkedContactsToAdd
							.get(i).getmNumbersList();

					for (int j = 0; j < contactNumbers.size(); j++) {

						phoneNumbersAndEmailIds = contactNumbers.get(j) + ","
								+ phoneNumbersAndEmailIds;

					}
					emailId = checkedContactsToAdd.get(i).getmEmailId();
					if (!emailId.equalsIgnoreCase("")) {
						phoneNumbersAndEmailIds = emailId + ","
								+ phoneNumbersAndEmailIds;
					} else {
						emailId = phoneNumbersAndEmailIds;
					}

				}

				System.out.println(emailId);

				if (utils.isConnectingToInternet(mContext)) {

					new InsertUserConnection().execute("");
				} else {
					utils.showDialog(mContext);
				}

			}
		});

		search_contacts = (EditText) findViewById(R.id.search_contacts);

		search_contacts.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				String text = search_contacts.getText().toString()
						.toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		search_contacts.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub

				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_DONE)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(
							search_contacts.getWindowToken(), 0);
				}
				return false;
			}
		});

		backBtn = (Button) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});

		show_connections = (Button) findViewById(R.id.show_connections);
		show_connections.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConnectionsScreen.this,
						DeleteMyConnections.class);

				startActivity(intent);
				finish();
			}
		});

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mGestureDetector.onTouchEvent(event)) {
			return true;
		} else {
			return false;
		}
	}

	public void updateList() {
		LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
		sideIndex.removeAllViews();
		indexListSize = alphabet.size();
		if (indexListSize < 1) {
			return;
		}

		int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
		int tmpIndexListSize = indexListSize;
		while (tmpIndexListSize > indexMaxSize) {
			tmpIndexListSize = tmpIndexListSize / 2;
		}
		double delta;
		if (tmpIndexListSize > 0) {
			delta = indexListSize / tmpIndexListSize;
		} else {
			delta = 1;
		}

		TextView tmpTV;
		for (double i = 1; i <= indexListSize; i = i + delta) {
			Object[] tmpIndexItem = alphabet.get((int) i - 1);
			String tmpLetter = tmpIndexItem[0].toString();

			tmpTV = new TextView(this);
			tmpTV.setText(tmpLetter);
			tmpTV.setGravity(Gravity.CENTER);
			tmpTV.setTextSize(15);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1);
			tmpTV.setLayoutParams(params);
			sideIndex.addView(tmpTV);
		}

		sideIndexHeight = sideIndex.getHeight();

		sideIndex.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// now you know coordinates of touch
				sideIndexX = event.getX();
				sideIndexY = event.getY();

				// and can display a proper item it country list
				displayListItem();

				return false;
			}
		});
	}

	public void displayListItem() {
		LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
		sideIndexHeight = sideIndex.getHeight();
		// compute number of pixels for every side index item
		double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

		// compute the item index for given event position belongs to
		int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

		// get the item (we can do it since we know item index)
		if (itemPosition < alphabet.size()) {
			Object[] indexItem = alphabet.get(itemPosition);
			int subitemPosition = sections.get(indexItem[0]);

			// ListView listView = (ListView) findViewById(android.R.id.list);
			getListView().setSelection(subitemPosition);

		}
	}

	private ArrayList<Contacts> readContacts() {
		// TODO Auto-generated method stub
		final ArrayList<Contacts> list = new ArrayList<Contacts>();
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);

		String phone = null;
		String emailContact = null;

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));
				long contactId = Long.parseLong(id);

				int col = cur
						.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);

				String updated = cur.getString(col);

				Contacts contactData = new Contacts();

				String name = cur
						.getString(cur
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

				contactData.setmContactName(name);

				if (Build.VERSION.SDK_INT <= 11) {
					Uri uri = getPhotoUri(contactId);

					if (uri != null) {
						contactData.setPhoto(uri);
					}
				} else {
					String image_uri = cur
							.getString(cur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

					if (image_uri != null) {
						contactData.setContactImage(image_uri);
					} else {
						contactData.setContactImage("NA");
					}
				}

				if (Integer
						.parseInt(cur.getString(cur
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					System.out.println("name : " + name + ", ID : " + id);
					if (name.equalsIgnoreCase("Client")) {
						System.out.println("in");
					}

					Cursor pCur = cr.query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { id }, null);
					ArrayList<String> phoneNumbers = new ArrayList<String>();
					String finalNumbersAdd = "";
					while (pCur.moveToNext()) {
						phone = pCur
								.getString(pCur
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

						String phoneNumbersAdd = phone.replace(" ", "");

						finalNumbersAdd = phoneNumbersAdd + ","
								+ finalNumbersAdd;

						contactData.setmContactNumber(finalNumbersAdd);
						System.out.println("phone : " + phone + ", ID : " + id);
						phoneNumbers.add(phone.replace(" ", ""));

						contactData.setmNumbersList(phoneNumbers);

						checkUsersList.add(phone.replace(" ", ""));

					}

					pCur.close();
					Cursor emailCur = cr.query(
							ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID
									+ " = ?", new String[] { id }, null);
					while (emailCur.moveToNext()) {
						emailContact = emailCur
								.getString(emailCur
										.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

						contactData.setmEmailId(emailContact);
						System.out.println("emailContact : " + emailContact
								+ ", ID : " + id);

						// finalNumbersAdd = emailContact + "," +
						// finalNumbersAdd;
						// contactData.setPhoneAndEmailIds(finalNumbersAdd);
						checkUsersList.add(emailContact);
					}
					emailCur.close();

					list.add(contactData);
				}

			}

		}

		return list;

	}

	private class CheckUserConnections extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConnectionsScreen.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.USER_LIST, new StringBody(
						checkUsers));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.CHECK_USER_CONNECTION);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				connectionsArrayList = new ArrayList<Contacts>();

				JSONArray checkedContactsArray = jsonResponse
						.getJSONArray("data");

				for (int i = 0; i < checkedContactsArray.length(); i++) {

					JSONObject userContacts = checkedContactsArray
							.getJSONObject(i);

					String mUserContact = userContacts.getString("usercontact");

					for (int j = 0; j < contactsList.size(); j++) {

						// String numbers[] = contactsList.get(j)
						// .getmContactNumber().split(",");
						String numbers[];

						// ***** to check for maild id and phone numbers inslide
						// out values to give color *******///

						if (mUserContact.contains("@")) {
							numbers = contactsList.get(j).getmEmailId()
									.split(",");
						} else {
							numbers = contactsList.get(j).getmContactNumber()
									.split(",");
						}

						for (int m = 0; m < numbers.length; m++) {
							if (numbers[m].equalsIgnoreCase(mUserContact)) {

								String mInslide = userContacts
										.getString("inslide");

								if (mInslide.equalsIgnoreCase("1")) {

									contactsList.get(j).setCheckedUser(true);
									contactsList.get(j).setmTextColor("green");

									if (!connectionsArrayList
											.contains(contactsList.get(j))) {
										connectionsArrayList.add(contactsList
												.get(j));
									}

								} else if (mInslide.equalsIgnoreCase("2")) {

									contactsList.get(j).setmTextColor("green");
									contactsList.get(j).setCheckedUser(false);

									System.err.println(contactsList);
								} else {
									if (contactsList.get(j).getmTextColor() == null) {
										contactsList.get(j).setCheckedUser(
												false);
										contactsList.get(j)
												.setmTextColor("red");
									}

								}

							}
						}

						// if (contactsList.get(j).getmContactNumber()
						// .equalsIgnoreCase(mUserContact)) {
						// }
					}

				}

				// adapter.notifyDataSetChanged();

				loadListContacts();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void loadListContacts() {
		List<Row> rows = new ArrayList<Row>();
		int start = 0;
		int end = 0;
		String previousLetter = null;
		Object[] tmpIndexItem = null;
		Pattern numberPattern = Pattern.compile("[0-9]");

		for (Contacts contact : contactsList) {
			String firstLetter = contact.getmContactName().substring(0, 1)
					.toUpperCase();

			// Group numbers together in the scroller
			if (numberPattern.matcher(firstLetter).matches()) {
				firstLetter = "#";
			}

			// If we've changed to a new letter, add the previous letter to the
			// alphabet scroller
			if (previousLetter != null && !firstLetter.equals(previousLetter)) {
				end = rows.size() - 1;
				tmpIndexItem = new Object[3];
				tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
				tmpIndexItem[1] = start;
				tmpIndexItem[2] = end;
				alphabet.add(tmpIndexItem);

				start = end + 1;
			}

			// Check if we need to add a header row
			if (!firstLetter.equals(previousLetter)) {
				rows.add(new Section(firstLetter));
				sections.put(firstLetter, start);

			}

			// Add the country to the list

			// rows.add(new Item(contact.getmContactName()));

			rows.add(new Item(contact));

			previousLetter = firstLetter;
		}

		if (previousLetter != null) {
			// Save the last letter
			tmpIndexItem = new Object[3];
			tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
			tmpIndexItem[1] = start;
			tmpIndexItem[2] = rows.size() - 1;
			alphabet.add(tmpIndexItem);
		}

		adapter.setRows(rows, ConnectionsScreen.this, true);
		setListAdapter(adapter);

		updateList();
	}

	private class InsertUserConnection extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConnectionsScreen.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.USER_LIST, new StringBody(
						phoneNumbersAndEmailIds));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.INSERT_USER_CONNECTION);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equalsIgnoreCase(
						"success")) {
					Toast.makeText(ConnectionsScreen.this,
							"Mail Invitation(s) Sent", Toast.LENGTH_SHORT);

					// Intent i = new
					// Intent(android.content.Intent.ACTION_VIEW);
					//
					// i.putExtra("address", phoneNumbersAndEmailIds);
					//
					// i.putExtra("sms_body",
					// "Hi. We don't see each other enough. This FREE app will help. https://play.google.com/store/apps/details?id=com.inslideout ");
					//
					// i.setType("vnd.android-dir/mms-sms");
					//
					// startActivity(i);

					// Intent smsIntent = new
					// Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:5551212;5551217"));

					String phoneNumbersOnly[] = phoneNumbersAndEmailIds
							.split(",");
					ArrayList<String> phoneNumberss = new ArrayList<String>();
					for (int i = 0; i < phoneNumbersOnly.length; i++) {

						if (!phoneNumbersOnly[i].contains("@")) {
							phoneNumberss.add(phoneNumbersOnly[i]);
						}

					}
					String numbers = "";
					for (int i = 0; i < phoneNumberss.size(); i++) {
						numbers = phoneNumberss.get(i) + "," + numbers;
					}

					Intent smsIntent = new Intent(Intent.ACTION_SENDTO,
							Uri.parse("smsto:" + numbers));
					smsIntent
							.putExtra(
									"sms_body",
									"Hi. We don't see each other enough. This FREE app will help. Android: j.mp/iso-and \n iPhone: bit.ly/inslideout");
					startActivity(smsIntent);

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public Uri getPhotoUri(long contactId) {
		try {
			Cursor cur = this
					.getContentResolver()
					.query(ContactsContract.Data.CONTENT_URI,
							null,
							ContactsContract.Data.CONTACT_ID
									+ "="
									+ contactId
									+ " AND "
									+ ContactsContract.Data.MIMETYPE
									+ "='"
									+ ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
									+ "'", null, null);
			if (cur != null) {
				if (!cur.moveToFirst()) {
					return null; // no photo
				}
			} else {
				return null; // error in cursor process
			}
			cur.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		Uri person = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, contactId);
		return Uri.withAppendedPath(person,
				ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void removeDuplicates(List list) {
		Set uniqueEntries = new HashSet();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (!uniqueEntries.add(element)) // if current element is a
												// duplicate,
				iter.remove(); // remove it
		}
	}

	private class LoadingContacts extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {

			contactsList = readContacts();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Collections.sort(contactsList);

			removeDuplicates(checkUsersList);

			for (int i = 0; i < checkUsersList.size(); i++) {
				checkUsers = checkUsersList.get(i) + "," + checkUsers;
			}

			if (utils.isConnectingToInternet(mContext)) {
				new CheckUserConnections().execute("");
			} else {
				utils.showDialog(mContext);
			}

		}
	}

}
