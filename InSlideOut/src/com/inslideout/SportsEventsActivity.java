package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inslideout.adapter.SportsEventsAdapter;
import com.inslideout.data.SportsEvents;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class SportsEventsActivity extends Activity {

	private Button manage_teams, home_button;

	private ListView eventsListView;
	private ImageButton events_type;
	private TextView title_text;

	public ProgressDialog progressDialog;

	private String mLevel;

	private boolean isNational;

	private ArrayList<SportsEvents> sportsEventsArray = new ArrayList<SportsEvents>();

	private SportsEventsAdapter adapter;
	private ArrayList<String> connectionsList = new ArrayList<String>();

	private Context mContext;
	private Utils utils = new Utils();
	private Button background_layout;
	private boolean help_screen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		mContext = SportsEventsActivity.this;

		setContentView(R.layout.sports_events_activity);

		background_layout = (Button) findViewById(R.id.background_layout);
		eventsListView = (ListView) findViewById(R.id.eventsListView);

		help_screen = Global.getInstance().getBooleanType(
				SportsEventsActivity.this, "help_sports");

		if (help_screen) {
			background_layout.setVisibility(View.GONE);
			eventsListView.setVisibility(View.VISIBLE);
		} else {
			eventsListView.setVisibility(View.INVISIBLE);
		}

		background_layout.setBackgroundResource(R.drawable.help_sports);
		background_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				background_layout.setVisibility(View.GONE);
				eventsListView.setVisibility(View.VISIBLE);
				Global.getInstance().storeBooleanType(
						SportsEventsActivity.this, "help_sports", true);
			}
		});

		progressDialog = new ProgressDialog(SportsEventsActivity.this);

		events_type = (ImageButton) findViewById(R.id.events_type);

		title_text = (TextView) findViewById(R.id.title_text);

		home_button = (Button) findViewById(R.id.home_button);

		home_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		if (!isNational) {
			mLevel = "0";
			title_text.setText("Local Sports");

			if (utils.isConnectingToInternet(mContext)) {
				new FetchSportsAndConcertsEvents().execute("");
			} else {
				utils.showDialog(mContext);
			}

		}

		events_type.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!isNational) {
					isNational = true;
					mLevel = "1";
					title_text.setText("National Sports");
					events_type.setImageResource(R.drawable.local_icon);
					if (utils.isConnectingToInternet(mContext)) {
						new FetchSportsAndConcertsEvents().execute("");
					} else {
						utils.showDialog(mContext);
					}
				} else {
					isNational = false;
					mLevel = "0";
					title_text.setText("Local Sports");
					events_type.setImageResource(R.drawable.national_icon);
					if (utils.isConnectingToInternet(mContext)) {
						new FetchSportsAndConcertsEvents().execute("");
					} else {
						utils.showDialog(mContext);
					}
				}

			}
		});

		manage_teams = (Button) findViewById(R.id.manage_teams);

		manage_teams.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SportsEventsActivity.this,
						SportsTeamsActivity.class);
				startActivity(intent);

			}
		});

		eventsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				SportsEvents eventData = (SportsEvents) adapter
						.getItem(position);

				Intent intent = new Intent(SportsEventsActivity.this,
						SportsEventsDetailsActivity.class);

				intent.putExtra("level", mLevel);
				intent.putExtra("eventId", eventData.getmEventid());
				intent.putExtra("connectionsCount", eventData.getmOutLength());
				intent.putStringArrayListExtra("connectiosArray",
						eventData.getConnections());

				startActivity(intent);
			}
		});

	}

	public void loadSportsEventsList() {

		Collections.sort(sportsEventsArray);

		if (sportsEventsArray.size() == 0) {

			if (mLevel.equalsIgnoreCase("0")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"No event suggestions. Add local teams and/or connections.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// do things
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			} else if (mLevel.equalsIgnoreCase("1")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"No event suggestions. Add distant connections and/or teams.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// do things
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}

		}

		adapter = new SportsEventsAdapter(SportsEventsActivity.this,
				R.layout.sports_events_inflate_changed, sportsEventsArray);

		eventsListView.setAdapter(adapter);

	}

	private class FetchSportsAndConcertsEvents extends
			AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setTitle("Please wait....");
			progressDialog.setMessage("Event loading may take a few moments");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								SportsEventsActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.LEVEL, new StringBody(mLevel));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.FETCH_EVENTS_SPORTS);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				sportsEventsArray = null;
				sportsEventsArray = new ArrayList<SportsEvents>();

				JSONObject jsonObject = jsonResponse.getJSONObject("data");

				JSONArray jsonSportsArray = jsonObject.getJSONArray("sport");

				for (int i = 0; i < jsonSportsArray.length(); i++) {

					connectionsList = null;
					connectionsList = new ArrayList<String>();

					JSONObject JsonData = jsonSportsArray.getJSONObject(i);

					JSONArray eventOutArray = JsonData.getJSONArray("out");

					SportsEvents sEventsData = new SportsEvents();

					for (int j = 0; j < eventOutArray.length(); j++) {

						JSONObject objectConnect = eventOutArray
								.getJSONObject(j);

						connectionsList.add(objectConnect.getString("name"));

					}

					sEventsData
							.setmPostalcode(JsonData.getString("postalcode"));
					sEventsData.setmAddress(JsonData.getString("address"));
					sEventsData
							.setmTicketlink(JsonData.getString("ticketlink"));
					sEventsData.setmEventdate(JsonData.getString("eventdate"));
					sEventsData.setmEventid(JsonData.getString("eventid"));
					sEventsData.setmEventtype(JsonData.getString("eventtype"));
					sEventsData.setmEventname(JsonData.getString("eventname"));

					sEventsData.setmOutLength(eventOutArray.length() + "");
					sEventsData.setConnections(connectionsList);

					String eventName = JsonData.getString("eventname");
					String teams[] = eventName.split(" at ");

					sEventsData.setmHomeTeam(teams[1]);

					sportsEventsArray.add(sEventsData);
				}

				loadSportsEventsList();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
