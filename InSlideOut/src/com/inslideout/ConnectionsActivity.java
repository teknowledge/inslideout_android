package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.inslideout.AlphabetListAdapter.Item;
import com.inslideout.AlphabetListAdapter.Row;
import com.inslideout.AlphabetListAdapter.Section;
import com.inslideout.data.Contacts;
import com.inslideout.database.DataBaseHandler;
import com.inslideout.database.DataBaseSingleTon;
import com.inslideout.segmentview.SegmentedButton;
import com.inslideout.segmentview.SegmentedButton.OnClickListenerSegmentedButton;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class ConnectionsActivity extends ListActivity {

	private AlphabetListAdapter adapter = new AlphabetListAdapter();
	private GestureDetector mGestureDetector;
	private List<Object[]> alphabet = new ArrayList<Object[]>();
	private HashMap<String, Integer> sections = new HashMap<String, Integer>();
	private int sideIndexHeight;
	private static float sideIndexX;
	private static float sideIndexY;
	private int indexListSize;

	public ProgressDialog progressDialog;
	public ArrayList<String> checkUsersList = new ArrayList<String>();

	private String checkUsers = "";
	List<Contacts> contactsList = new ArrayList<Contacts>();
	List<Contacts> isoContactsList = new ArrayList<Contacts>();
	List<Contacts> connectedContactsList = new ArrayList<Contacts>();
	String phoneNumbersAndEmailIds;
	private Button addBtn, backBtn, show_connections;

	private RelativeLayout background_text_image;
	private TextView background_empty_text_message;
	String emailId = "";

	private Context mContext;
	private Utils utils = new Utils();

	private EditText search_contacts;
	private boolean isNotInAllContacts;

	public static ArrayList<Contacts> connectionsArrayList;
	private Button background_layout;
	private boolean help_screen;
	private boolean isContactsTableExists;

	SegmentedButton buttons;

	// DataBaseHandler db = new DataBaseHandler(ConnectionsActivity.this);

	DataBaseHandler db;

	private LoadingContacts mTask;

	private List<Row> allContactsArrayList = new ArrayList<Row>();

	String emailIds = "";

	class SideIndexGestureListener extends
			GestureDetector.SimpleOnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			sideIndexX = sideIndexX - distanceX;
			sideIndexY = sideIndexY - distanceY;

			if (sideIndexX >= 0 && sideIndexY >= 0) {
				displayListItem();
			}

			return super.onScroll(e1, e2, distanceX, distanceY);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		mTask.cancel(true);
		// db.close();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (!isContactsTableExists()) {
			isContactsTableExists = true;
		} else {
			isContactsTableExists = false;
		}

		// if (utils.isConnectingToInternet(ConnectionsActivity.this)) {
		// // new LoadingContacts().execute();
		//
		// mTask = new LoadingContacts();
		// mTask.execute();
		//
		// } else {
		// utils.showDialog(ConnectionsActivity.this);
		// }
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_alphabet);

		mContext = ConnectionsActivity.this;

		DataBaseSingleTon.sContext = mContext;

		db = DataBaseSingleTon.getDB();

		// if (db.isOpen()) {
		// db.close();
		// }
		// db.open();

		if (!isContactsTableExists()) {
			isContactsTableExists = true;
		}

		addBtn = (Button) findViewById(R.id.addBtn);

		background_text_image = (RelativeLayout) findViewById(R.id.background_text_image);
		background_empty_text_message = (TextView) findViewById(R.id.background_empty_text_message);

		// Create the segmented buttons
		buttons = (SegmentedButton) findViewById(R.id.segmented);
		buttons.clearButtons();
		buttons.addButtons(("InSlideOut"), ("All"), "Connected");

		// First button is selected
		buttons.setPushedButtonIndex(1);
		Button button1 = (Button) buttons.getChildAt(1);
		button1.setTextColor(Color.WHITE);

		// Some example click handlers. Note the click won't get executed
		// if the segmented button is already selected (dark blue)
		buttons.setOnClickListener(new OnClickListenerSegmentedButton() {
			@Override
			public void onClick(int index) {
				if (index == 0) {

					Handler handler = new Handler();
					handler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							initIsoContacts();
						}
					});

				} else if (index == 1) {

					Handler handler = new Handler();
					handler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							initAll();
						}
					});

				} else if (index == 2) {

					Handler handler = new Handler();
					handler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							initConnectedList();
						}
					});

				}
			}
		});

		mGestureDetector = new GestureDetector(this,
				new SideIndexGestureListener());

		progressDialog = new ProgressDialog(ConnectionsActivity.this);
		background_layout = (Button) findViewById(R.id.background_layout);

		help_screen = Global.getInstance().getBooleanType(
				ConnectionsActivity.this, "help_connections");

		if (help_screen) {
			background_layout.setVisibility(View.GONE);

		}
		background_layout.setBackgroundResource(R.drawable.help_connections);
		background_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				background_layout.setVisibility(View.GONE);
				Global.getInstance().storeBooleanType(ConnectionsActivity.this,
						"help_connections", true);
			}
		});

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		RelativeLayout layout_parent = (RelativeLayout) findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, ConnectionsActivity.this);

		// contactsList = getContactsFromDB();
		contactsList.clear();
		contactsList.addAll(getContactsFromDB());
		Collections.sort(contactsList);

		List<Row> list = loadListContacts(contactsList);

		setAdapterForAllContactsList(list);

		if (utils.isConnectingToInternet(ConnectionsActivity.this)) {
			// new LoadingContacts().execute("");

			mTask = new LoadingContacts();
			mTask.execute();

		} else {
			utils.showDialog(ConnectionsActivity.this);
		}

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isNotInAllContacts) {
					phoneNumbersAndEmailIds = "";

					ArrayList<Contacts> checkedContactsToAdd = adapter
							.getContactsArrayListChecked();

					if (checkedContactsToAdd.size() > 0) {
						for (int i = 0; i < checkedContactsToAdd.size(); i++) {
							String phoneNumber = checkedContactsToAdd.get(i)
									.getmContactNumber();

							phoneNumbersAndEmailIds = phoneNumber
									+ phoneNumbersAndEmailIds;

							// for (int j = 0; j < contactNumbers.size(); j++) {
							//
							// phoneNumbersAndEmailIds = contactNumbers.get(j)
							// + "," + phoneNumbersAndEmailIds;
							//
							// }
							emailId = checkedContactsToAdd.get(i).getmEmailId();
							if (!emailId.equalsIgnoreCase("")) {
								phoneNumbersAndEmailIds = emailId + ","
										+ phoneNumbersAndEmailIds;
							} else {
								emailId = phoneNumbersAndEmailIds;
							}

						}
						if (utils.isConnectingToInternet(mContext)) {

							new InsertUserConnection(phoneNumbersAndEmailIds,
									checkedContactsToAdd).execute("");
						} else {
							utils.showDialog(mContext);
						}
					}

				} else {
					phoneNumbersAndEmailIds = "";

					ArrayList<Contacts> checkedContactsToAdd = adapter
							.getContactsArrayListChecked();

					if (checkedContactsToAdd.size() > 0) {
						for (int i = 0; i < checkedContactsToAdd.size(); i++) {
							String phoneNumber = checkedContactsToAdd.get(i)
									.getmContactNumber();

							phoneNumbersAndEmailIds = phoneNumber
									+ phoneNumbersAndEmailIds;

							// for (int j = 0; j < contactNumbers.size(); j++) {
							//
							// phoneNumbersAndEmailIds = contactNumbers.get(j)
							// + "," + phoneNumbersAndEmailIds;
							//
							// }
							emailId = checkedContactsToAdd.get(i).getmEmailId();
							if (!emailId.equalsIgnoreCase("")) {
								phoneNumbersAndEmailIds = emailId + ","
										+ phoneNumbersAndEmailIds;
							} else {
								emailId = phoneNumbersAndEmailIds;
							}

						}

						String phoneNumbersOnly[] = phoneNumbersAndEmailIds
								.split(",");
						ArrayList<String> phoneNumberss = new ArrayList<String>();
						ArrayList<String> emailIdsList = new ArrayList<String>();

						for (int i = 0; i < phoneNumbersOnly.length; i++) {

							if (!phoneNumbersOnly[i].contains("@")) {
								phoneNumberss.add(phoneNumbersOnly[i]);
							} else {
								emailIdsList.add(phoneNumbersOnly[i]);
							}

						}

						emailIds = "";
						for (int i = 0; i < emailIdsList.size(); i++) {
							emailIds = emailIdsList.get(i) + "," + emailIds;
						}
						if (phoneNumberss.size() == 0) {
							if (!emailIds.equalsIgnoreCase("")) {
								sendMail(emailIds);
							}
						} else {
							sendSms(phoneNumberss);
						}

					}
				}

			}
		});

		search_contacts = (EditText) findViewById(R.id.search_contacts);

		search_contacts.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				String text = search_contacts.getText().toString()
						.toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		search_contacts.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub

				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_DONE)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(
							search_contacts.getWindowToken(), 0);
				}
				return false;
			}
		});

		backBtn = (Button) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});

		show_connections = (Button) findViewById(R.id.show_connections);
		show_connections.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConnectionsActivity.this,
						DeleteMyConnections.class);

				startActivity(intent);
				finish();
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 100) {

			if (!emailIds.equalsIgnoreCase("")) {
				sendMail(emailIds);
			}

		}

	}

	public void initAll() {
		isNotInAllContacts = false;

		buttons.setPushedButtonIndex(1);

		Button button0 = (Button) buttons.getChildAt(0);
		button0.setTextColor(Color.parseColor("#0D5F62"));
		Button button1 = (Button) buttons.getChildAt(1);
		button1.setTextColor(Color.WHITE);
		Button button2 = (Button) buttons.getChildAt(2);
		button2.setTextColor(Color.parseColor("#0D5F62"));

		addBtn.setVisibility(View.VISIBLE);

		background_text_image.setVisibility(View.GONE);

		// contactsList = getContactsFromDB();
		contactsList.clear();
		contactsList.addAll(getContactsFromDB());
		Collections.sort(contactsList);

		List<Row> list = loadListContacts(contactsList);

		setAdapterForAllContactsList(list);
	}

	public void initIsoContacts() {

		isNotInAllContacts = true;

		Button button0 = (Button) buttons.getChildAt(0);
		button0.setTextColor(Color.WHITE);
		Button button1 = (Button) buttons.getChildAt(1);
		button1.setTextColor(Color.parseColor("#0D5F62"));

		Button button2 = (Button) buttons.getChildAt(2);
		button2.setTextColor(Color.parseColor("#0D5F62"));
		// Toast.makeText(MainActivity.this, "Sort by recent",
		// Toast.LENGTH_SHORT).show();

		addBtn.setVisibility(View.VISIBLE);

		// isoContactsList.clear();
		// isoContactsList.addAll(getISOContactsFromDB());

		if (isoContactsList.size() > 0) {

			background_text_image.setVisibility(View.GONE);
			Collections.sort(isoContactsList);

			loadListIsoContacts(isoContactsList);
		} else {
			background_text_image.setVisibility(View.VISIBLE);
			background_empty_text_message.setText(getResources().getString(
					R.string.background_iso_text));
		}

	}

	public void initConnectedList() {
		Button button0 = (Button) buttons.getChildAt(0);
		button0.setTextColor(Color.parseColor("#0D5F62"));
		Button button1 = (Button) buttons.getChildAt(1);
		button1.setTextColor(Color.parseColor("#0D5F62"));
		Button button2 = (Button) buttons.getChildAt(2);
		button2.setTextColor(Color.WHITE);

		addBtn.setVisibility(View.GONE);

		// connectedContactsList.clear();
		// connectedContactsList.addAll(getConnectedContactsFromDB());

		if (connectedContactsList.size() > 0) {
			background_text_image.setVisibility(View.GONE);

			Collections.sort(connectedContactsList);
			loadListConnectedContacts(connectedContactsList);
		} else {
			background_text_image.setVisibility(View.VISIBLE);
			background_empty_text_message.setText(getResources().getString(
					R.string.background_connected_text));
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mGestureDetector.onTouchEvent(event)) {
			return true;
		} else {
			return false;
		}
	}

	public void updateList() {
		LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
		sideIndex.removeAllViews();
		indexListSize = alphabet.size();
		if (indexListSize < 1) {
			return;
		}

		int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
		int tmpIndexListSize = indexListSize;
		while (tmpIndexListSize > indexMaxSize) {
			tmpIndexListSize = tmpIndexListSize / 2;
		}
		double delta;
		if (tmpIndexListSize > 0) {
			delta = indexListSize / tmpIndexListSize;
		} else {
			delta = 1;
		}

		TextView tmpTV;
		for (double i = 1; i <= indexListSize; i = i + delta) {
			Object[] tmpIndexItem = alphabet.get((int) i - 1);
			String tmpLetter = tmpIndexItem[0].toString();

			tmpTV = new TextView(this);
			tmpTV.setText(tmpLetter);
			tmpTV.setGravity(Gravity.CENTER);
			tmpTV.setTextSize(15);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1);
			tmpTV.setLayoutParams(params);
			sideIndex.addView(tmpTV);
		}

		sideIndexHeight = sideIndex.getHeight();

		sideIndex.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// now you know coordinates of touch
				sideIndexX = event.getX();
				sideIndexY = event.getY();

				// and can display a proper item it country list
				displayListItem();

				return false;
			}
		});
	}

	public void displayListItem() {
		LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
		sideIndexHeight = sideIndex.getHeight();
		// compute number of pixels for every side index item
		double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

		// compute the item index for given event position belongs to
		int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

		// get the item (we can do it since we know item index)
		if (itemPosition < alphabet.size()) {
			Object[] indexItem = alphabet.get(itemPosition);
			int subitemPosition = sections.get(indexItem[0]);

			// ListView listView = (ListView) findViewById(android.R.id.list);
			getListView().setSelection(subitemPosition);

		}
	}

	private ArrayList<Contacts> readContacts() {
		// TODO Auto-generated method stub
		final ArrayList<Contacts> list = new ArrayList<Contacts>();
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);

		String phone = null;
		String emailContact = null;

		if (cur.getCount() > 0) {

			while (cur.moveToNext()) {

				if (mTask.isCancelled())
					return null;

				Contacts contactData = new Contacts();

				String id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));

				contactData.setContactId(id);

				String updated = "";

				long contactId = Long.parseLong(id);

				if (Build.VERSION.SDK_INT <= 18) {
					contactData.setUpdatedTime(updated);
				} else {
					int col = cur
							.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);
					updated = cur.getString(col);

					contactData.setUpdatedTime(updated);
				}

				if (mTask.isCancelled())
					return null;
				if (isContactExists(id)) {

					if (mTask.isCancelled())
						return null;
					String updatedTimeStamp = getUpdatedTimeStamp(id);
					if (updatedTimeStamp != null)

						if (!updatedTimeStamp.equalsIgnoreCase(updated)) {
							String name = cur
									.getString(cur
											.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

							contactData.setmContactName(name);

							if (Build.VERSION.SDK_INT <= 11) {
								Uri uri = getPhotoUri(contactId);

								if (uri != null) {
									// contactData.setPhoto(uri);
									contactData.setContactImage(uri.toString());

								} else {
									contactData.setContactImage("NA");
								}
							} else {
								String image_uri = cur
										.getString(cur
												.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

								if (image_uri != null) {
									contactData.setContactImage(image_uri);
								} else {
									contactData.setContactImage("NA");
								}
							}

							if (Integer
									.parseInt(cur.getString(cur
											.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
								System.out.println("name : " + name + ", ID : "
										+ id);
								if (name.equalsIgnoreCase("Client")) {
									System.out.println("in");
								}

								Cursor pCur = cr
										.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
												null,
												ContactsContract.CommonDataKinds.Phone.CONTACT_ID
														+ " = ?",
												new String[] { id }, null);
								ArrayList<String> phoneNumbers = new ArrayList<String>();
								String finalNumbersAdd = "";
								while (pCur.moveToNext()) {
									phone = pCur
											.getString(pCur
													.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

									String phoneNumbersAdd = phone.replace(" ",
											"");

									finalNumbersAdd = phoneNumbersAdd + ","
											+ finalNumbersAdd;

									contactData
											.setmContactNumber(finalNumbersAdd);
									System.out.println("phone : " + phone
											+ ", ID : " + id);
									phoneNumbers.add(phone.replace(" ", ""));

									contactData.setmNumbersList(phoneNumbers);

									checkUsersList.add(phone.replace(" ", ""));

								}

								pCur.close();
								Cursor emailCur = cr
										.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
												null,
												ContactsContract.CommonDataKinds.Email.CONTACT_ID
														+ " = ?",
												new String[] { id }, null);
								while (emailCur.moveToNext()) {
									emailContact = emailCur
											.getString(emailCur
													.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

									contactData.setmEmailId(emailContact);
									System.out.println("emailContact : "
											+ emailContact + ", ID : " + id);

									// finalNumbersAdd = emailContact +
									// ","
									// +
									// finalNumbersAdd;
									// contactData.setPhoneAndEmailIds(finalNumbersAdd);
									checkUsersList.add(emailContact);
								}

								emailCur.close();

								list.add(contactData);

								updateContacts(contactData);

							}

						}

				} else {
					if (mTask.isCancelled())
						return null;
					String name = cur
							.getString(cur
									.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

					contactData.setmContactName(name);

					if (Build.VERSION.SDK_INT <= 11) {
						Uri uri = getPhotoUri(contactId);

						if (uri != null) {
							contactData.setPhoto(uri);
						}
					} else {
						String image_uri = cur
								.getString(cur
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

						if (image_uri != null) {
							contactData.setContactImage(image_uri);
						} else {
							contactData.setContactImage("NA");
						}
					}

					if (Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
						System.out.println("name : " + name + ", ID : " + id);
						if (name.equalsIgnoreCase("Client")) {
							System.out.println("in");
						}

						Cursor pCur = cr
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);
						ArrayList<String> phoneNumbers = new ArrayList<String>();
						String finalNumbersAdd = "";
						while (pCur.moveToNext()) {
							phone = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

							String phoneNumbersAdd = phone.replace(" ", "");

							finalNumbersAdd = phoneNumbersAdd + ","
									+ finalNumbersAdd;

							contactData.setmContactNumber(finalNumbersAdd);
							System.out.println("phone : " + phone + ", ID : "
									+ id);
							phoneNumbers.add(phone.replace(" ", ""));

							contactData.setmNumbersList(phoneNumbers);

							checkUsersList.add(phone.replace(" ", ""));

						}

						pCur.close();
						Cursor emailCur = cr
								.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Email.CONTACT_ID
												+ " = ?", new String[] { id },
										null);
						while (emailCur.moveToNext()) {
							emailContact = emailCur
									.getString(emailCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

							contactData.setmEmailId(emailContact);
							System.out.println("emailContact : " + emailContact
									+ ", ID : " + id);

							// finalNumbersAdd = emailContact + ","
							// +
							// finalNumbersAdd;
							// contactData.setPhoneAndEmailIds(finalNumbersAdd);
							checkUsersList.add(emailContact);
						}
						emailCur.close();
						list.add(contactData);

						insertContacts(contactData);

					}

				}

			}

		}
		cur.close();

		return list;

	}

	public void insertContacts(Contacts contacts) {

		String contact_id = contacts.getContactId();
		String contact_name = contacts.getmContactName();
		String contact_number = contacts.getmContactNumber();
		String contact_updated_time = contacts.getUpdatedTime();
		String contact_mail = contacts.getmEmailId();
		String contact_icon = contacts.getContactImage();

		db.open();

		// if (db.isOpen()) {
		db.insertContacts(contact_id, contact_name, contact_mail,
				contact_number, contact_updated_time, contact_icon);
		// }

		db.close();

	}

	public void updateContacts(Contacts contacts) {
		String contact_id = contacts.getContactId();
		String contact_name = contacts.getmContactName();
		String contact_number = contacts.getmContactNumber();
		String contact_updated_time = contacts.getUpdatedTime();
		String contact_mail = contacts.getmEmailId();
		String contact_icon = contacts.getContactImage();

		db.open();
		// if (db.isOpen()) {
		db.updateContacts(contact_id, contact_name, contact_mail,
				contact_number, contact_updated_time, contact_icon);
		// }

		db.close();
	}

	public boolean isContactExists(String contact_id) {

		boolean isExists = false;

		db.open();

		// if (db.isOpen()) {

		isExists = db.isContactExists(contact_id);
		// }

		db.close();

		return isExists;

	}

	public String getUpdatedTimeStamp(String contact_id) {

		String updatedTime = null;
		db.open();

		// if (db.isOpen()) {

		try {
			Cursor contacts = db.getContactUpdatedTime(contact_id);

			if (contacts.moveToFirst()) {
				do {

					updatedTime = contacts.getString(0);

				} while (contacts.moveToNext());
			}
			contacts.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		// }

		db.close();
		return updatedTime;

	}

	public void updateISOStatus(String contactID, String isoStatus) {
		db.open();
		// if (db.isOpen()) {
		db.updateISOStatus(contactID, isoStatus);
		// }

		db.close();
	}

	private class CheckUserConnections extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (isContactsTableExists) {
				progressDialog.setMessage("Please Wait...");
				progressDialog.setCancelable(false);
				progressDialog.show();
			}

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// ***** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConnectionsActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.USER_LIST, new StringBody(
						checkUsers));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.CHECK_USER_CONNECTION);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);
			if (isContactsTableExists) {
				if (progressDialog != null && progressDialog.isShowing()) {
					try {
						progressDialog.dismiss();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			try {

				connectionsArrayList = new ArrayList<Contacts>();

				JSONArray checkedContactsArray = jsonResponse
						.getJSONArray("data");

				for (int i = 0; i < checkedContactsArray.length(); i++) {

					JSONObject userContacts = checkedContactsArray
							.getJSONObject(i);

					String mUserContact = userContacts.getString("usercontact");

					for (int j = 0; j < contactsList.size(); j++) {

						// String numbers[] = contactsList.get(j)
						// .getmContactNumber().split(",");
						String numbers[] = null;

						// ***** to check for maild id and phone numbers inslide
						// out values to give color *******///

						if (mUserContact.contains("@")) {
							if (contactsList.get(j).getmEmailId() != null)
								numbers = contactsList.get(j).getmEmailId()
										.split(",");
						} else {
							if (contactsList.get(j).getmContactNumber() != null)
								numbers = contactsList.get(j)
										.getmContactNumber().split(",");
						}
						if (numbers != null)
							for (int m = 0; m < numbers.length; m++) {
								if (numbers[m].equalsIgnoreCase(mUserContact)) {

									String mInslide = userContacts
											.getString("inslide");

									if (mInslide.equalsIgnoreCase("1")) {

										// contactsList.get(j).setCheckedUser(true);
										contactsList.get(j).setmTextColor(
												"green");

										String contactID = contactsList.get(j)
												.getContactId();

										updateISOStatus(contactID, "1");

										if (!connectionsArrayList
												.contains(contactsList.get(j))) {
											connectionsArrayList
													.add(contactsList.get(j));
										}

									} else if (mInslide.equalsIgnoreCase("2")) {

										contactsList.get(j).setmTextColor(
												"green");
										// contactsList.get(j).setCheckedUser(false);

										String contactID = contactsList.get(j)
												.getContactId();

										updateISOStatus(contactID, "2");

										System.err.println(contactsList);
									} else {
										if (contactsList.get(j).getmTextColor() == null) {
											// contactsList.get(j).setCheckedUser(
											// false);
											contactsList.get(j).setmTextColor(
													"red");

											String contactID = contactsList
													.get(j).getContactId();

											updateISOStatus(contactID, "0");
										}

									}

								}
							}

					}

				}

				// adapter.notifyDataSetChanged();

				if (mTask.isCancelled())
					return;

				// contactsList = getContactsFromDB();

				if (contactsList != null) {

					contactsList.clear();
					contactsList.addAll(getContactsFromDB());

					Collections.sort(contactsList);

					List<Row> list = loadListContacts(contactsList);

					allContactsArrayList.clear();
					allContactsArrayList.addAll(list);

					adapter.notifyDataSetChanged();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public List<Row> loadListContacts(List<Contacts> contactsList) {
		List<Row> rows = new ArrayList<Row>();
		int start = 0;
		int end = 0;
		String previousLetter = null;
		Object[] tmpIndexItem = null;
		Pattern numberPattern = Pattern.compile("[0-9]");

		for (Contacts contact : contactsList) {
			String firstLetter = contact.getmContactName().substring(0, 1)
					.toUpperCase();

			// Group numbers together in the scroller
			if (numberPattern.matcher(firstLetter).matches()) {
				firstLetter = "#";
			}

			// If we've changed to a new letter, add the previous letter to the
			// alphabet scroller
			if (previousLetter != null && !firstLetter.equals(previousLetter)) {
				end = rows.size() - 1;
				tmpIndexItem = new Object[3];
				tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
				tmpIndexItem[1] = start;
				tmpIndexItem[2] = end;
				alphabet.add(tmpIndexItem);

				start = end + 1;
			}

			// Check if we need to add a header row
			if (!firstLetter.equals(previousLetter)) {
				rows.add(new Section(firstLetter));
				sections.put(firstLetter, start);

			}

			// Add the country to the list

			// rows.add(new Item(contact.getmContactName()));

			rows.add(new Item(contact));

			previousLetter = firstLetter;
		}

		if (previousLetter != null) {
			// Save the last letter
			tmpIndexItem = new Object[3];
			tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
			tmpIndexItem[1] = start;
			tmpIndexItem[2] = rows.size() - 1;
			alphabet.add(tmpIndexItem);
		}

		return rows;
	}

	public void setAdapterForAllContactsList(List<Row> list) {

		allContactsArrayList.clear();
		allContactsArrayList.addAll(list);
		adapter.setRows(allContactsArrayList, ConnectionsActivity.this, false);
		setListAdapter(adapter);

		updateList();
	}

	public void loadListIsoContacts(List<Contacts> contactsList) {
		List<Row> rows = new ArrayList<Row>();
		int start = 0;
		int end = 0;
		String previousLetter = null;
		Object[] tmpIndexItem = null;
		Pattern numberPattern = Pattern.compile("[0-9]");

		for (Contacts contact : contactsList) {
			String firstLetter = contact.getmContactName().substring(0, 1)
					.toUpperCase();

			// Group numbers together in the scroller
			if (numberPattern.matcher(firstLetter).matches()) {
				firstLetter = "#";
			}

			// If we've changed to a new letter, add the previous letter to the
			// alphabet scroller
			if (previousLetter != null && !firstLetter.equals(previousLetter)) {
				end = rows.size() - 1;
				tmpIndexItem = new Object[3];
				tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
				tmpIndexItem[1] = start;
				tmpIndexItem[2] = end;
				alphabet.add(tmpIndexItem);

				start = end + 1;
			}

			// Check if we need to add a header row
			if (!firstLetter.equals(previousLetter)) {
				rows.add(new Section(firstLetter));
				sections.put(firstLetter, start);

			}

			// Add the country to the list

			// rows.add(new Item(contact.getmContactName()));

			rows.add(new Item(contact));

			previousLetter = firstLetter;
		}

		if (previousLetter != null) {
			// Save the last letter
			tmpIndexItem = new Object[3];
			tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
			tmpIndexItem[1] = start;
			tmpIndexItem[2] = rows.size() - 1;
			alphabet.add(tmpIndexItem);
		}

		adapter.setRows(rows, ConnectionsActivity.this, false);
		setListAdapter(adapter);

		updateList();
	}

	public void loadListConnectedContacts(List<Contacts> contactsList) {
		List<Row> rows = new ArrayList<Row>();
		int start = 0;
		int end = 0;
		String previousLetter = null;
		Object[] tmpIndexItem = null;
		Pattern numberPattern = Pattern.compile("[0-9]");

		for (Contacts contact : contactsList) {
			String firstLetter = contact.getmContactName().substring(0, 1)
					.toUpperCase();

			// Group numbers together in the scroller
			if (numberPattern.matcher(firstLetter).matches()) {
				firstLetter = "#";
			}

			// If we've changed to a new letter, add the previous letter to the
			// alphabet scroller
			if (previousLetter != null && !firstLetter.equals(previousLetter)) {
				end = rows.size() - 1;
				tmpIndexItem = new Object[3];
				tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
				tmpIndexItem[1] = start;
				tmpIndexItem[2] = end;
				alphabet.add(tmpIndexItem);

				start = end + 1;
			}

			// Check if we need to add a header row
			if (!firstLetter.equals(previousLetter)) {
				rows.add(new Section(firstLetter));
				sections.put(firstLetter, start);

			}

			// Add the country to the list

			// rows.add(new Item(contact.getmContactName()));

			rows.add(new Item(contact));

			previousLetter = firstLetter;
		}

		if (previousLetter != null) {
			// Save the last letter
			tmpIndexItem = new Object[3];
			tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
			tmpIndexItem[1] = start;
			tmpIndexItem[2] = rows.size() - 1;
			alphabet.add(tmpIndexItem);
		}

		adapter.setRows(rows, ConnectionsActivity.this, true);
		setListAdapter(adapter);

		updateList();
	}

	private class InsertUserConnection extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String userList;
		ArrayList<Contacts> checkedContactsToAdd;

		public InsertUserConnection(String userList,
				ArrayList<Contacts> checkedContactsToAdd) {
			// TODO Auto-generated constructor stub

			this.userList = userList;
			this.checkedContactsToAdd = checkedContactsToAdd;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ConnectionsActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.USER_LIST, new StringBody(
						userList));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.UPDATE_USER_CONNECTION);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equalsIgnoreCase(
						"success")) {
					Toast.makeText(ConnectionsActivity.this,
							"Mail Invitation(s) Sent", Toast.LENGTH_SHORT);

					for (int i = 0; i < checkedContactsToAdd.size(); i++) {
						String contactID = checkedContactsToAdd.get(i)
								.getContactId();
						updateISOStatus(contactID, "1");
					}

					initAll();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void sendMail(String emailIds) {
		String[] mailIds = emailIds.split(",");

		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, mailIds);
		intent.putExtra(Intent.EXTRA_SUBJECT, "InSlideOut");
		intent.putExtra(Intent.EXTRA_TEXT,
				"Hi. Join me on InSlideOut. http://goo.gl/DhVSHs");
		final PackageManager pm = getPackageManager();
		final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
		ResolveInfo best = null;
		for (final ResolveInfo info : matches)
			if (info.activityInfo.packageName.endsWith(".gm")
					|| info.activityInfo.name.toLowerCase().contains("gmail"))
				best = info;
		if (best != null)
			intent.setClassName(best.activityInfo.packageName,
					best.activityInfo.name);

		startActivity(intent);
	}

	public void sendSms(ArrayList<String> phoneNumberss) {

		String numbers = "";
		for (int i = 0; i < phoneNumberss.size(); i++) {
			numbers = phoneNumberss.get(i) + "," + numbers;
		}

		Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"
				+ numbers));
		smsIntent.putExtra("sms_body",

		"Hi. Join me on InSlideOut. http://goo.gl/DhVSHs");
		startActivityForResult(smsIntent, 100);
	}

	public Uri getPhotoUri(long contactId) {
		try {
			Cursor cur = this
					.getContentResolver()
					.query(ContactsContract.Data.CONTENT_URI,
							null,
							ContactsContract.Data.CONTACT_ID
									+ "="
									+ contactId
									+ " AND "
									+ ContactsContract.Data.MIMETYPE
									+ "='"
									+ ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
									+ "'", null, null);
			if (cur != null) {
				if (!cur.moveToFirst()) {
					return null; // no photo
				}
			} else {
				return null; // error in cursor process
			}
			cur.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		Uri person = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, contactId);
		return Uri.withAppendedPath(person,
				ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void removeDuplicates(List list) {
		Set uniqueEntries = new HashSet();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (!uniqueEntries.add(element)) // if current element is a
												// duplicate,
				iter.remove(); // remove it
		}
	}

	private class LoadingContacts extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			if (isContactsTableExists) {
				progressDialog
						.setMessage("Please wait while we synchronize your connections");
				progressDialog.setCancelable(false);
				progressDialog.show();
			}

		}

		@Override
		protected Void doInBackground(String... params) {

			// contactsList = readContacts();
			contactsList.clear();

			ArrayList<Contacts> list = new ArrayList<Contacts>();

			list = readContacts();
			if (list != null) {
				contactsList.addAll(list);
			}

			// readContacts();
			// contactsList = getContactsFromDB();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (isContactsTableExists) {
				if (progressDialog != null && progressDialog.isShowing()) {
					try {
						progressDialog.dismiss();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			Collections.sort(contactsList);

			removeDuplicates(checkUsersList);

			for (int i = 0; i < checkUsersList.size(); i++) {
				checkUsers = checkUsersList.get(i) + "," + checkUsers;
			}

			if (!mTask.isCancelled()) {
				if (utils.isConnectingToInternet(mContext)) {
					new CheckUserConnections().execute("");
				} else {
					utils.showDialog(mContext);
				}
			} else {
				return;
			}

		}
	}

	public boolean isContactsTableExists() {
		boolean isExists = false;
		// if (db.isOpen()) {

		db.open();
		isExists = db.isContactsTableExists();
		// }
		db.close();
		return isExists;
	}

	public ArrayList<Contacts> getContactsFromDB() {
		ArrayList<Contacts> list = new ArrayList<Contacts>();

		isoContactsList.clear();
		connectedContactsList.clear();

		db.open();

		if (db.isOpen()) {
			Cursor cursor = db.getContacts();
			if (cursor.moveToFirst()) {
				do {
					String contactId = cursor.getString(0);
					String contactName = cursor.getString(1);
					String contactMail = cursor.getString(2);
					String contactNumber = cursor.getString(3);
					String contactUpdatedTime = cursor.getString(4);
					String contactISOStatus = cursor.getString(5);
					String contactIcon = cursor.getString(6);

					Contacts contacts = new Contacts();
					contacts.setContactId(contactId);
					contacts.setmContactName(contactName);
					contacts.setmEmailId(contactMail);
					contacts.setmContactNumber(contactNumber);
					contacts.setUpdatedTime(contactUpdatedTime);

					contacts.setContactImage(contactIcon);

					if (contactISOStatus == null) {
						contacts.setIsoStatus("0");
					}else{
						contacts.setIsoStatus(contactISOStatus);
					}
					list.add(contacts);

					if (contactISOStatus != null) {
						if (contactISOStatus.equalsIgnoreCase("1")) {
							contacts.setIsoStatus(contactISOStatus);
							connectedContactsList.add(contacts);

						}
						if (contactISOStatus.equalsIgnoreCase("1")
								|| contactISOStatus.equalsIgnoreCase("2")) {
							contacts.setIsoStatus(contactISOStatus);
							isoContactsList.add(contacts);
						}
					} else {

					}

				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		db.close();

		return list;

	}

	// public ArrayList<Contacts> getISOContactsFromDB() {
	// ArrayList<Contacts> list = new ArrayList<Contacts>();
	// db.open();
	//
	// // if (db.isOpen()) {
	// Cursor cursor = db.getISOContacts();
	// if (cursor.moveToFirst()) {
	// do {
	// String contactId = cursor.getString(0);
	// String contactName = cursor.getString(1);
	// String contactMail = cursor.getString(2);
	// String contactNumber = cursor.getString(3);
	// String contactUpdatedTime = cursor.getString(4);
	// String contactISOStatus = cursor.getString(5);
	// String contactIcon = cursor.getString(6);
	//
	// Contacts contacts = new Contacts();
	// contacts.setContactId(contactId);
	// contacts.setmContactName(contactName);
	// contacts.setmEmailId(contactMail);
	// contacts.setmContactNumber(contactNumber);
	// contacts.setUpdatedTime(contactUpdatedTime);
	// contacts.setIsoStatus(contactISOStatus);
	// contacts.setContactImage(contactIcon);
	// list.add(contacts);
	// } while (cursor.moveToNext());
	// }
	// cursor.close();
	// // }
	//
	// db.close();
	// return list;
	//
	// }

	// public ArrayList<Contacts> getConnectedContactsFromDB() {
	// ArrayList<Contacts> list = new ArrayList<Contacts>();
	// db.open();
	//
	// // if (db.isOpen()) {
	// Cursor cursor = db.getConnectedContacts();
	// if (cursor.moveToFirst()) {
	// do {
	// String contactId = cursor.getString(0);
	// String contactName = cursor.getString(1);
	// String contactMail = cursor.getString(2);
	// String contactNumber = cursor.getString(3);
	// String contactUpdatedTime = cursor.getString(4);
	// String contactISOStatus = cursor.getString(5);
	// String contactIcon = cursor.getString(6);
	//
	// Contacts contacts = new Contacts();
	// contacts.setContactId(contactId);
	// contacts.setmContactName(contactName);
	// contacts.setmEmailId(contactMail);
	// contacts.setmContactNumber(contactNumber);
	// contacts.setUpdatedTime(contactUpdatedTime);
	// contacts.setIsoStatus(contactISOStatus);
	// contacts.setContactImage(contactIcon);
	// list.add(contacts);
	// } while (cursor.moveToNext());
	// }
	// cursor.close();
	// // }
	//
	// db.close();
	//
	// return list;
	//
	// }

}
