package com.inslideout;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class MyProfileFragment extends Fragment {

	private EditText firstName, lastName, mobileNumber, emailAddress, zipCode;

	private EditText old_password, new_password, retype_password;
	private Button cancelBtn, submitBtn;

	public ProgressDialog progressDialog;

	private Dialog dialog;

	ImageButton editBtn;

	Button changePassword;

	private Context mContext;
	private Utils utils = new Utils();

	private boolean editOption = false;

	public MyProfileFragment(ImageButton editButton) {
		editBtn = editButton;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.my_profile_fragment, null);
		
		mContext = this.getActivity();

		progressDialog = new ProgressDialog(this.getActivity());

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		LinearLayout layout_parent = (LinearLayout) view
				.findViewById(R.id.layout_parent);

		Utils.setupUI(layout_parent, getActivity());

		if (utils.isConnectingToInternet(mContext)) {
			new GetUserDetails().execute("");
		} else {
			utils.showDialog(mContext);
		}

		firstName = (EditText) view.findViewById(R.id.firstName);
		lastName = (EditText) view.findViewById(R.id.lastName);
		mobileNumber = (EditText) view.findViewById(R.id.mobileNumber);
		emailAddress = (EditText) view.findViewById(R.id.emailAddress);
		zipCode = (EditText) view.findViewById(R.id.zipCode);

		changePassword = (Button) view.findViewById(R.id.changePassword);

		changePassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// custom dialog
				dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				dialog.setContentView(R.layout.change_password);
				dialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(android.graphics.Color.TRANSPARENT));

				old_password = (EditText) dialog
						.findViewById(R.id.old_password);
				new_password = (EditText) dialog
						.findViewById(R.id.new_password);
				retype_password = (EditText) dialog
						.findViewById(R.id.retype_password);

				cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
				submitBtn = (Button) dialog.findViewById(R.id.submitBtn);

				cancelBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

				submitBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (!old_password.getText().toString()
								.equalsIgnoreCase("")) {
							if (!new_password.getText().toString()
									.equalsIgnoreCase("")) {
								if (!retype_password.getText().toString()
										.equalsIgnoreCase("")) {
									// change passsword code will be here
									if (new_password
											.getText()
											.toString()
											.equalsIgnoreCase(
													retype_password.getText()
															.toString())) {

										if (utils
												.isConnectingToInternet(mContext)) {
											new UpdatePasswordTask()
													.execute("");
										} else {
											utils.showDialog(mContext);
										}

									} else {
										Toast.makeText(
												getActivity(),
												"new password and  retype password mismatch",
												Toast.LENGTH_SHORT).show();
									}

								} else {
									Toast.makeText(getActivity(),
											"Enter retype password",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(getActivity(),
										"Enter new password",
										Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(getActivity(), "Enter old password",
									Toast.LENGTH_SHORT).show();
						}
					}
				});

				dialog.show();

			}
		});

		editBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!editOption) {

					// Toast.makeText(getActivity(), "edit button clicked",
					// Toast.LENGTH_SHORT).show();

					editOption = true;

					editBtn.setImageResource(R.drawable.add);

					enableAllFields();

				} else {

					if (!firstName.getText().toString().equalsIgnoreCase("")) {
						if (!lastName.getText().toString().equalsIgnoreCase("")) {
							if (!mobileNumber.getText().toString()
									.equalsIgnoreCase("")) {
								if (!emailAddress.getText().toString()
										.equalsIgnoreCase("")) {
									if (!zipCode.getText().toString()
											.equalsIgnoreCase("")) {
										// update user details

										if (utils
												.isConnectingToInternet(mContext)) {
											new UpdateUserDetails().execute("");
										} else {
											utils.showDialog(mContext);
										}

									} else {
										Toast.makeText(getActivity(),
												"Please enter zipcode",
												Toast.LENGTH_SHORT).show();
									}

								} else {
									Toast.makeText(getActivity(),
											"Please enter email id",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(getActivity(),
										"Please enter mobile number",
										Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(getActivity(),
									"Please enter last name",
									Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getActivity(),
								"Please enter first name", Toast.LENGTH_SHORT)
								.show();
					}

				}

			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	public void enableAllFields() {
		firstName.setEnabled(true);
		lastName.setEnabled(true);
		mobileNumber.setEnabled(true);
		emailAddress.setEnabled(true);
		zipCode.setEnabled(true);

		changePassword.setVisibility(View.VISIBLE);
	}

	public void disableAllFields() {
		firstName.setEnabled(false);
		lastName.setEnabled(false);
		mobileNumber.setEnabled(false);
		emailAddress.setEnabled(false);
		zipCode.setEnabled(false);

		changePassword.setVisibility(View.GONE);
	}

	private class GetUserDetails extends AsyncTask<String, Void, Void> {

		String response;
		JSONObject jsonResponse = new JSONObject();

		String mMobileNumber = null;
		String mEmailId = null;
		String mFirstName = null;
		String mLastName = null;
		String mZipCode = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// progressDialog = new ProgressDialog(LoginActivity.this);
			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {
				// *** Add The JSON to the Entity ********
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								getActivity(), RequestParameters.USER_ID)));

				System.out.println();
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}

			try {

				String userId = Global.getInstance().getPreferenceVal(
						getActivity(), RequestParameters.USER_ID);
				// jsonObject = PostingDataToServer.getJSONFromServer(entity,
				// ApplicationAPIs.GET_USER);

				String url = ApplicationAPIs.GET_USER_BY_ID + "?userid="
						+ userId;
				jsonResponse = PostingDataToServer.getJSONFromUrl(url);
				System.out.println("Get user" + jsonResponse);

			} catch (Exception e) {

				System.out
						.println("EXception occured in LoginActivity after getting response from server");
				e.printStackTrace();
			}

			// ********* Parse the response "response" **************
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				response = jsonResponse.getString("errorMsg");

				if (response.equalsIgnoreCase("success")) {
					mMobileNumber = jsonResponse.getString("contactnumber");
					mEmailId = jsonResponse.getString("email");
					mZipCode = jsonResponse.getString("zipcode");
					mLastName = jsonResponse.getString("lastname");
					mFirstName = jsonResponse.getString("firstname");

					firstName.setText(mFirstName);
					lastName.setText(mLastName);
					mobileNumber.setText(mMobileNumber);
					emailAddress.setText(mEmailId);
					zipCode.setText(mZipCode);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class UpdatePasswordTask extends AsyncTask<String, Void, Void> {

		String response;
		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// progressDialog = new ProgressDialog(LoginActivity.this);
			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {
				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								getActivity(), RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.OLD_PASSWORD, new StringBody(
						old_password.getText().toString()));

				entity.addPart(RequestParameters.NEW_PASSWORD, new StringBody(
						new_password.getText().toString()));
				System.out.println();
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}

			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.UPDATE_PASSWORD);

				System.out.println("Get user" + jsonResponse);

			} catch (Exception e) {

				System.out
						.println("EXception occured in LoginActivity after getting response from server");
				e.printStackTrace();
			}

			// ********* Parse the response "response" **************
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				response = jsonResponse.getString("errorMsg");
				if (response.equalsIgnoreCase("success")) {
					Toast.makeText(getActivity(),
							"your password has been updated successfully",
							Toast.LENGTH_SHORT).show();
					dialog.dismiss();
				} else if (response.equalsIgnoreCase("error")) {

					old_password.setText("");
					new_password.setText("");
					retype_password.setText("");

					Toast.makeText(getActivity(), "Entered the wrong password",
							Toast.LENGTH_SHORT).show();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class UpdateUserDetails extends AsyncTask<String, Void, Void> {

		String response = "";

		JSONObject jsonResponse = new JSONObject();

		// String alreadyExisted = "{" + '"' + "errorMsg" + '"' + ":" + '"'
		// + "email already exists" + '"' + "}";

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		// name, email,
		// password,gender,age,nationality,language,longitude,latitude,interestedin,userpic
		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.FIRST_NAME, new StringBody(
						firstName.getText().toString()));

				entity.addPart(RequestParameters.LAST_NAME, new StringBody(
						lastName.getText().toString()));
				entity.addPart(RequestParameters.CONTACT_NUMBER,
						new StringBody(mobileNumber.getText().toString()));

				entity.addPart(RequestParameters.EMAIL, new StringBody(
						emailAddress.getText().toString()));

				entity.addPart(RequestParameters.ZIP_CODE, new StringBody(
						zipCode.getText().toString()));

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								getActivity(), RequestParameters.USER_ID)));

			} catch (UnsupportedEncodingException e) {

				response = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				response = "exception";
				e.printStackTrace();
			}

			// *** Simply Copy Paste ********
			try {

				// response = PostingDataToServer.postJson(entity,
				// ApplicationAPIs.REGISTER_USER);
				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.UPDATE_USER);

				response = jsonResponse.getString("errorMsg");
				System.out.println(response + jsonResponse);

				// System.out.println(response + "value text");

			} catch (Exception e) {
				response = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// {"errorMsg":"emailExists"}
			try {

				if (response.equalsIgnoreCase("success")) {
					Toast.makeText(getActivity(),
							" User details have been updated",
							Toast.LENGTH_LONG).show();

					editBtn.setImageResource(R.drawable.edit);

					editOption = false;

					disableAllFields();

				} else if (response.equalsIgnoreCase("error")) {
					Toast.makeText(getActivity(), "Not updated",
							Toast.LENGTH_LONG).show();
				}

				else {
					System.out.println(response);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
