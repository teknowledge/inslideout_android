package com.inslideout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.inslideout.data.Contacts;
import com.inslideout.database.DataBaseHandler;
import com.inslideout.service.ApplicationAPIs;
import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;
import com.inslideout.service.Utils;
import com.inslideout.webservices.PostingDataToServer;

public class AlphabetListAdapter extends BaseAdapter {

	public Context mContext;

	ArrayList<Item> contacts = new ArrayList<Item>();

	public ArrayList<Contacts> checkedContactsList;

	private ArrayList<Row> arraylist;

	private boolean isConnected;
	public ProgressDialog progressDialog;
	private Utils utils;

	private DataBaseHandler db;

	public static abstract class Row {
	}

	public static final class Section extends Row {
		public final String text;
		public boolean checked;

		public boolean isChecked() {
			return checked;
		}

		public void setChecked(boolean checked) {
			this.checked = checked;

		}

		public Section(String text) {
			this.text = text;

		}
	}

	public static final class Item extends Row {
		public final Contacts text;
		public String letter;

		// public Contacts contact ;

		public String getLetter() {
			return letter;
		}

		public void setLetter(String letter) {
			this.letter = letter;
		}

		public boolean checked;

		public boolean isChecked() {
			return checked;
		}

		public void setChecked(boolean checked) {
			this.checked = checked;
		}

		public Item(Contacts text) {
			this.text = text;

			if (text.isCheckedUser()) {
				checked = true;
			} else {
				checked = false;
			}
		}

	}

	private List<Row> rows;

	public void setRows(List<Row> rows, Context mContext, boolean isConnected) {
		this.rows = rows;
		this.mContext = mContext;

		this.isConnected = isConnected;

		this.arraylist = new ArrayList<Row>();
		this.arraylist.addAll(rows);

		checkedContactsList = new ArrayList<Contacts>();
		utils = new Utils();
		progressDialog = new ProgressDialog(mContext);
		db = new DataBaseHandler(mContext);
	}

	@Override
	public int getCount() {
		return rows.size();
	}

	@Override
	public Row getItem(int position) {
		return rows.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	class ViewHolder {
		TextView textView;
		CheckBox checkbox;
		ImageView contactImage;
		ImageView connected_icon;
		ImageView delete_icon;
	}

	@Override
	public int getItemViewType(int position) {
		if (getItem(position) instanceof Section) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		ViewHolder holder;

		if (getItemViewType(position) == 0) { // Item
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) parent.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.row_item, parent, false);

				holder = new ViewHolder();
				holder.textView = (TextView) view.findViewById(R.id.textView1);
				holder.checkbox = (CheckBox) view.findViewById(R.id.checkbox);
				holder.contactImage = (ImageView) view
						.findViewById(R.id.contact_image);

				holder.connected_icon = (ImageView) view
						.findViewById(R.id.connected_icon);
				holder.delete_icon = (ImageView) view
						.findViewById(R.id.delete_icon);
				holder.checkbox.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						CheckBox cb = (CheckBox) v;
						Item item = (Item) cb.getTag();
						item.setChecked(cb.isChecked());

						if (cb.isChecked()) {

							checkedContactsList.add(item.text);

						} else {

							checkedContactsList.remove(item.text);
						}

					}
				});

				view.setTag(holder);

			} else {
				holder = (ViewHolder) view.getTag();
			}

			final Item item = (Item) getItem(position);

			// checkbox.setChecked(true);

			holder.textView.setText(item.text.getmContactName());

			if (isConnected) {
				if (item.text.getIsoStatus().equalsIgnoreCase("1")) {
					holder.textView.setTextColor(mContext.getResources()
							.getColor(R.color.green));
					holder.checkbox.setVisibility(View.GONE);
					holder.connected_icon.setVisibility(View.GONE);
					holder.delete_icon.setVisibility(View.VISIBLE);
				}
			} else {
				if (item.text.getIsoStatus() != null) {
					if (item.text.getIsoStatus().equalsIgnoreCase("0")) {
						holder.textView.setTextColor(mContext.getResources()
								.getColor(R.color.red));
						holder.checkbox.setVisibility(View.VISIBLE);
						holder.connected_icon.setVisibility(View.GONE);

					} else if (item.text.getIsoStatus().equalsIgnoreCase("1")) {
						holder.textView.setTextColor(mContext.getResources()
								.getColor(R.color.green));
						holder.checkbox.setVisibility(View.GONE);
						holder.connected_icon.setVisibility(View.VISIBLE);

					} else if (item.text.getIsoStatus().equalsIgnoreCase("2")) {
						holder.textView.setTextColor(mContext.getResources()
								.getColor(R.color.green));
						holder.checkbox.setVisibility(View.VISIBLE);
						holder.connected_icon.setVisibility(View.GONE);
					}

				}
			}

			holder.delete_icon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					ArrayList<String> contactNumbers = item.text
							.getmNumbersList();
					String phoneNumbersAndEmailIds = "";

					for (int j = 0; j < contactNumbers.size(); j++) {

						phoneNumbersAndEmailIds = contactNumbers.get(j) + ","
								+ phoneNumbersAndEmailIds;

					}

					String emailId = item.text.getmEmailId();
					if (!emailId.equalsIgnoreCase("")) {
						phoneNumbersAndEmailIds = emailId + ","
								+ phoneNumbersAndEmailIds;
					} else {
						// phoneNumbersAndEmailIds = emailId;
					}

					if (utils.isConnectingToInternet(mContext)) {
						new RemoveConnectionUserList(phoneNumbersAndEmailIds,
								item).execute("");
					} else {
						utils.showDialog(mContext);
					}

				}
			});

			holder.checkbox.setChecked(item.isChecked());

			holder.checkbox.setTag(item);

			if (Build.VERSION.SDK_INT <= 11) {
				if (!item.text.getContactImage().equalsIgnoreCase("NA")) {
					try {
						//holder.contactImage.setImageURI(item.text.getPhoto());
						holder.contactImage.setImageURI(Uri.parse(item.text
								.getContactImage()));
					} catch (Exception e) {
						e.printStackTrace();

					}

				} else {
					holder.contactImage
							.setImageResource(R.drawable.contact_image);
				}
			} else {
				if (!item.text.getContactImage().equalsIgnoreCase("NA")) {
					try {
						holder.contactImage.setImageURI(Uri.parse(item.text
								.getContactImage()));

					} catch (Exception e) {
						e.printStackTrace();

					}

				} else {
					holder.contactImage
							.setImageResource(R.drawable.contact_image);
				}
			}

		} else { // Section
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) parent.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.row_section, parent, false);
				holder = new ViewHolder();
				holder.textView = (TextView) view.findViewById(R.id.textView1);
				holder.checkbox = (CheckBox) view.findViewById(R.id.checkBox1);
				view.setTag(holder);

			} else {
				holder = (ViewHolder) view.getTag();
			}

			Section section = (Section) getItem(position);

			holder.textView.setText(section.text);
			holder.checkbox.setChecked(section.isChecked());

			holder.checkbox.setTag(section);

			holder.checkbox.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					CheckBox cb = (CheckBox) v;
					Section data = (Section) cb.getTag();

					data.setChecked(cb.isChecked());
					checkedItems(data);
					notifyDataSetChanged();
				}
			});
		}

		return view;
	}

	public void checkedItems(Section section) {
		if (section.isChecked()) {
			for (int i = 0; i < rows.size(); i++) {
				if (rows.get(i) instanceof Item) {
					Item item = (Item) rows.get(i);
					if (item.text.getmContactName().substring(0, 1)
							.toUpperCase().equals(section.text.toUpperCase())) {
						item.setChecked(true);
						contacts.add(item);
						// checkedContactsList.add(item.text);

					}

				}
			}
		} else {
			for (int i = 0; i < rows.size(); i++) {
				if (rows.get(i) instanceof Item) {
					Item item = (Item) rows.get(i);
					if (item.text.getmContactName().substring(0, 1)
							.toUpperCase().equals(section.text.toUpperCase())) {
						item.setChecked(false);
						contacts.remove(item);

						// checkedContactsList.remove(item.text);
					}
				}
			}
		}
	}

	public ArrayList<Contacts> getContactsArrayListChecked() {
		for (int i = 0; i < rows.size(); i++) {
			if (rows.get(i) instanceof Item) {
				Item item = (Item) rows.get(i);
				if (item.text.isCheckedUser() && !item.text.isCheckedUser()) {

					checkedContactsList.add(item.text);

				}
			}
		}

		return checkedContactsList;

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		// if (rows == null) {
		// return;
		// }
		rows.clear();
		if (charText.length() == 0) {
			rows.addAll(arraylist);
		} else {

			for (int i = 0; i < arraylist.size(); i++) {
				if (arraylist.get(i) instanceof Item) {
					Item item = (Item) arraylist.get(i);
					if (item.text.getmContactName()
							.toLowerCase(Locale.getDefault())
							.contains(charText)) {

						rows.add(item);

					}
				} else {
					Section item = (Section) arraylist.get(i);
					if (item.text.toLowerCase(Locale.getDefault()).contains(
							charText)) {

						rows.add(item);

					}
				}

			}
		}
		notifyDataSetChanged();
	}

	private class RemoveConnectionUserList extends
			AsyncTask<String, Void, Void> {

		String mResponse = "";
		String connectionStr = "";
		Item item;

		JSONObject jsonResponse = new JSONObject();
		int mPosition;

		public RemoveConnectionUserList(String connectionStr, Item item) {
			this.connectionStr = connectionStr;
			this.item = item;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please Wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								mContext, RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.USER_LIST, new StringBody(

				connectionStr));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.REMOVE_USER_CONNECTION);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				if (jsonResponse.getString("errorMsg").equals("success")) {
					Toast.makeText(mContext,
							"Connection is deleted successfully",
							Toast.LENGTH_LONG).show();

					rows.remove(item);
					notifyDataSetChanged();

					updateISOStatus(item.text.getContactId(), "2");
					
					((ConnectionsActivity) mContext).initAll();
					
				} else {
					Toast.makeText(mContext, "error deleting Connections",
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void updateISOStatus(String contactID, String isoStatus) {
		db.open();
		db.updateISOStatus(contactID, isoStatus);
		db.close();
	}
}
