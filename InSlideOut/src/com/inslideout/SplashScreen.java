package com.inslideout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

import com.inslideout.service.Global;
import com.inslideout.service.RequestParameters;

public class SplashScreen extends Activity {

	String session = "LOGIN";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);

		final String mLogin = Global.getInstance().getPreferenceVal(
				SplashScreen.this, RequestParameters.SESSION);

		Handler handle = new Handler();
		Runnable action = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (session.equals(mLogin)) {
					Intent intent = new Intent(getApplicationContext(),
							MainActivity.class);
					startActivity(intent);
					finish();
				} else {
					Intent intent = new Intent(getApplicationContext(),
							LoginActivity.class);
					startActivity(intent);
					finish();
				}

			}
		};

		handle.postDelayed(action, 2000);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

}
