package com.inslideout.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Global {

	public static Global global = null;
	public Context ctx;

	public static Global getInstance() {
		if (global == null)
			global = new Global();

		return global;
	}

	private Global() {

	}

	public static String PREFERENCE_STORAGE = "Storage_preferances";

	public void storeIntoPreference(Context ctx, String key, String val) {
		SharedPreferences.Editor editor = ctx.getSharedPreferences(
				Global.PREFERENCE_STORAGE, Activity.MODE_PRIVATE).edit();
		editor.putString(key, val);
		editor.commit();
	}

	public String getPreferenceVal(Context ctx, String key) {
		SharedPreferences userPreferences = ctx.getSharedPreferences(
				PREFERENCE_STORAGE, Activity.MODE_PRIVATE);
		if (userPreferences.contains(key))
			return userPreferences.getString(key, "NA");
		else
			return "NA";
	}

	public void deletePreferenceVal(Context ctx, String key) {
		SharedPreferences userPreferences = ctx.getSharedPreferences(
				PREFERENCE_STORAGE, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = ctx.getSharedPreferences(
				Global.PREFERENCE_STORAGE, Activity.MODE_PRIVATE).edit();
		if (userPreferences.contains(key)) {
			editor.remove(key);
			editor.commit();
		}

	}

	public void storeBooleanType(Context ctx, String key, boolean val) {
		SharedPreferences.Editor editor = ctx.getSharedPreferences(
				Global.PREFERENCE_STORAGE, Activity.MODE_PRIVATE).edit();
		editor.putBoolean(key, val);
		editor.commit();
	}

	public boolean getBooleanType(Context ctx, String key) {
		SharedPreferences userPreferences = ctx.getSharedPreferences(
				PREFERENCE_STORAGE, Activity.MODE_PRIVATE);
		if (userPreferences.contains(key))
			return userPreferences.getBoolean(key, false);
		else
			return false;
	}

	// public void storeBooleanType(Context ctx, String key, boolean val) {
	// SharedPreferences.Editor editor = ctx.getSharedPreferences(
	// Global.PREFERENCE_STORAGE, Activity.MODE_PRIVATE).edit();
	// editor.putBoolean(key, val);
	// editor.commit();
	// }
	//
	// public boolean getBooleanType(Context ctx, String key) {
	// SharedPreferences userPreferences = ctx.getSharedPreferences(
	// PREFERENCE_STORAGE, Activity.MODE_PRIVATE);
	// if (userPreferences.contains(key))
	// return userPreferences.getBoolean(key, false);
	// else
	// return false;
	// }
}
