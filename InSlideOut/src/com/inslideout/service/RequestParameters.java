package com.inslideout.service;

public class RequestParameters {

	public static final String SESSION = "session";
	
	public static final String FIRST_TIME = "first";
	/**
	 * 
	 * 
	 * @param Strings
	 *            for http://staging.teks.co.in/construction/index.php/api/
	 *            registerUser & update profile have userID
	 * 
	 */

	public static final String FIRST_NAME = "firstname";

	public static final String LAST_NAME = "lastname";

	public static final String CONTACT_NUMBER = "contactnumber";

	public static final String EMAIL = "email";

	public static final String ZIP_CODE = "zipcode";

	public static final String PASSWORD = "password";

	// for updtate profile

	public static final String USER_ID = "userid";

	// ****for change password*****///

	public static final String OLD_PASSWORD = "oldpassword";

	public static final String NEW_PASSWORD = "newpassword";

	// for search sports teams api parameters

	public static final String CATEGORY = "category";

	public static final String IS_PRO = "ispro";

	public static final String SEARCH_TEXT = "searchtext";

	public static final String TEAM_LIST = "teamlist";

	public static final String TEAM_ID = "teamid";

	public static final String ARTIST_LIST = "artistlist";

	public static final String ARTIST_ID = "artistid";

	public static final String LEVEL = "level";

	public static final String EVENT_ID = "eventid";

	public static final String ARTIST_NAME = "artistname";

	public static final String USER_LIST = "userlist";

}
