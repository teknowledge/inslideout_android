package com.inslideout.service;

public class ApplicationAPIs {

	// public static final String BASE_URL =
	// "http://apps.teks.co.in/inslideout/index.php/api/";

	public static final String BASE_URL = "http://apps.teks.co.in/inslideout/index.php/api/";

	public static final String REGISTER_USER = BASE_URL + "registerUser";

	public static final String GET_USER_BY_ID = BASE_URL + "getUserByID";

	public static final String UPDATE_USER = BASE_URL + "updateUser";

	public static final String AUTHENTICATE = BASE_URL + "authenticate";

	public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";

	public static final String UPDATE_PASSWORD = BASE_URL + "update_password";

	public static final String CHECK_USER_CONNECTION = BASE_URL
			+ "checkUserConnection";

	public static final String INSERT_USER_CONNECTION = BASE_URL
			+ "insertUserConnection";

	public static final String UPDATE_USER_CONNECTION = BASE_URL
			+ "updateUserConnection";

	public static final String GET_ARTIST_LIST = BASE_URL
			+ "getArtistList?searchtext=";

	// public static final String INSERT_USER_ARTIST = BASE_URL
	// + "insertUserArtist";

	public static final String INSERT_USER_ARTIST = BASE_URL
			+ "addDeviceArtist";

	public static final String FETCH_TEAM_COUNT = BASE_URL + "fetch_team_count";

	public static final String GET_SPORT_TEAMS = BASE_URL + "getSportTeams";

	public static final String SEARCH_SPORT_TEAM = BASE_URL + "searchSportTeam";

	public static final String INSERT_USER_TEAM = BASE_URL + "insertUserTeam";

	public static final String FETCH_USER_ADDED_TEAM = BASE_URL
			+ "fetch_user_added_team";

	public static final String REMOVE_USER_TEAM = BASE_URL + "remove_user_team";

	// public static final String FETCH_EVENTS = BASE_URL + "fetch_events";

	public static final String FETCH_EVENTS_ARTISTS = BASE_URL
			+ "fetch_artist_events";

	public static final String FETCH_EVENTS_SPORTS = BASE_URL
			+ "fetch_sports_events";

	public static final String GET_EVENT_DETAILS = BASE_URL + "getEventdetails";

	public static final String FETCH_ARTIST_EVENTS = BASE_URL
			+ "fetchArtistEvents";

	public static final String GET_ADDED_ARTIST = BASE_URL + "getAddedArtist";

	public static final String REMOVE_USER_ARTIST = BASE_URL
			+ "removeUserArtist";

	public static final String REMOVE_CONNECTIONS = BASE_URL
			+ "deleteUserConnection";

	public static final String REMOVE_USER_CONNECTION = BASE_URL
			+ "removeUserConnection";

	public static final String REGISTER_DEVICE_FOR_PUSH = BASE_URL
			+ "registerForPush";

	public static final String UNREGISTER_PUSH = BASE_URL
			+ "unregisterFromPush";

}
